import {createSlice} from "@reduxjs/toolkit";
/*
export const CampainReducer = createSlice({
    name : 'campain',
    initialState : {
        data: {}
    },
    reducers: {
        setType: (state, action) => {
            state.data.type = action.payload;
        },
        setDetails: (state, action) => {
            state.data = {...state.data, ...action.payload}
        },
        setAction: (state, action) => {
            state.data.action = action.payload
        },
        empty: (state) => {
            state.data = {}
        }
    }
})

export const {setType, setDetails, setAction, empty} = CampainReducer.actions;
export default CampainReducer.reducer; */

export const CampainReducer = createSlice({
    name: 'campain',
    initialState: {
        data: null
    },
    reducers: {
        connection: (state, action) => {
            state.token = action.payload.token;
        },
        disconnection: (state) => {
            state.token = null;
            localStorage.removeItem('revalidate');
            localStorage.removeItem('token');
        }
    }
});

export const { connection, disconnection } = CampainReducer.actions;
export default CampainReducer.reducer;