import { createSlice } from "@reduxjs/toolkit";

export const AgencyReducer = createSlice({
    name: 'token',
    initialState: {
        details: null,
        twoFactor: false,
        newCampain: {}
    },
    reducers: {
        fill: (state, action) => {
            state.details = action.payload
        },
        empty: (state) => {
            state.details = null
            state.twoFactor = false;
        },
        validated: (state) => {
            state.twoFactor = true
        },
        changeTwoFactor: (state, action) => {
            state.details.isSecured = action.payload; 
        },
        setCampainType: (state, action) => {
            state.newCampain.type = action.payload;
        },
        setCampainDetails: (state, action) => {
            state.newCampain = {...state.newCampain, ...action.payload}
        },
        setCampainAction: (state, action) => {
            state.newCampain.action = action.payload
        },
        setCustomAction: (state, action) => {
            state.newCampain.custom = action.payload;
        },
        emptyCampain: (state) => {
            state.newCampain = {}
        }
    }
})

export const { fill, empty, validated, changeTwoFactor, setCampainType, setCustomAction, setCampainDetails, setCampainAction, emptyCampain } = AgencyReducer.actions;
export default AgencyReducer.reducer;