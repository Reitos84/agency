import { createSlice } from "@reduxjs/toolkit";

export const AddPropertyReducer = createSlice({
    name: "addProperty",
    initialState: {
        data: {},
        editionReload: false
    },
    reducers: {
        chooseCategory: (state, action) => {
            state.data.category = action.payload
        },
        chooseType: (state, action) => {
            state.data.type = action.payload
        },
        addAddress: (state, action) => {
            state.data.country = action.payload.country;
            state.data.city = action.payload.city;
            state.data.address = action.payload.address;
            state.data.area = action.payload.area;
            state.data.what3words = action.payload.what3words;
            state.data.lng = action.payload.lng;
            state.data.lat = action.payload.lat;
        },
        addPictures: (state, action) => {
            state.data.pictures = action.payload;
        },
        removePictures:(state) => {
            state.data.pictures = null
        },
        addAmanties: (state, action) => {
          state.data.amanties = action.payload
        },
        emptyReducers: (state) => {
            state.data = {}
        },
        fillDataToEdition: (state, action) => {
            state.data = {
                ...action.payload,
                amanties: {
                    ...action.payload
                },
                isEditing: true
            };
        },
        revertUpdateState: (state) => {
            if(state.data.isEditing){
                state.data = {}
            }
        },
        toggleEditionRelaod: (state) => {
            state.editionReload = !state.editionReload
        }
    }
});

export const { chooseCategory, toggleEditionRelaod, revertUpdateState, fillDataToEdition, chooseType, addAddress, addPictures, removePictures, emptyReducers, addAmanties } = AddPropertyReducer.actions;
export default AddPropertyReducer.reducer;