import { createSlice } from "@reduxjs/toolkit";

export const bellReducer = createSlice({
    name: 'bell',
    initialState: {
        alert: false,
        interest: false
    },
    reducers: {
        newInterest: (state, action) => {
            state.interest = true
        },
        removeInterest: (state, action) => {
            state.interest = false
        },
        newAlert: (state, action) => {
            state.alert = true
        },
        removeAlert: (state, actione) => {
            state.alert = false
        }
    }
})

export const {newInterest, removeInterest, newAlert, removeAlert} = bellReducer.actions
export default bellReducer.reducer