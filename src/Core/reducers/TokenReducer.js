import { createSlice } from "@reduxjs/toolkit";

export const TokenReducer = createSlice({
    name: 'token',
    initialState: {
        token: null
    },
    reducers: {
        connection: (state, action) => {
            state.token = action.payload.token;
        },
        disconnection: (state) => {
            state.token = null;
            localStorage.removeItem('revalidate');
            localStorage.removeItem('token');
        }
    }
});

export const { connection, disconnection } = TokenReducer.actions;
export default TokenReducer.reducer;