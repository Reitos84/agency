import { createStore, combineReducers } from "@reduxjs/toolkit";
import TokenReducer from "@reducers/TokenReducer";
import AgencyReducer from "@reducers/AgencyReducer";
import {persistReducer, persistStore} from "redux-persist";
import storage from "redux-persist/lib/storage";
import AddPropertyReducer from "@reducers/AddPropertyReducer";
import bellReducer from "../reducers/bellReducer";

const persistConfig = {
    key: 'root',
    storage,
};

const combinerReducers = combineReducers({
    token: TokenReducer,
    agency: AgencyReducer,
    property: AddPropertyReducer,
    bell: bellReducer
});

const persistedReducer = persistReducer(persistConfig, combinerReducers);

let store =  createStore(
    persistedReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
let persistor = persistStore(store);
export {store, persistor};