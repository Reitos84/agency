import Service from "./Service";

class PromoteService extends Service{
    async add(data){
        return await this.client.post('/promotes', data);
    }
}

export default PromoteService;