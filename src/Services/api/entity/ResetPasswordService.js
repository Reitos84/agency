import Service from "./Service";

class ResetPasswordService extends Service{
    constructor() {
        super();
    }

    async requestEmail(email){
        const query = await this.client.post('/reset_password', {
            email: email,
            side: 'agency'
        });
        return query.data;
    }

    async changePassword(data){
        const query = await this.client.post('/reset_password/resolve', data);
        return query.data;
    }

    async verifyParams(data){
        const query = await this.client.post('/reset_password/check', data);
        return query.data;
    }
}

export default ResetPasswordService;