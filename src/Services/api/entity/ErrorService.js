import Service from "./Service";

class ErrorService extends Service{
    async get(state, index){
        let params = new URLSearchParams();
        params.append('isReal', 1);
        params.append('agency', index);
        params.append('isDone', state === 'in-corse' ? 0 : 1);
       let query = await this.client.get(`/property_reports?${params.toString()}`);
       return query.data;
    }

    async update(index, data){
        let query = await this.client.put(`/property_reports/${index}`, data);
        return query.data; 
    }
}

export default ErrorService;