import Service from "@api/entity/Service";

class BookingService extends Service{
    constructor() {
        super();
    }

    async getInCorse(page){
        let query = await this.client.get(`/bookings?page=${page}&status=0`);
        return query.data;
    }

    async getAccepted(page){
        let query = await this.client.get(`/bookings?page=${page}&status=1`);
        return query.data;
    }

    async accept(index){
        let query = await this.client.put(`/bookings/${index}`, {
            status: 1
        });
        return query.data;
    }

    async refuse(index){
        let query = await this.client.put(`/bookings/${index}`, {
            status: 3
        });
        return query.data;
    }
}

export default BookingService;