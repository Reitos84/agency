import Service from "./Service";

class AlerteService extends Service{
    constructor() {
        super();
    }

    async getAll(index){
        const query = await this.client.get(`/alertes?order[isView]=asc&user=${index}`);
        return query.data;
    }

    async getNotView(){
        const query = await this.client.get(`/alertes?isView=true&user=${index}`);
        return query.data;
    }

    async setView(index){
        const query = await this.client.put(`/alertes/${index}`, {
            isView: true
        });
        return query.data;
    }
}

export default AlerteService;