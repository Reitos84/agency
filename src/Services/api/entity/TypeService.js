import Service from "@api/entity/Service";

class TypeService extends Service{

    constructor(){
        super();
    }

    async getList(){
        const response = await this.client.get('/property_types');
        return response.data;
    }
}

export default TypeService;