import client from "@api/client/axios";

class Service{

    api_path = '/api/';

    constructor(){
        this.client = client;
    }

}

export default Service;