import Service from "./Service";

class CampainService extends Service {
    actions = null;
    types = null;

    constructor() {
        super();
    }

    async getTypes(){
        if(this.types){
            return this.types;
        }else{
            const query = await this.client.get('/campain_types?isActive=1');
            this.types = query.data;
            return query.data;
        }
    }

    async getActions(){
        if(this.actions){
            return this.actions;
        }else{
            const query = await this.client.get('/campain_actions?isActive=1');
            this.actions = query.data;
            return query.data;
        }
    }

    async postActions(data) {
        const query =  await this.client.post('/campain_actions', data);
        return query.data;
    }

    async sendCampain(data, index){
        const query = await this.client.post('/campains', {
            ...data,
            status: 'pending',
            agency: `${this.api_path}users/${index}`
        });
        return query;
    }

    async get(index, status, page){
        const params = new URLSearchParams();
        params.append('agency', index);
        params.append('status', status);
        params.append('page', page);

        const query = await this.client.get(`/campains?${params.toString()}`);
        return query.data;
    }
}

export  default CampainService;