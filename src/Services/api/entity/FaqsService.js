import Service from "@api/entity/Service";

class FaqsService extends Service{

    constructor(){
        super()
    }

    async getAgencyList(){
        let param = new URLSearchParams();
        param.append('side', 'agency');
        param.append('isAgency', 1);
        const query = await this.client.get('/faqs?' + param.toString());
        return query.data;
    }

    async getOnAgency(){
        let param = new URLSearchParams();
        param.append('side', 'agency');
        const query = await this.client.get('/faqs?' + param.toString());
        return query.data;
    }

    async getSeachKeys(key){
        let param = new URLSearchParams();
        param.append('side', 'agency');
        param.append('question', key);
        const query = await this.client.get('/faqs?' + param.toString());
        return query.data;
    }
}

export default FaqsService;