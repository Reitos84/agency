import Service from "@api/entity/Service";

class PictureService extends Service {

    constructor() {
        super()
    }

    async sendPictures(files){
        let data = new FormData();
        for(var i = 0; i < files.length; i++){
            data.append(`file${i}`, files[i]);
        }
        let query = await this.client.post('/pictures/send', data);
        return query.data;
    }

    async remove(index){
        return await this.client.delete(`/pictures/${index}`);
    }

    async setIsBackground(index){
        return await this.client.put(`/pictures/${index}`, {
            isBackground: true
        });
    }

    async removeBackground(index){
        return await this.client.put(`/pictures/${index}`, {
            isBackground: false
        });
    }

    async update(index, data){
        return await this.client.put(`/pictures/${index}`, {
            ...data
        });
    }
}

export default PictureService;