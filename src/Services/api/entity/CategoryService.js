import Service from "@api/entity/Service";

class CategoryService extends Service{

    constructor(){
        super();
    }

    async getAll(){
        const query = await this.client.get('/categories');
        return query.data;
    }

    async findWithSlug(title){
        const query = await this.client.get( `/categories?slug=${title}`);
        return query.data;
    }
}

export default CategoryService;