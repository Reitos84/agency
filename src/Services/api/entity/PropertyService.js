import Service from "@api/entity/Service";

class PropertyService extends Service{

    taggedPropertyFind = null;
    taggedPropertyResult = null;

    constructor(){
        super();
    }

    async post(data){
        return await this.client.post(`/properties`, {
            ...data
        });
    }

    urlFactory(category, path, key, page, type, order, status){
        let url = `/properties?deleted=false&category=${category}&page=${page}`;
        if(key){
            url += `&${path}=${key}`;
        }
        if(type){
            url += `&type=${type}`;
        }
        if(status){
            url += `&status=${status}`;
        }
        if(order){
            url += encodeURI(`&order[price]=${order}`);
        }else{
            url += encodeURI(`&order[createdAt]=desc`);
        }
        return url;
    }

    async getProperties(category, page, ...args){
        let query = await this.client.get(this.urlFactory(category, null, null, page, args[0], args[1], args[2]));
        return query.data;
    }

    async searchPropertiesWithTitle(category, key, page, ...args){
        let query = await this.client.get(this.urlFactory(category, 'title', key, page, args[0], args[1], args[2]));
        return query.data;
    }

    async searchPropertyWithAddress(category, key, page, ...args){
        let query = await this.client.get(this.urlFactory(category, 'address', key, page, args[0], args[1], args[2]));
        return query.data;
    }

    async getRecents(){
        let query = await this.client.get('/properties?page=1&deleted=false&order%5BcreatedAt%5D=desc');
        return query.data;
    }

    async getWithId(index){
        if(this.taggedPropertyFind === index){
            return this.taggedPropertyResult;
        }else{
            let query = await this.client.get(`/properties/${index}`);
            this.taggedPropertyFind = index;
            this.taggedPropertyResult = query.data;
            return query.data;
        }
    }

    async updatePorperty(index, data){
        let query = await this.client.put(`/properties/${index}`, data);
        return query.data;
    }
}

export default PropertyService;