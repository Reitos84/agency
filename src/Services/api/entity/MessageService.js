import Service from "./Service";

class MessageService extends Service{
    async getMyMessage(index){
        let query = await this.client.get(`/messages?user=${index}&order[createdAt]=desc`);
        return query.data;
    }

    async getGlobalMessage(){
        let query = await this.client.get(`/messages?isAbsolute=${1}`);
        return query.data;
    }

    async getMyUnreadMessage(index){
        let query = await this.client.get(`/messages?user=${index}&isView=false&order[createdAt]=desc`);
        return query.data;
    }

    async setSeenMessage(){
        let query = await this.client.put(`/messages/view`, {});
        return query.data;
    }

    async deleteMyNote(){
        let query = await this.client.delete(`/messages/delete`);
        return query.data;
    }
}

export default MessageService;