import Service from "./Service";

class InterestService extends Service{

    constructor(){
        super();
    }

    async getAll(page=1, done=0){
        page = page > 0 ? page : 1;
        let query = await this.client.get(`interests?page=${page}&done=${done}`);
        return await query.data;
    }

    async update(index, data){
        let query = await this.client.put(`interests/${index}`, data);
        return await query.data;
    }

}

export default InterestService;