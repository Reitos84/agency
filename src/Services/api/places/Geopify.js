class Geopify {

    key = '';

    constructor(){
        this.key = process.env.REACT_APP_GEOPIFY;
    }

    async neightborhood(lat, lng, category){
        let query = await fetch(`https://api.geoapify.com/v2/places?categories=${category}&filter=circle:${lng},${lat},1000&bias=proximity:${lng},${lat}&lang=fr&limit=5&apiKey=${this.key}`);
        return await query.json();
    }
}

export default Geopify;