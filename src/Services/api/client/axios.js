import axios from "axios";
import {store} from "@core/stores/store";
import { disconnection } from "@reducers/TokenReducer";
import LocalStorageService from "@services/utils/LocalStorageService";

const basePath = process.env.REACT_APP_API_BASE_PATH;

const client = axios.create({
    baseURL: basePath + "/api"
});

let localStorageService = new LocalStorageService();

const refreshToken = async () => {
    let Refresh = localStorageService.getItem('revalidate');
    if(!Refresh){
        store.dispatch(disconnection());
        window.location.href = '/login';
    }
    const rClient = axios.create({
        baseURL: basePath + "/api"
    });
    try {
        let query = await rClient.post('/token/refresh', {
            refresh_token: Refresh
        });
        let data = query.data;
        localStorage.setItem('token', data.token);
        localStorage.setItem('revalidate', data.refresh_token);
        return data.token;
    }catch (err){
        store.dispatch(disconnection());
        window.location.href = '/login';
    }
}

client.interceptors.request.use(
    config => {
        const token = localStorage.getItem('token');
        if (token) {
            config.headers['Authorization'] = 'Bearer ' + token;
        }
        // config.headers['Content-Type'] = 'application/json';
        return config;
    },
    error => {
    Promise.reject(error)
});
/*
axios.interceptors.response.use((response) => {
    return response
}, function (error) {
    const originalRequest = error.config;
    if (error.response.status === 401 && !originalRequest._retry) {

        originalRequest._retry = true;
        const refreshToken = localStorage.getItem('revalidate');
        return axios.post('/token/refresh', {
                "refresh_token": refreshToken
        }).then(res => {
            if (res.status === 201) {
                localStorageService.setToken(res.data);
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorageService.getAccessToken();
                return axios(originalRequest);
            }
        })
    }
return Promise.reject(error);
});
*/
client.interceptors.response.use((response) => {
    return response;
}, async (error) => {
    const originalRequest = error.config;
    if(error.response.status === 401){
        if(!originalRequest._retry){
            originalRequest._retry = true;
            const access_token = await refreshToken();
            client.defaults.headers.common['Authorization'] = "Bearer " + access_token;

            return client(originalRequest);
        }else{
            store.dispatch(disconnection());
            window.location.href = '/login';
        }
    }else{
        return error;
    }
});

export default client;