import axios from "axios";

class AuthRequest{
    constructor(){
        const basePath = process.env.REACT_APP_API_BASE_PATH;
        this.client = axios.create({
            baseURL: basePath + "/api"
        });
    }

    async login(username, password){
        try{
            const response = await this.client.post('/login', {
                username: username,
                password: password
            });
            return response.data;
        }catch(error){
            return error;
        }
    }
}

export default AuthRequest;