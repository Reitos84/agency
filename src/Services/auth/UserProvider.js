import client from "@api/client/axios";

class UserProvider{
    constructor(){
        this.client = client;
    }

    async emailIsExist(email){
        let query = await this.client.get(`/users?email=${email}`);
        return query.data;
    }

    async Provide(token){
        this.client.defaults.headers.common.Authorization = `Bearer ${token}`;
        const response = await this.client.get(`/provide`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });
        const data = await response.data;
        return data;
    }

    async sendSignInRequest(data){
        const query = await this.client.post(
            '/users', 
            {
                ...data, 
                isSignAsAgency: true,
                roles: ["ROLE_AGENCY"]
            }
        );
        return query;
    }

    async EditInfos(details){
        const response = await this.client.put(`/users/${details.id}`, details);
        const data = await response.data;
        return data;
    }

    async PostAvatar(avatar){
        const response = await this.client.post('/send/avatar', avatar);
        const data = await response.data;
        return data;
    }

    async getResume(){
        const response = await this.client.get('/resume');
        const data = await response.data;
        return data;
    }

    async changePassword(password){
        const query = await this.client.post('/change/password', {password: password});
        const data = await query.data;
        return data;
    }

    async requestEmail(email) {
        const query = await this.client.get(`/mail/two-factor-auth/${email}`);
        return query.data;
    }

    async verifyCode(code){
        const query = await this.client.post(`/auth/verify`, {
            code: code
        });
        return query.data;
    }

    async changeSecurity(index, value){
        const query = await this.client.put(`/users/${index}`, {
            isSecured: value
        });
        return query.data;
    }
}

export default UserProvider;