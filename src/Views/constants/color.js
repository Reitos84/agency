export const primary = "#8ac411";
export const primaryLight = "#8ac41127";
export const profileColors = [
    "#023e8a",
    "#e63946",
    "#6b705c",
    "#fb8500",
    "#2a9d8f",
    "#264653",
    "#001219",
    "#ae2012",
    "#f72585",
    "#606c38",
    "#7209b7",
    "#5a189a",
    "#272640",
    "#f4d58d",
    "#641220",
    "#3d0e61",
    "#114b5f",
    "#2e294e",
    "#46414b",
    "#7161ef"
]