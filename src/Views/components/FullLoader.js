import loader from "@assets/loader/home-outline.gif"
import styled from "@emotion/styled";

const FullLoader = () => {

    const LoadContent = styled.div({
        position: "fixed",
        width: "100%",
        height: "100vh",
        background: "#fff",
        zIndex: "9999",
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    })

    return(
        <LoadContent>
            <img alt="chargement" src={loader} />
        </LoadContent>
    )
}

export default FullLoader;