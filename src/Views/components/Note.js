import {
    AccountBoxOutlined,
    AnnouncementOutlined, BubbleChartOutlined, DataUsageOutlined,
    EventAvailable, HelpOutline, HelpOutlineOutlined,
    HouseOutlined, InfoOutlined, PolicyOutlined,
    UpdateOutlined
} from "@material-ui/icons";
import * as moment from "moment";
import 'moment/locale/fr';

const Note = ({data}) => {

    let icon = '';
    switch (data.category){
        case 'register':
            icon = <AccountBoxOutlined />;
            break;
        case 'publish':
            icon = <HouseOutlined />;
            break;
        case 'update':
            icon = <UpdateOutlined />;
            break;
        case 'booking':
            icon = <EventAvailable />
            break;
        case 'tips':
            icon = <HelpOutlineOutlined />;
            break;
        case 'boost':
            icon = <DataUsageOutlined />;
            break;
        case 'campain':
            icon = <AnnouncementOutlined />;
            break;
        case 'policy':
            icon = <PolicyOutlined />;
            break;
        default:
            icon = <InfoOutlined />;
    }

    return (
        <div className={"note"}>
            {
                data.isView ? '' : <div className={ data.isAbsolute ? "dot blue" : "dot"} />
            }
            <div className={"note-icon"}>
                {icon}
            </div>
            <div className={"note-content"}>
                { data.content }
                <div className={"note-date"}>
                    {moment(data.createdAt).calendar()}
                </div>
            </div>
        </div>
    )
}

export default Note;