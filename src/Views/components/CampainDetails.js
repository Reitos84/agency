import {KeyboardBackspaceOutlined} from "@material-ui/icons";
import {useState} from "react";
import {useDispatch} from "react-redux";
import {setCampainDetails} from "@reducers/AgencyReducer";
import {useSelector} from "react-redux";

const CampainDetails = ({nextStep}) => {

    const campain = useSelector(state => state.agency.newCampain);

    const [title, setTitle] = useState(campain.title);
    const [days, setDays] = useState(campain.days);
    const [description, setDescription] = useState(campain.description);
    const [message, setMessage] = useState('');

    const dispatch = useDispatch();

    const handleNextStep = () => {
        if(title.length > 0 && days > 0 && description.length > 0){
            setMessage('');
            dispatch(setCampainDetails({
                title: title,
                days: days,
                description: description
            }));
            nextStep(3);
        }else{
            setMessage('Veillez remplir le formulaire pour passer à l\'étape suivante');
        }
    }

    return(
      <div>
          <div className={"c-title"}>
              Donnez plus de détails sur votre campagne
          </div>
          <div className={"message"}>
              {message}
          </div>
          <div className={"campain-form"}>
              <div className={"box"}>
                  <label>Titre</label>
                  <input value={title} onChange={(e) => setTitle(e.currentTarget.value)} type={"text"} />
              </div>
              <div className={"box"}>
                  <label>Durée</label>
                  <div className={"ipt-group"}>
                      <input value={days} onChange={(e) => setDays(e.currentTarget.value)} type={"number"} />
                      <span>Jours</span>
                  </div>
              </div>
              <div className={"box"}>
                  <label>Description</label>
                  <textarea value={description} onChange={(e) => setDescription(e.currentTarget.value)}/>
              </div>
          </div>
          <div className={"my-btns"}>
              <button onClick={() => nextStep(1)} className={"previous"}>
                  <KeyboardBackspaceOutlined />
                  Précédent
              </button>
              <button onClick={handleNextStep} className={"next"}>Suivant</button>
          </div>
      </div>
    )
}

export default CampainDetails;