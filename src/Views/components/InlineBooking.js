import * as moment from "moment";
import basepath from "@constants/basepath";
import 'moment/locale/fr';
import {DoneAll, MoreHoriz, Remove, TrendingFlat} from "@material-ui/icons";
import {thousandsSeparators} from "currency-thousand-separator";
import {useState} from "react";
import {Link, useLocation} from "react-router-dom";

const InlineBooking = ({data, accept, refuse}) => {

    const [optionShow, setOptionShow] = useState(false);

    let background = false;
    const firstImage = data.property.pictures[0];
    data.property.pictures.forEach(picture => {
        if(picture.isBackground){
            background = picture;
        }
    });

    const params = new URLSearchParams(useLocation().search);
    const pathname = useLocation().pathname;
    params.append('property', data.property.id)

    return(
        <tr>
            <td>{moment(data.createdAt).calendar()}</td>
            <td>
                <Link to={{pathname: pathname, search: params.toString()}}>
                    <img src={background ? basepath + background.url : basepath + firstImage.url} alt={background ? background.name : firstImage.name}/>
                    <span className={"property-title"}>
                        {data.property.title}
                    </span>
                </Link>
            </td>
            <td>
                <span className={"user-avatar"} style={{background: data.client.color}}>{data.client.name.charAt(0)}</span>
                {data.client.name}
            </td>
            <td>
                <span className={"date"}>
                    {moment(data.startedAt).format('l')}
                </span>
                <TrendingFlat />
                <span className={"date"}>
                    {moment(data.endsAt).calendar()}
                </span>
            </td>
            <td>
                {thousandsSeparators(data.amount)}{process.env.REACT_APP_CURENCY}
            </td>
            { data.status === 0 ?
                <td>
                    <MoreHoriz onClick={() => setOptionShow(!optionShow)} className={"opt"} />
                    { optionShow ? <div className={"options-box"}>
                        <span onClick={accept} className={"option"}>
                            <DoneAll /> Accepter
                        </span>
                        <span onClick={refuse} className={"option"}>
                            <Remove /> Refuser
                        </span>
                    </div> : '' }

                </td> : <td /> }
        </tr>
    );
}

export default InlineBooking;