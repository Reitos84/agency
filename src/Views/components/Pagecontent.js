import styled from "@emotion/styled";

const Pagecontent = styled.div({
    width: "100%",
    height: "60vh",
    margin: "auto",
    position: "relative",
    "& .loading": {
        margin: "100px auto 0",
        textAlign: "center",
        "& img": {
            width: "50px"
        }
    }
});

export default Pagecontent;