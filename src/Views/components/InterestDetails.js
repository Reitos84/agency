import loader from "@assets/loader/785.svg";
import {CloseOutlined} from "@material-ui/icons";

const InterestDetails = ({data, close, done, load}) => {

    return(
        <div className={"mask active"}>
            <div className={"mask-content"}>
                <div className={"mask-title"}>
                    Détails sur l'intérêt
                    <CloseOutlined onClick={close} />
                </div>
                <div className="mask-body">
                    <div className="mask-property-int">
                        <div className={"details-inline"}>
                            {
                                data && data.more_details ?
                                    <div className={"cases"}>
                                        Le client souhaite obtenir plus d'informations
                                    </div>
                                :
                                    <></>
                            }
                            {
                                data && data.visit ?
                                    <div className={"cases"}>Le client souhaite visiter la propriété</div>
                                :
                                    <></>
                            }
                        </div>
                        <div className={"details-inline"}>
                            <strong>Client</strong>: <br />{ data ? data.name : '' } 
                        </div>
                        <div className={"details-inline"}>
                            <strong>Email</strong>: <br />{ data ? data.email : '' } 
                        </div>
                        <div className={"details-inline"}>
                            <strong>Contact</strong>: <br /><a href={`tel:${data.contact}`}>{ data ? data.contact : '' } </a>
                        </div>
                        {
                            data && data.country ?
                            <div className={"details-inline"}>
                                <strong>Pays</strong>: <br />{ data.country} 
                            </div>
                            :
                            <></>
                        }
                        <div className={"details-inline"}>
                            <strong>Message</strong>: <br />
                            { data ? data.comment : '' } 
                        </div>
                    </div>
                </div>
                <div className={"mask-footer"}>
                    <button onClick={close} className={"inno"}>Fermer</button>
                    {
                        data.done ?
                        ''
                        :
                        <button onClick={() => done(data.id)}>
                            {
                                load ? 
                                    <img src={loader} alt={"veillez pateinter"} />
                                :
                                    ""
                            }
                            Terminer
                        </button>
                    }           
                </div>
            </div>
        </div>
    )
}

export default InterestDetails;