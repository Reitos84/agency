import loader from "@assets/loader/home-outline.gif"

const Loader = () => {
    return(
        <div className="loading">
            <img alt="chargement" src={loader} />
        </div>
    )
}

export default Loader;