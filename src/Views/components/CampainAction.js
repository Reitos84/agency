import {useEffect, useState} from "react";
import Flatloader from "./Flatloader";
import {KeyboardBackspaceOutlined} from "@material-ui/icons";
import {useDispatch} from "react-redux";
import {setCampainAction, setCustomAction} from "@reducers/AgencyReducer";
import {useSelector} from "react-redux";

const CampainAction = ({nextStep, service}) => {

    const customAction = useSelector(state => state.agency.newCampain.custom);

    const [load, setLoad] = useState(true);
    const [selected, setSelected] = useState(useSelector(state => state.agency.newCampain.action));
    const [actions, setActions] = useState([]);
    const [message, setMessage] = useState('');
    const [describe, setDescribe] = useState(customAction ? customAction.description : '');

    useEffect(() => {
        service.getActions().then((data) => {
            setActions(data['hydra:member']);
            setLoad(false);
        });
    }, []);

    const dispatch = useDispatch();

    const handleNextStep = () => {
        if(selected){
            console.log(customAction)
            if((selected === "other" && !customAction) || (selected === "other" && !customAction) || (customAction && describe !== customAction.description)){
                service.postActions({
                    title: "Proposition",
                    description: describe,
                    provider: 'agency'
                }).then(data => {
                    if(data.id){
                        dispatch(setCustomAction({
                            description: describe,
                            id: data['@id']
                        }));
                        dispatch(setCampainAction(data['@id']));
                        setMessage('');
                        nextStep(4);
                    }else{
                        setMessage('Une erreur s\'est produite, essayez à nouveau');
                    }
                }).catch(() => {
                    setMessage('Une erreur s\'est produite, essayez à nouveau');
                });
            }else{
                dispatch(setCampainAction(selected));
                setMessage('');
                nextStep(4);
            }
        }else{
            setMessage('Veillez choisir une action avant de passer à l\'étape suivante')
        }
    }

    if(load){
        return <Flatloader />
    }else {
        return (
            <div>
                <div className={"c-title"}>
                    Définir le but de votre campagne
                </div>
                <div className={"message"}>
                    {message}
                </div>
                <div className={"reponses"}>
                    { actions.map(x =>
                        <div key={x.id} className={"uno-response"}>
                            <input checked={selected === x['@id']} onChange={(e) => setSelected(e.currentTarget.value)} value={x['@id']} name={"action"} type={"radio"}/>
                            <div>
                                {x.title}
                                <div>
                                    {x.description}
                                </div>
                            </div>
                        </div>
                    ) }
                    <div className={"uno-response"}>
                        <input checked={customAction && selected === customAction.id || selected === 'other'} onChange={(e) => setSelected(e.currentTarget.value)} value={"other"} name={"action"} type={"radio"}/>
                        <div>
                            Autre
                            <div>
                                Donnez plus de détails sur votre objectif, soyez très précis
                            </div>
                        </div>
                    </div>
                    {
                        selected === "other" || (customAction && customAction.id === selected) ?
                            <div className={"box"}>
                                <textarea value={describe} onChange={(e) => setDescribe(e.currentTarget.value)} placeholder={"Décrire le but de votre campagne"} />
                            </div>
                        : <></>
                    }
                </div>
                <div className={"my-btns"}>
                    <button onClick={() => nextStep(2)} className={"previous"}>
                        <KeyboardBackspaceOutlined />
                        Précédent
                    </button>
                    <button onClick={handleNextStep} className={"next"}>Suivant</button>
                </div>
            </div>
        )
    }
}

export default CampainAction;