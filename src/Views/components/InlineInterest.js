import {useLocation, Link} from "react-router-dom";
import defaultImage from "../assets/icon/home-concept.png";
import * as moment from "moment";

const InlineInterest = ({data, viewDetails}) => {

    let background = false;
    const firstImage = data.property.pictures[0];
    data.property.pictures.forEach(picture => {
        if(picture.isBackground){
            background = picture;
        }
    });

    const params = new URLSearchParams(useLocation().search);
    const pathname = useLocation().pathname;
    params.append('property', data.property.id);

    let basepath = process.env.REACT_APP_API_BASE_PATH;

    return(
        <tr key={data.property.id} >
            <td>
                <Link to={{pathname: pathname, search: params.toString()}}>
                    {
                        firstImage ?
                            <img src={background ? basepath + background.url : basepath + firstImage.url} alt={background ? background.name : firstImage.name}/>
                            :
                            <img src={defaultImage} alt={"home-concept"} />
                    }
                    <span className={"label"}>
                        {
                            data.property.title.substr(0, 35)
                        }
                        {
                            data.property.title.length > 35 ? '...' : ''
                        }
                    </span>
                </Link>
            </td>
            <td>
                { data.name }
            </td>
            <td>
                { data.contact }
            </td>
            <td>
                { data.email }
            </td>
            <td>
                { data.country }
            </td>
            <td>
                { moment(data.createdAt).calendar() }
            </td>
            <td>
                <span className="sample-details" onClick={() => viewDetails(data)}>Details</span>
            </td>
        </tr>
    )
}

export default InlineInterest;