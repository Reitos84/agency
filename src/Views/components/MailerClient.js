const MailerClient = () => {
    return(
        <div className={"mailer-client"}>
            <div className={"client-avatar"}>
                <span style={{background: '#e56925'}}>
                    S
                </span>
            </div>
            <div className={"client-text"}>
                <div className={"client-name"}>Sanogo Ibrahim</div>
                <div className={"client-text-text"}>
                    Bonjour juste un renseignement sur le prix de vos mensuel pour cette propriété, il y a des réduction ?
                </div>
                <div className={"time"}>
                    hier à 19:34
                </div>
                <div className={"new"}>

                </div>
            </div>
        </div>
    );
}

export default MailerClient;