import { useState, useEffect } from "react";
import { HelpOutline } from "@material-ui/icons";

const BoostDetails = ({data, setData}) => {

    const [featured, setFeatured] = useState(data.featured ? data.featured : false);
    const [emailing, setEmailing] = useState(data.emailing ? data.emailing : false);
    const [social, setSocial] = useState(data.social ? data.social : false);
    const [startedAt, setStartedAt] = useState(data.startedAt ? data.startedAt : false);
    const [duration, setDuration] = useState(data.duration ? data.duration : 1);
    const [message, setMessage] = useState('');
    const [amount, setAmount] = useState(data.amount ? data.amount : 0);

    useEffect(() => {
        let price = 0;
        if(emailing){
            price += parseInt(process.env.REACT_APP_MAILING_BOOST_PRICE) * duration; 
        }
        if(social){
            price += parseInt(process.env.REACT_APP_SOCIAL_BOOST_PRICE) * duration; 
        }
        if(featured){
            price += parseInt(process.env.REACT_APP_FEATURED_BOOST_PRICE) * duration; 
        }
        setAmount(price);
    }, [featured, emailing, social, duration]);

    const handleSaveData = () => {
        let now = new Date();
        if([emailing, featured, social].some(e => e) && duration > 0){
            setMessage('');
            setData({
                featured: featured,
                emailing: emailing,
                social: social,
                startedAt: startedAt,
                duration: duration,
                amount: amount
            });
        }else{
            setMessage('Veillez bien remplir le formulaire');
        }
    }

    return(
        <div className="boost-details">
            <div className="boost-title">
                Booster une propriété
            </div>
            <div className="desc">
                Le lorem ipsum est, en imprimerie, une suite de mots sans signification utilisée à titre provisoire pour calibrer une mise en page 
            </div>
            <div className={"message"}>
                {message}
            </div>
            <div>
                Choisir les option <a className={"tip"} href={""}><HelpOutline /></a>
            </div>
            <div className={"box choose-plan"}>
                <div onClick={() => setFeatured(!featured)} className={featured ? "plan active" : "plan"}>
                    Feateared Properties
                </div>
                <div onClick={() => setEmailing(!emailing)} className={emailing ? "plan active" : "plan"}>
                    Emailing
                </div>
                <div onClick={() => setSocial(!social)} className={social ? "plan active" : "plan"}>
                    Réseaux Sociaux
                </div>
            </div>
            <div className={"box"}>
                <label>Durée du boost</label>
                <input value={duration} onChange={(e) => setDuration(e.currentTarget.value) } min={1} max={7} type={"number"} />
                <span className={"fixed"}>Jours</span>
            </div>
            <div className={"box"}>
                <div className={"total"}>
                    <span>Total</span>
                    <span>{ `${amount} ${process.env.REACT_APP_CURENCY}` }</span>
                </div>
            </div>
            <button onClick={handleSaveData}>Passer au paiement</button>
        </div>
    )
}

export default BoostDetails;