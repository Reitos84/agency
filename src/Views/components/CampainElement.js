import styled from "@emotion/styled";
import background from "@assets/images/login-back.jpg";
import { semibold } from "@constants/fonts";
import { primary } from "@constants/color";

const CampainElement = () => {
    return(
        <Contain>
            <img src={background} alt={"Concept de nouvelle propriété"} />
            <div className="body">
                <div className="camp-title">
                    Devenez propriétaire en 10 jours sans conditions, souscrivez à votre pass habitât
                </div>
            </div>
            <div className="camp-bottom">
                <span className="status">
                    <strong/>
                    Pending
                </span>
                <button>voir</button>
            </div>
        </Contain>
    );
}

const Contain = styled.div({
    margin: "0 50px 30px 0",
    width: "250px",
    height: "350px",
    boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.1)",
    borderRadius: "5px",
    position: "relative",
    "& img": {
        display: "block",
        width: "250px",
        height: "150px",
        objectFit: "cover",
        borderRadius: "5px 5px 0 0"
    },
    "& .body" : {
        padding: "10px",
        height: "160px"
    },
    "& .camp-title": {
        fontSize: "20px",
        fontFamily: semibold
    },
    "& .camp-bottom": {
        position: "absolute",
        bottom: "10px",
        left: "0",
        width: "100%",
        padding: "0 10px",
        display: "flex",
        justifyContent: "space-between",
        "& button": {
            fontFamily: semibold,
            fontSize: "14px",
            padding: "1px 10px 2px",
            border: "none",
            background: primary,
            color: "#fff",
            borderRadius: "30px",
            cursor: "pointer"
        },
        "& .status strong":{
            display: "inline-block",
            height: "10px",
            width: "10px",
            backgroundColor: "gold",
            borderRadius: "50%",
            marginRight: "3px"
        }
    }
});

export default CampainElement;