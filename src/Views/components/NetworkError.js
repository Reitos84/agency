import styled from "@emotion/styled";
import { Report } from "@material-ui/icons";
import { semibold } from "@constants/fonts";

const NetworkError = () => {

    const Content = styled.div({
        margin: "auto",
        textAlign: "center",
        maxWidth: "250px",
        color: "#727272",
        fontFamily: semibold,
        "& svg": {
            fontSize: "50px",
            marginBottom: "20px"
        }
    });

    return(
        <Content>
            <Report />
            <div>
                Une erreur s'est produite, essayez à nouveau
            </div>
        </Content>
    );
}

export default NetworkError;