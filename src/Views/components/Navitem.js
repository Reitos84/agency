import { Link } from "react-router-dom";

const Navitem = ({to, children, className}) => {
    return(
        <Link className={className} to={to}>
            {children}
        </Link>
    );
}

export default Navitem;