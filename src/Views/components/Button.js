import styled from '@emotion/styled';
import {primary} from '@views/constants/color';
import { semibold } from '@constants/fonts';

const Button = styled.button({
    backgroundColor: primary,
    border: "none",
    fontFamily: semibold,
    height: "35px",
    padding: "0 25px",
    borderRadius: "3px",
    color: "#fff",
    cursor: "pointer",
    "& img": {
        width: "15px",
        position: "relative",
        top: "2px",
        marginRight: "5px"
    }
});

export default Button;