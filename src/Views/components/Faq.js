import { ExpandLess, ExpandMore } from "@material-ui/icons";

const Faq = ({data, active, set}) => {

    return(
        <div className="questions">
            <div onClick={() => set(data.id) } className="questions-title">
                {data.question}
                { data.id === active ? <ExpandLess /> : <ExpandMore />}
            </div>
            <div className={ data.id === active ? "question-content active" : "question-content"}>
                {data.answer}
            </div>
        </div>
    );
}

export default Faq;