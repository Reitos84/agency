import {KeyboardBackspaceOutlined} from "@material-ui/icons";
import {useState, useEffect} from "react";
import loader from "@assets/loader/785.gif";
import {useSelector, useDispatch} from "react-redux";
import {emptyCampain} from "@reducers/AgencyReducer";

const CampainValidation = ({service, nextStep}) => {
    const [published, setPublished] = useState(false)

    const campain = useSelector(state => state.agency.newCampain);
    const user = useSelector(state => state.agency.details );

    const dispatch = useDispatch();

    useEffect(() => {
        campain.days = Number(campain.days);
        service.sendCampain(campain, user.id).then((data) => {
            if(data.status === 201){
                dispatch(emptyCampain());
                setPublished(true);
                setTimeout(nextStep, 1000);
            }
        });
        return false;
    }, []);

    return(
        <div>
            <div className={"c-title"}>
                {
                    published ? 'Envoie effectuée' : 'Envoie en cours'
                }
            </div>
            {
                published ? 
                <div className="success">
                    Publier
                </div>
                : 
                <div className="camp-publish">
                    <img src={loader} alt={"Veillez patienter"} />
                </div>
            }
        </div>
    )
}

export default CampainValidation;