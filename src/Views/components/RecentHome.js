import basepath from "@constants/basepath";
import {thousandsSeparators} from "currency-thousand-separator";
import defaultImage from "../assets/icon/home-concept.png";

const RecentHome = ({property}) => {
    let background = false;
    const firstImage = property.pictures[0];
    property.pictures.forEach(picture => {
        if(picture.isBackground){
            background = picture;
        }
    });

    return(
        <div className={"recent-home"}>
            {
                firstImage ?
                    <img src={background ? basepath + background.url : basepath + firstImage.url} alt={background ? background.name : firstImage.name} />
                    :
                    <img src={defaultImage} alt={"property-concept"} />
            }
            <div>
                <div className="label">
                    {property.title}
                </div>
                <div className="prix">{thousandsSeparators(property.price)}xof</div>
            </div>
            <div className="category">{property.category.title}</div>
        </div>
    );
}



export default RecentHome;