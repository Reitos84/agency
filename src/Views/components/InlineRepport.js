import {Link, useLocation} from "react-router-dom";
import defaultImage from "../assets/icon/home-concept.png";
import * as moment from "moment";

const InlineRepport = ({data, viewDetails}) => {

    let background = false;
    const firstImage = data.property.pictures[0];
    data.property.pictures.forEach(picture => {
        if(picture.isBackground){
            background = picture;
        }
    });
    const params = new URLSearchParams(useLocation().search);
    const pathname = useLocation().pathname;
    params.append('property', data.property.id);
    let basepath = process.env.REACT_APP_API_BASE_PATH;

    return(
        <tr key={data.property.id} >
            <td>
                <Link to={{pathname: pathname, search: params.toString()}}>
                    {
                        firstImage ?
                            <img src={background ? basepath + background.url : basepath + firstImage.url} alt={background ? background.name : firstImage.name}/>
                            :
                            <img src={defaultImage} alt={"home-concept"} />
                    }
                    <span className={"label"}>
                        {
                            data.property.title.substr(0, 35)
                        }
                        {
                            data.property.title.length > 35 ? '...' : ''
                        }
                    </span>
                </Link>
            </td>
            <td>
                <ul style={{padding: 0}}>
                    {
                        data.message.map((x, key) => 
                            <li key={key}>{x}</li>    
                        )
                    }
                </ul>
            </td>
            <td>
                { moment(data.createdAt).fromNow() }
            </td>
            <td>
                <span className="sample-details" onClick={() => viewDetails(data)}>Details</span>
            </td>
        </tr>
    )
}

export default InlineRepport;