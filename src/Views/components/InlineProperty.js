import {MoreHoriz, VisibilityOutlined} from "@material-ui/icons";
import {thousandsSeparators} from "currency-thousand-separator";
import {useLocation, Link} from "react-router-dom";
import defaultImage from "../assets/icon/home-concept.png";
import * as moment from "moment";
import 'moment/locale/fr';

const InlineProperty = ({property, changeStatus, indexed, toggleIndexed, goToEdition}) => {

    let background = false;
    const firstImage = property.pictures[0];
    property.pictures.forEach(picture => {
        if(picture.isBackground){
            background = picture;
        }
    });

    const params = new URLSearchParams(useLocation().search);
    const pathname = useLocation().pathname;
    params.append('property', property.id)

    let basepath = process.env.REACT_APP_API_BASE_PATH;
    const clientLink = process.env.REACT_APP_CLIENT_LINK;
    const validKey = process.env.REACT_APP_REFFERER_KEY;

    return(
        <tr key={property.id} >
            <td>
                <a a target={'_blank'} href={`${clientLink}/property-detail/${property.code}?referrer=${validKey}`}>
                    {
                        firstImage ?
                            <img src={background ? basepath + background.url : basepath + firstImage.url} alt={background ? background.name : firstImage.name}/>
                            :
                            <img src={defaultImage} alt={"home-concept"} />
                    }
                    <span className={"label"}>
                        {
                            property.title.substr(0, 35)
                        }
                        {
                            property.title.length > 35 ? '...' : ''
                        }
                    </span>
                </a>
            </td>
            <td>{property.type.title}</td>
            <td title={property.address}>
                {
                    property.address.substr(0, 20)
                }
                {
                    property.address.length > 20 ? '...' : ''
                }
            </td>
            <td>
                {thousandsSeparators(property.price)}{ process.env.REACT_APP_CURENCY }
                {
                    property.frequency ?
                        ` / ${property.frequency}`
                        :
                        ""
                }
            </td>
            <td>
                <VisibilityOutlined
                    style={{
                        fontSize: '17px',
                        marginRight: '3px',
                        position: 'relative',
                        top: '4px'
                    }} />
                {property.view ? property.view : 0}
            </td>
            <td>
                <span className="status">
                    <i className={property.status}/>
                    {property.status}
                </span>
            </td>
            <td>
                { moment(property.createdAt).fromNow() }
            </td>
            <td>
                <span onClick={() => toggleIndexed(property.id)} className="more">
                    <MoreHoriz className={"m-icon"} />
                </span>
                <div className={ indexed === property.id ? "toolbox active" : "toolbox" }>
                    <span
                        onClick={() => goToEdition(property.id)}
                    >
                        Modifier
                    </span>
                    {
                        property.status === 'indisponible' ?
                            <span onClick={() => {
                                changeStatus(property.id, {status: 'disponible', deletedAt: null});
                            }}>Disponible</span>
                            :
                            <span onClick={() => {
                                changeStatus(property.id, {status: 'indisponible', deletedAt: null});
                            }}>Indisponible</span>
                    }
                    {
                        property.status === 'supprimer' ?
                            <span onClick={() => {
                                changeStatus(property.id, {status: 'disponible', deletedAt: null});
                            }}>Disponible</span>
                            :
                            <span onClick={() => {
                                changeStatus(property.id, {status: 'supprimer', deletedAt: (new Date()).toJSON()});
                            }}>Supprimer</span>
                    }
                </div>
            </td>
        </tr>
    );
}
export default InlineProperty;