import { ExpandLess, ExpandMore, HelpOutlineOutlined } from "@material-ui/icons";
import nl2br from "react-nl2br";

const Linefaq = ({data, selected, setSelected}) => {
    return(
        <div className={selected === data.id ? "inline-faq selected" : "inline-faq"}>
            <div className="quest-title" onClick={setSelected}>
                <div>
                    <HelpOutlineOutlined /> 
                    {data.question}
                </div>
                { data.id === selected ? <ExpandLess className="expand" /> : <ExpandMore className="expand" /> }
            </div>
            <div className="quest-content">
                { nl2br(data.answer) }
            </div>
        </div>
    )
}



export default Linefaq;