
import loader from "@assets/loader/785.svg";
import {CloseOutlined, EditOutlined} from "@material-ui/icons";

const ErrorDetails = ({data, close, done, load, goEdition}) => {

    return(
        <div className={"mask active"}>
            <div className={"mask-content"}>
                <div className={"mask-title"}>
                    Détails sur l'erreur
                    <CloseOutlined onClick={close} />
                </div>
                <div className="mask-body">
                    <div className="mask-property-int">
                        <div className={"details-inline"}>
                            <div className={"cases"}>
                                <h4>Liste des erreurs</h4>
                                <ul>
                                {
                                    data.message.map((x, key) => <li key={key}>{x}</li>)
                                }
                                </ul>
                            </div>
                        </div>
                        <div className={"details-inline"}>
                            <strong>Nom du mentionneur</strong>: <br />
                            { data ? data.name : '' } 
                        </div>
                        <div className={"details-inline"}>
                            <strong>Role du mentionneur</strong>: <br />
                            { data ? data.role : '' } 
                        </div>
                        <div className={"details-inline"}>
                            <strong>Message</strong>: <br />
                            { data ? data.comment : '' } 
                        </div>
                        <button onClick={() => goEdition(data.property.id)} to={""}>
                            Modifier la propriété maintenant
                            <EditOutlined 
                                style={
                                    {
                                        fontSize: '20px', 
                                        position: 'relative',
                                        top: '3px'
                                    }
                                } 
                            />
                        </button>
                    </div>
                </div>
                <div className={"mask-footer"}>
                    <button onClick={close} className={"inno"}>Fermer</button>
                    {
                        data.done ?
                        ''
                        :
                        <button onClick={() => done(data.id)}>
                            {
                                load ? 
                                    <img src={loader} alt={"veillez pateinter"} />
                                :
                                    ""
                            }
                            Terminer
                        </button>
                    }           
                </div>
            </div>
        </div>
    )
}

export default ErrorDetails;