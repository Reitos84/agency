import styled from "@emotion/styled";
import {anotherbold} from "../constants/fonts";

const Simpletitle = styled.div({
    width: "600px",
    textAlign: 'center',
    fontSize: "35px",
    fontFamily: anotherbold,
    margin: "0 auto 50px",
    "@media (max-width: 800px)": {
        fontSize: "24px",
        width: "70%"
    }
})

export default Simpletitle;