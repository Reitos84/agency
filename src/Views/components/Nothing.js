import styled from "@emotion/styled";
import { RoundedCornerOutlined } from "@material-ui/icons";
import { semibold } from "@constants/fonts";

const Nothing = () => {

    const NotContent = styled.div({
        width: "300px",
        margin: "30px auto",
        textAlign: "center",
        "& svg": {
            fontSize: "65px",
            marginBottom: "30px"
        },
        "& div": {
            fontFamily: semibold
        }
    })

    return(
        <NotContent>
            <RoundedCornerOutlined/>
            <div>
                Désolé, nous n'avons trouvé aucun résultat
            </div>
        </NotContent>
    )
}

export default Nothing;