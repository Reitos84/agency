import {useEffect, useState} from "react";
import Flatloader from "./Flatloader";
import {useDispatch} from "react-redux";
import {setCampainType} from "@reducers/AgencyReducer";
import {useSelector} from "react-redux";

const CampainType = ({nextStep, service}) => {
    const [load, setLoad] = useState(true);
    const [selected, setSelected] = useState(useSelector(state => state.agency.newCampain.type));
    const [allTypes, setAllTypes] = useState([]);
    const [message, setMessage] = useState('');

    const dispatch = useDispatch();

    useEffect(() => {
        service.getTypes().then((data) => {
            setAllTypes(data['hydra:member']);
            setLoad(false);
        })
    }, []);

    const handleSelectNext = () => {
        if(selected){
            dispatch(setCampainType(selected));
            nextStep(2);
        }else{
            setMessage('Veillez choisir un type avant de passer à l\'étape suivante')
        }
    }

    if(load){
        return <Flatloader />;
    }else {
        return (
            <div>
                <div className={"c-title"}>
                    Quel est la nature de votre campagne ?
                </div>
                <div className={"message"}>
                    {message}
                </div>
                <div className={"reponses"}>
                    {
                        allTypes.map(x =>
                            <div key={x.id} className={"uno-response"}>
                                <input checked={selected === x['@id']} onChange={(e) => setSelected(e.currentTarget.value)} name={"campain-type"}
                                       type={"radio"} value={x['@id']}/>
                                <div>
                                    {x.title}
                                    <div>
                                        {x.description}
                                    </div>
                                </div>
                            </div>
                        )
                    }
                </div>
                <div className={"my-btns"}>
                    <button onClick={() => handleSelectNext()} className={"next"}>
                        Suivant
                    </button>
                </div>
            </div>
        )
    }
}

export default CampainType;