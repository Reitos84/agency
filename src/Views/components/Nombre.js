import styled from "@emotion/styled";
import { primaryLight, primary } from "@constants/color";
import {semibold, bold } from "@constants/fonts";

const Nombre = styled.div({
    background: primaryLight,
    padding: "10px 100px 20px 50px",
    position: "relative",
    marginRight: "20px",
    fontFamily: semibold,
    borderRadius: "10px",
    "& .total": {
        fontSize: "30px",
        display: "block",
        fontFamily: bold
    },
    "& svg": {
        position: "absolute",
        left: "10px",
        top: "15px",
        background: primary,
        color: "#fff",
        padding: "3px",
        borderRadius: "50%",
        fontSize: "30px"
    }
})

export default Nombre;