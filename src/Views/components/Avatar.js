import styled from "@emotion/styled";
import { semibold } from "@constants/fonts";

const Avatar = styled.div({
    height: "40px",
    marginTop: "10px",
    display: "flex",
    alignItems: "center",
    "& img": {
        height: "40px",
        width: "40px",
        borderRadius: "50%",
        objectFit: "cover",
    },
    "& svg": {
        fontSize: "40px",
        position: "relative",
        top: "-2px",
        right: "none"
    },
    "& span": {
        fontFamily: semibold,
        marginLeft: "10px"
    }
})

export default Avatar;