import {DeleteOutline, EventAvailable} from "@material-ui/icons";
import Note from "@components/Note";
import {useEffect, useState} from "react";
import Flatloader from "@components/Flatloader";
import Empty from "@components/Empty";
import {useDispatch, useSelector} from "react-redux";
import MessageService from "../../Services/api/entity/MessageService";
import { removeAlert } from "../../Core/reducers/bellReducer";

const Notification = () => {
    const [list, setList] = useState([]);
    const [count, setCount] = useState(0);
    const [globalNotes, setGlobalNotes] = useState([]);
    const [load, setLoad] = useState(true);

    const user = useSelector(state => state.agency.details);
    const asAlert = useSelector(state => state.bell.alert);
    const dispatch = useDispatch();

    let messageService = new MessageService();

    useEffect(async () => {
        let myNotifications = await messageService.getMyMessage(user.id);
        let global = await messageService.getGlobalMessage();
        setGlobalNotes(global['hydra:member']);
        setList([...global['hydra:member'], ...myNotifications['hydra:member']]);
        setLoad(false);
        messageService.setSeenMessage();
        let nbre = 0;
        myNotifications['hydra:member'].forEach(note => {
            if(!note.isView){
                nbre++;
            }
        });
        setCount(nbre);

        if(asAlert){
            dispatch(removeAlert());
        }
    }, []);

    const handleDelete = async () => {
        let texts = await messageService.deleteMyNote();
        if(texts.success === true){
            setList(globalNotes);
        }
    }

    return(
        <div className={"notification"}>
            <div className={"note-title"}>
                Notifications
                <span onClick={handleDelete}>
                    <DeleteOutline />
                </span>
            </div>
            <div className={"notification-list"}>
                { load ? <Flatloader /> : list.length > 0 ? list.map(note => <Note data={note} key={note.id} />) : <Empty />  }

            </div>
            {
                count > 0 ? <div className={"note-bottom"}>
                    {count} notification{ count > 1 ? 's' : '' } non lus
                </div> : ''
            }
        </div>
    )
}

export default Notification;