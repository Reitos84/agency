import todoIcon from "@assets/images/todo-list.png";
import { semibold } from "@constants/fonts";


const Empty = () => {
    return(
        <div
        style={{
            textAlign: "center",
            marginTop: "50px",
            fontFamily: semibold
        }}
        >
            <img style={{
             display: "block",
             margin: "0 auto 20px",}}
             src={todoIcon}
             alt="empty" />
            Rien à afficher pour l'instant
        </div>
    )
};

export default Empty;