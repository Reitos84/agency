import { useState } from "react";
import PromoteService from '@api/entity/PromoteService';
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { CheckCircleOutlined } from "@material-ui/icons";

const BoostPayment = ({boostData, uid}) => {

    const [cardnumber, setCardnumber] = useState('');
    const [cardexpire, setCardexpire] = useState('');
    const [cvc, setCvc] = useState('');
    const [cardname, setCardname] = useState('');
    const [payed, setPayed] = useState(true);
    const [message, setMessage] = useState('');
    const [success, setSuccess] = useState(false);

    const promoteService = new PromoteService();

    const user = useSelector(data => data.agency.details);

    const handleChangeNumber = (e) => {
        let value = e.target.value;
        if(cardnumber.length === 4 && value.length === 5){
            value = `${value.substring(0, 4)} ${value.substring(4, 5)}`;
        }
        if(cardnumber.length === 9 && value.length === 10){
            value = `${value.substring(0, 9)} ${value.substring(9, 10)}`;
        }
        if(cardnumber.length === 14 && value.length === 15){
            value = `${value.substring(0, 14)} ${value.substring(14, 15)}`;
        }
        if(value.length > 19){
            value = cardnumber;
        }
        setCardnumber(value);
    }

    const handleChangeExpiry = (e) => {
        let value = e.target.value;
        if(cardexpire.length === 2 && value.length === 3){
            if(!value.includes('/')){
                value = `${value.substring(0, 2)}/${value.substring(2, 3)}`;
            }
        }
        if(value.length > 5){
            value = cardexpire
        }
        setCardexpire(value);
    }

    const handleChangeCvc = (e) => {
        let value = e.target.value;
        if(value.length > 3){
            value = cvc
        }
        setCvc(value);
    }

    const handleSaveBoost = () => {
        if(payed){
            let data = {...boostData, property: uid, agency: `/api/users/${user.id}`};
            data.duration = parseInt(boostData.duration);
            promoteService.add(data).then((response) => {
                if(response.status === 201){
                    setSuccess(true);
                }else{
                    setMessage('Une erreur s\'est produite, essayez à nouveau');
                }
            }).catch(() => {
                setMessage('Une erreur s\'est produite, essayez à nouveau');
            });
        }
    }

    const history = useHistory();

    return(
        <>
            <div className={success ? "success-process display" : "success-process"}>
                <div className={"succes-process-content"}>
                    <div className={"icon"}>
                        <CheckCircleOutlined />
                    </div>
                    <div className={"tit"}>Réussie</div>
                    <div>Votre demande a bien été valider, nous allons traiter votre requête, vous serez notifier continuellement</div>
                    <button onClick={() => history.goBack()} className={"btn"}>Continuer</button>
                </div>
            </div>
            <div className="boost-details">
                <div className="boost-title">
                    Informations de paiement
                </div>
                <div className="desc">
                    Finalisez votre boost en fournissant vos informations de paiement
                </div>
                <label>Détails de la carte</label>
                <div className={"box"}>
                    <input type={"text"} onChange={handleChangeNumber} value={cardnumber} placeholder={"Numéro de la carte"} />
                </div>
                <div className={"box"}> 
                    <input type={"text"} onChange={handleChangeExpiry} value={cardexpire} placeholder={"Expiration"} />
                </div>
                <div className={"box"}> 
                    <input type={"text"} onChange={handleChangeCvc} value={cvc} placeholder={"cvc"} />
                </div>
                <div className={"box"}>
                    <label>Nom sur la carte</label>
                    <input style={{textTransform: "uppercase"}} value={cardname} onChange={(e) => setCardname(e.currentTarget.value)} type={"text"} />
                </div>
                <div style={{color: "red"}} className={"message"}>
                    { message }
                </div>
                <button onClick={handleSaveBoost}>Payer {boostData.amount}{ process.env.REACT_APP_CURENCY }</button>
            </div>
        </>
    )
}

export default BoostPayment;