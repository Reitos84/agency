import styled from "@emotion/styled";
import { primaryLight } from "@constants/color";
import { semibold } from "@constants/fonts";

let style = {
    display: "flex",
    height: "313px",
    width: "303px",
    borderWidth: "4px",
    borderStyle: "solid",
    borderColor: "#e5e5e5",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: "0 20px",
    cursor: "pointer",
    background: "#fff",
    transition: "0.3s",
    ":hover": {
        border: `4px solid ${primaryLight}`
    },
    "& span": {
        fontFamily: semibold,
        marginTop: "20px",
        fontSize: "22px"
    },
    ".active": {
        borderColor: "#e5e5e5"
    }
};


const Category = styled('div', style);

export default Category;