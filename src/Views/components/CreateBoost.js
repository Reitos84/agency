import '@css/booster.css';

const CreateBoost = ({property}) => {
    return(
        <div className={"boost-modal"}>
            <div className={"boost-modal-content"}>
                <div className={"title"}>
                    Booster une propriété
                </div>
                <div className={"form"}>
                    <div className={"box"}>
                        <label>Propriété</label>
                        <div className={"boost-property"}>
                            { property.title }
                        </div>
                    </div>
                    <div className={"box"}>
                        <label>Date de démarage</label>
                        <input type={"date"} />
                    </div>
                    <div className={"box"}>
                        <label>Durée</label>
                        <input type={"number"} />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CreateBoost;