import loader from "@assets/loader/785.svg";

const Whiteloader = () => {
    return(
        <img src={loader} />
    );
}

export default Whiteloader;