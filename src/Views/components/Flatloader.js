import loader from "@assets/loader/simple-loader.svg";

const Flatloader = () => {
    return(
        <img className="flat-loader" src={loader} alt={"Chargement en cours..."} />
    )
}

export default Flatloader;