import styled from "@emotion/styled";

const Centered = styled.div({
    textAlign: "center",
    marginTop: "90px"
});

export default Centered;