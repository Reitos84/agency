import {useLocation, Link} from "react-router-dom";
import defaultImage from "../assets/icon/home-concept.png";

const InlineBoost = ({data}) => {

    let background = false;
    const firstImage = data.property.pictures[0];
    data.property.pictures.forEach(picture => {
        if(picture.isBackground){
            background = picture;
        }
    });

    const params = new URLSearchParams(useLocation().search);
    const pathname = useLocation().pathname;
    params.append('property', data.property.id)

    let basepath = process.env.REACT_APP_API_BASE_PATH;

    return(
        <tr>
            <td>
                {
                    firstImage ?
                        <img src={background ? basepath + background.url : basepath + firstImage.url} alt={background ? background.name : firstImage.name}/>
                        :
                        <img src={defaultImage} alt={"home-concept"} />
                }
                <span className="label">{ data.property.type }</span>
            </td>
            <td className="progress">
                <div className="progress-index">
                    <div className="budget">
                        <span className="dot"></span>
                        <span>Budget 15,000xof</span>
                    </div>
                    <div className="duration">
                        12 jours restants
                    </div>
                </div>
                <div className="timeline">
                    <span></span>
                </div>
            </td>
            <td>
                { data.print }
            </td>
            <td>
                532
            </td>
            <td>
                <img className="social" src={facebookIcon} alt="social" />
                <img className="social img1" src={facebookIcon} alt="social" />
                <img className="social img2" src={facebookIcon} alt="social" />
            </td>
            <td>
                <span className="dot"></span> En cours
            </td>
        </tr>
    );
}

export default InlineBoost;