import {DeleteOutline, DoneOutlined, WallpaperOutlined} from "@material-ui/icons";
import basepath from "@constants/basepath";

export const ImagePreview = ({file, delections, setIsBack, backIndex}) => {
    return(
        <div className={file.id === backIndex ? "picture wide": "picture"}>
            <div className={"actions"}>
                <span className={file.id === backIndex ? "tic active": "tic"} onClick={setIsBack}>
                    { file.id === backIndex ? <><DoneOutlined />Arrière plan</> :  <><WallpaperOutlined /> Arrière plan</> }
                </span>
                <span onClick={delections} className={"tic"}>
                    <DeleteOutline /> Supprimer
                </span>
            </div>
            <img className={"element"} src={basepath + file.url} alt={file.name} />
        </div>
    )
}