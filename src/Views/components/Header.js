import "@css/header.css";
import Button from "@components/Button";
import {BusinessRounded, MenuOutlined, NotificationsOutlined, PowerSettingsNew} from "@material-ui/icons";
import Avatar from "@components/Avatar";
import { Link, useLocation } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { disconnection } from "@reducers/TokenReducer";
import { empty } from "@reducers/AgencyReducer";
import { handleTooggleMenu, postlink } from "@navigation/queryfactory.js";
import basepath from "@constants/basepath";
import Notification from "@components/Notification";
import {useEffect, useState} from "react";
import MessageService from "../../Services/api/entity/MessageService";
import { newAlert, removeAlert } from "../../Core/reducers/bellReducer";

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const Header = () => {

    const [showNotification, setShowNotifiaction] = useState(false);
    const messageService = new MessageService();

    let { pathname } = useLocation(),
        params = useQuery();

    const dispatch = useDispatch();
    const agency = useSelector(state => state.agency);
    const asAlert = useSelector(state => state.bell.alert);

    const handleLogout = () => {
        dispatch(disconnection());
        dispatch(empty());
    }

    useEffect(() => {
        messageService.getMyUnreadMessage(agency.details.id).then(result => {
            if(result['hydra:totalItems'] > 0){
                dispatch(newAlert());
            }else{
                dispatch(removeAlert());
            }
        });
    }, []);

    return(
        <>
            <header>
                <div className="header-left">
                    <Link to={{pathname: pathname, search: handleTooggleMenu(params)}}>
                        <MenuOutlined className="menu-bar" />
                    </Link>
                    <Link to={{pathname: pathname, search: postlink(params)}}>
                        <Button className={"normal-post-btn"}>Poster une annonce</Button>
                        <Button className={"small-post-btn"}>Poster</Button>
                    </Link>
                </div>
                <div className="header-right">
                    <Avatar>
                        { agency.details.avatarUrl ? <img alt="agency logo" src={`${basepath}${agency.details.avatarUrl}`}/> : <BusinessRounded /> }
                        <span>{ agency.details ? agency.details.name : "Agency" }</span>
                    </Avatar>
                    <div className="bell-block">
                        {
                            asAlert ? <span className="r-bell"></span> : ''
                        }
                        <NotificationsOutlined onClick={() => setShowNotifiaction(!showNotification)} />
                    </div>
                    <PowerSettingsNew onClick={handleLogout} />
                </div>
            </header>
            {showNotification ? <Notification /> : '' }
        </>
    )
}

export default Header;