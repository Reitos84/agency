import styled from "@emotion/styled";
import {anotherbold} from "../constants/fonts";

const AddCampain = () => {
    return(
        <Content>
            <div className={"small-text"}>
                Créer une campagne
            </div>
            <div className={"title"}>
                Décrire votre campagne
            </div>
            <div className={"progress-bar"}>
                <div className={"dot"}/>
                <div className={"line"}/>
                <div className={"dot"}/>
                <div className={"line"}/>
                <div className={"dot"}/>
            </div>
            <div className={"details"}>

            </div>
        </Content>
    )
}

const Content = styled.div({
    padding: "30px",
    "& .small-text": {
        color: "#525252",
        marginBottom: "5px"
    },
    "& .title": {
        fontSize: "22px",
        fontFamily: anotherbold
    },
    "& .details": {

    },
    "& .progress-bar": {
        marginTop: "10px"
    }
});

export  default AddCampain;