import { Route, Switch } from "react-router-dom";
import Home from "@screens/dashboard/Home";
import Forsale from "@screens/dashboard/Forsale";
import Forrent from "@screens/dashboard/Forrent";
import Newpost from "@screens/dashboard/Newpost";
import About from "@screens/dashboard/About";
import Settings from "@screens/dashboard/Settings";
import Notifs from "@screens/dashboard/Messages";
import Promote from "@screens/dashboard/Promote";
import Credit from "@screens/dashboard/Credit";
import Campain from "@screens/dashboard/Campain";
import Knowledge from "@screens/dashboard/Knowledge";
import Booking from "@screens/dashboard/Booking";
import Interest from "@screens/dashboard/Interest";
import PropertyInfo from "@screens/dashboard/PropertyInfo";
import CampainModal from "@screens/CampainModal";
import BoostModal from "@screens/BoostModal";
import ErrorReport from "@screens/dashboard/ErrorReport";
import AddBoost from "../screens/boost/AddBoost";

const AccountRouter = () => {

    return(
        <>
            <Switch>
                <Route path={`/for-sale`}>
                    <div className="page-content">
                        <Forsale categorySlug={"sale"} titleText={"Propriétés à vendre"} />
                    </div>
                </Route>
                <Route path={`/for-rent`}>
                    <div className="page-content">
                        <Forrent categorySlug={"rent"} titleText={"Propriétés à louer"} />
                    </div>
                </Route>
                <Route path={`/about`}>
                    <About />
                </Route>
                <Route path={`/notification`}>
                    <div className="page-content">
                        <Notifs />
                    </div>
                </Route>
                <Route path={`/interest`}>
                    <div className="page-content">
                        <Interest />
                    </div>
                </Route>
                <Route path={`/campain`}>
                    <div className="page-content">
                        <Campain />
                    </div>
                </Route>
                <Route path={`/error-report`}>
                    <div className="page-content">
                        <ErrorReport />
                    </div>
                </Route>
                <Route path={`/promote`}>
                    <div className="page-content">
                        <Promote />
                    </div>
                </Route>
                <Route path={`/credit`}>
                    <div className="page-content">
                        <Credit />
                    </div>
                </Route>
                <Route path={`/settings`}>
                    <div className="page-content">
                        <Settings />
                    </div>
                </Route>
                <Route path={`/faqs`}>
                    <Knowledge />
                </Route>
                <Route path={`/boost/add/:propertyId`}>
                    <AddBoost />
                </Route>
                <Route path={`/`}>
                    <Home />
                </Route>
            </Switch>
            <Route path="/" component={Newpost} />
            <Route path="/" component={PropertyInfo} />
            <Route path="/" component={BoostModal} />
        </>
    );
}

export default AccountRouter;