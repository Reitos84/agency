import { useLocation } from "react-router-dom";
import {
    AnnouncementOutlined,
    DashboardOutlined, 
    EmojiFlagsOutlined,
    HelpOutline,
    HomeOutlined,
    HotelOutlined,
    InfoOutlined, 
    SettingsOutlined,
    ReportProblemOutlined
} from "@material-ui/icons";
import "@css/sidebar.css";
import Navitem from "@components/Navitem";
import logo from '../assets/logo/logo.png';
import smallLogo from '../assets/logo/small.png';
import { useSelector } from "react-redux";

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const Sidebar = () => {

    const asInterest = useSelector(state => state.bell.interest);

    let path = '/',
        {pathname} = useLocation(),
        menu = useQuery().get('menu'),
        params = useQuery().toString();

    return(
        <div className={menu ? "leftplacemenu leftbar" : "leftplacemenu sidebar"}>
            <div className="logo">
                <img className="long" src={logo} alt={"Abidjan Habitat"} />
                <img className="small" src={smallLogo} alt={"Abidjan Habitat"} />
            </div>
            <nav>
                <Navitem className={pathname===path ? "active" : ""} to={{pathname:`${path}`, search: params}}>
                    <DashboardOutlined />
                    <span>Tableau de bord</span>
                </Navitem>
                <Navitem className={pathname===`${path}for-sale` ? "active" : ""} to={{pathname:`${path}for-sale`, search: params}}>
                    <HomeOutlined />
                    <span>Propriété à vendre</span>
                </Navitem>
                <Navitem className={pathname===`${path}for-rent` ? "active" : ""} to={{pathname: `${path}for-rent`, search: params}}>
                    <HotelOutlined />
                    <span>Propriété à louer</span>
                </Navitem>
                <Navitem className={pathname===`${path}interest` ? "active" : ""} to={{pathname: `${path}interest`, search: params}}>
                    { asInterest ? <span className="r-dot"></span> : '' }
                    <EmojiFlagsOutlined />
                    <span>Intérêts</span>
                </Navitem>
                {/* <Navitem className={pathname===`${path}campain` ? "active" : ""} to={{pathname: `${path}campain`, search: params}}>
                    <AnnouncementOutlined />
                    <span>Campagnes</span>
                </Navitem> */}
                {/*
                    <Navitem className={pathname===`${path}promote` ? "active" : ""} to={{pathname: `${path}promote`, search: params}}>
                        <DonutLargeOutlined />
                        <span>Booster</span>
                    </Navitem>
                */}
                <Navitem className={pathname===`${path}error-report` ? "active" : ""} to={{pathname: `${path}error-report`, search: params}}>
                    <ReportProblemOutlined />
                    <span>Rapport d'erreurs</span>
                </Navitem>
                <Navitem className={pathname===`${path}about` ? "active" : ""} to={{pathname: `${path}about`, search: params}}>
                    <InfoOutlined />
                    <span>A propos</span>
                </Navitem>
                <Navitem className={pathname===`${path}faqs` ? "active" : ""} to={{pathname: `${path}faqs`, search: params}}>
                    <HelpOutline />
                    <span>Faqs</span>
                </Navitem>
            </nav>
            <div className="settings">
                <Navitem to={{pathname: `${path}settings`, search: params}}>
                    <SettingsOutlined />
                    <span>Configuration</span>
                </Navitem>
            </div>
        </div>
    );
}

export default Sidebar;