export const handleTooggleMenu = (params) => {
    if(params.get('menu')){
        params.delete('menu');
    }else{
        params.append('menu', 'hidden');
    }
    return params.toString();
}

export const absoluePostLink = (params) => {
    if(params.get('publish')){
        params.delete('publish');
    }else{
        params.append('publish', true);
    }
    return params.toString();
}

export const postlink = (params) => {
    if(params.get('publish')){
        params.delete('publish');
    }else{
        params.append('publish', true);
    }
    params = handleTooggleMenu(params);
    return params.toString();
}