import { BrowserRouter as Router } from "react-router-dom";
import "@css/sidebar.css";

const Navigation = ({children}) => {
    return(
        <Router>
            {children}
        </Router>
    )
}

export default Navigation;