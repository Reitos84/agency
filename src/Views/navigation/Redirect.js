import {Switch, Route} from "react-router-dom";
import Login from "@screens/Login";
import Account from "@screens/Account";
import ResetPassword from "@screens/ResetPassword";
import Register from "@screens/Register";
import ChangePassword from "@screens/ChangePassword";

const Redirect = () => {
    return(
        <Switch>
            <Route path="/account/">
                <Account />
            </Route>
            <Route path="/reset">
                <ResetPassword />
            </Route>
            <Route path={"/register"}>
                <Register />
            </Route>
            <Route path={"/password"}>
                <ChangePassword />
            </Route>
            <Route path="/">
                <Login />
            </Route>
        </Switch>
    )
}

export default Redirect;