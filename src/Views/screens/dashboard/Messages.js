import "@css/message.css";
import {MoreVert, ReplyOutlined, SearchOutlined} from "@material-ui/icons";
import MailerClient from "@components/MailerClient";
import RichText from "../../components/RichText";

const Messages = () => {

    return(
        <div className={"mailer"}>
            <div className={"mailer-client-content"}>
                <div className={"mailer-client-find"}>
                    <input type={"text"} placeholder={"Rechercher"} />
                    <SearchOutlined />
                </div>
                <div className={"mailer-client-list"}>
                    <div className={"mailer-client active"}>
                        <div className={"client-avatar"}>
                            <span style={{background: '#759635'}}>
                                K
                            </span>
                        </div>
                        <div className={"client-text"}>
                            <div className={"client-name"}>Kader Yoda</div>
                            <div className={"client-text-text"}>
                                Bonjour juste un renseignement sur le prix de vos mensuel pour cette propriété, il y a des réduction ?
                            </div>
                            <div className={"time"}>
                                hier à 19:34
                            </div>
                            <div className={"new"}>

                            </div>
                        </div>
                    </div>
                    <MailerClient />
                </div>
            </div>
            <div className={"mailer-response-content"}>
                <div className={"response-header"}>
                   <div className={"tagged-property"}>
                       Maison moderne cloturé
                   </div>
                    <div className={"options"}>
                        <MoreVert />
                    </div>
                </div>
                <div className={"reponse-message"}>
                    <div className={"reponse-date"}>
                        Mardi 16 septembre 2021, 16h30
                    </div>
                    <div className={"response-title"}>
                        Conversation avec Kader Yoda
                    </div>
                    <div className={"reponse-user-text"}>

                    </div>
                </div>
                <div className={"text-editor"}>
                    <div className={"editor-side"}>
                        <textarea placeholder={"Saisir votre message"} className={"message"}/>
                        <button>
                            <ReplyOutlined />
                            Envoyer
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Messages;
