import NewCampain from "./NewCampain";
import { useEffect, useState } from "react";
import Flatloader from "@components/Flatloader";
import Empty from "@components/Empty";
import CampainService from "@api/entity//CampainService";
import { useSelector } from "react-redux";
import * as moment from "moment";
import 'moment/locale/fr';

const Campain = () => {

    const details = useSelector(state => state.agency.details);

    document.title = "Campagnes";

    const [addCampain, setAddCampain] = useState(false);
    const [activatedPan, setActivatedPan] = useState('online');
    const [loading, setLoading] = useState(false);
    const [list, setList] = useState([]);
    const [page, setPage] = useState(1);
    const [pageTotal, setPageToTal] = useState(1);
    const [totalCampain, setTotalCampain] = useState(0);

    const campainService = new CampainService;

    useEffect(() => {
        if(addCampain === false){
            setLoading(true);
            campainService.get(details.id, activatedPan, page).then((data) => {
                setList(data['hydra:member']);
                setTotalCampain(data['hydra:totalItems']);
                setPageToTal(Math.ceil(data['hydra:totalItems'] / 15));
                setLoading(false);
            }).catch(() => {
                
            });
        }
    }, [activatedPan, addCampain, page]);

    return(
        <>
            <div className={"properties-content"}>
                <div className="title flex">
                    Campagnes
                    <button onClick={() => setAddCampain(true)} className={"btn"}>
                        Nouvelle campagne
                    </button>
                </div>
                {
                    addCampain ?
                        <NewCampain close={setAddCampain} />
                    :
                    <>
                        <div className="tab-pan">
                            <span onClick={() => setActivatedPan('pending')} className={ activatedPan === 'pending' ? "active" : ''}>
                                En attente
                            </span>
                            <span onClick={() => setActivatedPan('online')} className={ activatedPan === 'online' ? "active" : ''}>
                                En cours
                            </span>
                            <span onClick={() => setActivatedPan('done') } className={ activatedPan === 'done' ? "active" : ''}>
                                Terminées
                            </span>
                        </div>
                        {
                            loading ?
                                <Flatloader />
                                :
                                list.length > 0 ?
                                    <table>
                                        <thead>
                                            <tr>
                                                <td>Titre</td>
                                                <td>Duree (en jours)</td>
                                                <td>Description</td>
                                                <td>Ajouter le</td>
                                                <td>Actions</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                list.map(
                                                    x => <tr>
                                                        <td> { x.title } </td>
                                                        <td> { x.days } </td>
                                                        <td> { x.description } </td>
                                                        <td> { moment(x.createdAt).calendar() } </td>
                                                    </tr>
                                                )
                                            }
                                        </tbody>
                                    </table>
                                    :
                                    <Empty />
                        }
                    </>
                }
            </div>
            <div className={"paginate"}>
                <div className={"total-items"}>
                    { totalCampain } campagne{ totalCampain > 1 ? 's' : '' }
                </div>
                <div className={"page-switch"}>
                    <button
                        onClick={() => {
                            if(page > 1){
                                setPage(page - 1);
                            }
                        }}
                        className={ pageTotal === 1 ? "switch-action prev sad" : "switch-action prev"}>
                        Précédente
                    </button>
                    <div className={"current-page"}>
                        <span>Page</span>
                        <input
                            onChange={(e) => {
                                if(e.currentTarget.value <= pageTotal){
                                    setPage(e.currentTarget.value);
                                }
                            }}
                            type={"number"}
                            value={page}
                        />
                        sur { pageTotal }
                    </div>
                    <button
                        onClick={() => {
                            if(page < pageTotal){
                                setPage(page + 1);
                            }
                        }}
                        className={ pageTotal === page ? "switch-action suiv sad" : "switch-action suiv"}>
                        Suivante
                    </button>
                </div>
            </div>
        </>
    );
}

export default Campain;