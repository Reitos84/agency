import {useEffect, useRef, useState} from "react";
import ErrorService from "../../../Services/api/entity/ErrorService";
import Flatloader from "../../components/Flatloader";
import Empty from "../../components/Empty";
import InlineRepport from "../../components/InlineRepport";
import ErrorDetails from "../../components/ErrorDetails";
import { useDispatch, useSelector } from "react-redux";
import {fillDataToEdition} from "@reducers/AddPropertyReducer";
import { absoluePostLink } from "@navigation/queryfactory.js";
import {useLocation, Link} from "react-router-dom";
import PropertyService from "@api/entity/PropertyService";

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const ErrorReport = () => {

    const agency = useSelector(state => state.agency);

    const errorService = new ErrorService();

    document.title = "Rapport d'erreur";

    const dispatch = useDispatch();
    const propertyService = new PropertyService();

    const [list, setList] = useState([]);
    const [activatedPan, setActivatedPan] = useState('in-corse');
    const [loading, setLoading] = useState(true);
    const [tinyLoad, setTinyLoad] = useState(false);
    const [page, setPage] = useState(1);
    const [pageTotal, setPageToTal] = useState(1);
    const [totalRepport, setTotalRepport] = useState(0);
    const [indexedForDetails, setIndexedForDetails] = useState(null);

    useEffect(() => {
        setLoading(true);
        errorService.get(activatedPan, agency.details.id).then(data => {
            setList(data['hydra:member']);
            setTotalRepport(data['hydra:totalItems']);
            setPageToTal(Math.ceil(data['hydra:totalItems'] / 30));
            setLoading(false);
        });
    }, [activatedPan, page]);

    let { pathname } = useLocation(),
        params = useQuery();

    const handleDone = (index) => {
        setTinyLoad(true);
        errorService.update(index, {isDone: true}).then(data => {
            if(data.isDone){
                setList(list.filter(elem => elem.id != index));
                setIndexedForDetails(null);
                setTinyLoad(false);
            }else{
                setTinyLoad(false);
            }
        });
    }

    const editRef = useRef();

    const handleGoToEditProperty = async (index) => {
        let propertyDetails = await propertyService.getWithId(index);
        dispatch(fillDataToEdition(propertyDetails));
        editRef.current.click();
    }

    return(
        <>
            {
                indexedForDetails ?
                    <ErrorDetails done={handleDone} goEdition={handleGoToEditProperty} data={indexedForDetails} load={tinyLoad} close={() => setIndexedForDetails(null)} />
                :
                    ''
            }
            <div className={"properties-content"}>
                <div className="title">
                    Rapport d'erreurs
                </div>
                <Link className={"trigger-click"} ref={editRef} to={{pathname: pathname, search: absoluePostLink(params)}}></Link>
                <p>
                    Ici vous avez la liste des rapports d'erreurs que vous avez commis en renseignant vos propriétés.
                </p>
                <div className="tab-pan">
                    <span onClick={() => setActivatedPan('in-corse')} className={ activatedPan === 'in-corse' ? "active" : ''}>
                        En attente
                    </span>
                    <span onClick={() => setActivatedPan('done') } className={ activatedPan === 'done' ? "active" : ''}>
                        Terminées
                    </span>
                </div>
                {
                    loading ?
                        <Flatloader/>
                        :
                        list.length > 0 ?
                            <table>
                                <thead>
                                    <tr>
                                        <td>Propriété</td>
                                        <td>Erreur</td>
                                        <td>Ajouter le</td>
                                        <td>Actions</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        list.map((x, index) =>
                                            <InlineRepport viewDetails={setIndexedForDetails} data={x} key={index} />
                                        )
                                    }
                                </tbody>
                            </table>
                        :
                            <Empty />
                }
            </div>
            <div className={"paginate"}>
                <div className={"total-items"}>
                    { totalRepport } rapport{ totalRepport > 1 ? 's' : '' }
                </div>
                <div className={"page-switch"}>
                    <button
                        onClick={() => {
                            if(page > 1){
                                setPage(page - 1);
                            }
                        }}
                        className={ pageTotal === 1 ? "switch-action prev sad" : "switch-action prev"}>
                        Précédente
                    </button>
                    <div className={"current-page"}>
                        <span>Page</span>
                        <input
                            onChange={(e) => {
                                if(e.currentTarget.value <= pageTotal){
                                    setPage(e.currentTarget.value);
                                }
                            }}
                            type={"number"}
                            value={page}
                        />
                        sur { pageTotal }
                    </div>
                    <button
                        onClick={() => {
                            if(page < pageTotal){
                                setPage(page + 1);
                            }
                        }}
                        className={ pageTotal === page ? "switch-action suiv sad" : "switch-action suiv"}>
                        Suivante
                    </button>
                </div>
            </div>
        </>
    )
}

export default ErrorReport;