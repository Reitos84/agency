import "@css/credit.css";
import {AreaChart, Area, Tooltip, ResponsiveContainer } from "recharts";
import {AttachMoneyOutlined} from "@material-ui/icons";

const data = [
    {
        name: 'Page A',
        uv: 4000,
        pv: 2400,
        amt: 2400,
    },
    {
        name: 'Page B',
        uv: 3000,
        pv: 1398,
        amt: 2210,
    },
    {
        name: 'Page C',
        uv: 2000,
        pv: 9800,
        amt: 2290,
    },
    {
        name: 'Page D',
        uv: 2780,
        pv: 3908,
        amt: 2000,
    },
    {
        name: 'Page E',
        uv: 1890,
        pv: 4800,
        amt: 2181,
    },
    {
        name: 'Page F',
        uv: 2390,
        pv: 3800,
        amt: 2500,
    },
    {
        name: 'Page G',
        uv: 3490,
        pv: 4300,
        amt: 2100,
    },
];

const Credit = () => {
    return(
        <div className={"split-content"}>
            <div className={"left-content"}>
                <div className="title">
                    Portefeuille
                    <div className={"withdraw"}>
                        <AttachMoneyOutlined />
                        Retirer de l'argent
                    </div>
                </div>
                <div className={"balance-resume"}>
                    <div className={"balance"}>
                        <div className={"balance-title"}>Solde</div>
                        <span className={"amount"}>
                            45,000 xof
                        </span>
                    </div>
                    <div className={"balance"}>
                        <div className={"balance-title"}>En attente</div>
                        <span className={"amount"}>
                            76,000 xof
                        </span>
                    </div>
                    <div className={"balance"}>
                        <div className={"balance-title"}>Payments</div>
                        <span className={"amount"}>
                            17,000 xof
                        </span>
                    </div>
                </div>
                <div  className={"chart"}>
                    <ResponsiveContainer width="100%" height={180}>
                        <AreaChart
                            data={data}
                            syncId="anyId"
                            margin={{
                                top: 10,
                                right: 30,
                                left: 0,
                                bottom: 0
                            }}
                        >
                            <Tooltip />
                            <Area type="monotone" dataKey="pv" strokeWidth={3} stroke="#82ca9d" fill="#82ca9d" />
                        </AreaChart>
                    </ResponsiveContainer>
                </div>
                <div className={"history"}>
                    <div className={"label"}>
                        Historique des transactions
                    </div>
                    <table>
                        <thead>
                            <tr>
                                <td>Date</td>
                                <td>Opération</td>
                                <td>Débiteur</td>
                                <td>Montant</td>
                                <td>Status</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>23-10-2021</td>
                                <td>Reservation</td>
                                <td>
                                    <span style={{background: '#000'}} className={"avatar"}>S</span>
                                    Serge Sanogo
                                </td>
                                <td>45,000 xof</td>
                                <td>
                                    <span className={"status pending"}>
                                        En attente
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>23-10-2021</td>
                                <td>Promotion</td>
                                <td>
                                    <span style={{background: '#e5a356'}} className={"avatar"}>M</span>
                                    Moi
                                </td>
                                <td>3,000 xof</td>
                                <td>
                                    <span className={"status complete"}>
                                        Valider
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div className={"right-content"}>
                <div className={"title"}>
                    Méthode de paiements
                </div>
                <div className={"method-block"}>
                    <div className={"label"}>Carte de crédit</div>
                    <div className={"card"}>
                        <div className={"type"}>
                        </div>
                        <div className={"content"}>
                            <div>0897-089-0582-87</div>
                            <div className={"date"}>Expire le 17-08-2024</div>
                            <div className={"options"}>
                                <span>Modifier</span>
                                <span>Supprimer</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={"method-block"}>
                    <div className={"label"}>Mobile Money</div>
                </div>
            </div>
        </div>
    );
}

export default Credit;