import Postmodal from '@screens/Postmodal';
import { useLocation } from 'react-router';
import Categories from '@screens/publish/Categories';
import PropertyType from '@screens/publish/PropertyType';
import PropertyAdress from '@screens/publish/PropertyAdress';
import Gallery from '@screens/publish/Gallery';
import PropertyDetails from '@screens/publish/PropertyDetails';
import Published from "@screens/publish/Published";

const Newpost = ({history}) => {
    const params = new URLSearchParams(useLocation().search);
    const pathname = useLocation().pathname;

    let poster = params.get('publish');

    return(
        poster && <Postmodal
        close={() => {
            params.delete('publish');
            history.push(pathname + '?' + params.toString());
        }}
        >
            { poster === "true" ? <Categories /> : ''}
            { poster === "type" ? <PropertyType /> : ''}
            { poster === "location" ? <PropertyAdress /> : '' }
            { poster === "gallery" ? <Gallery /> : '' }
            { poster === "details" ? <PropertyDetails /> : '' }
            { poster === "done" ? <Published /> : '' }
        </Postmodal>
    )
}

export default Newpost;