import '@css/booster.css';
import facebookIcon from "@assets/icon/facebook.png";
import {Link, useLocation} from "react-router-dom";
import { useEffect, useState } from 'react';
import Flatloader from "@components/Flatloader";
import Empty from "@components/Empty";

const Promote = () => {

    document.title = "Boost";

    const [activatedPan, setActivatedPan] = useState('in-corse');
    const [list, setList] = useState([]);
    const [loading, setLoading] = useState(false);
    const [page, setPage] = useState(1);
    const [pageTotal, setPageToTal] = useState(1);
    const [totalPromote, setTotalPromote] = useState(0);

    const params = new URLSearchParams(useLocation().search);
    const pathname = useLocation().pathname;

    useEffect(() => {

    }, [])

    if(!params.has('boost')){
        params.append('boost', 'true');
    }

    return(
        <>
            <div className={"booster-page"}>
                <div className="title">
                    Booster
                    <Link to={{pathname: pathname, search: params.toString()}}>
                        <button>Booster une propiété</button>
                    </Link>
                </div>
                <div className="tab-pan">
                    <span onClick={() => setActivatedPan('waiting')} className={ activatedPan === 'waiting' ? "active" : ''}>
                        En attente
                    </span>
                    <span onClick={() => setActivatedPan('in-corse')} className={ activatedPan === 'in-corse' ? "active" : ''}>
                        En cours
                    </span>
                    <span onClick={() => setActivatedPan('done') } className={ activatedPan === 'done' ? "active" : ''}>
                        Terminés
                    </span>
                </div>
                <div className={"booster-content"}>
                    {
                        loading ?
                            <Flatloader />
                                :
                            list.length > 0 ?
                                <table>
                                    <thead>
                                        <tr>
                                            <td>Propriété</td>
                                            <td>Progression</td>
                                            <td>Impression</td>
                                            <td>Click</td>
                                            <td>Réseaux sociaux</td>
                                            <td>Status</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            list.map(
                                                x => <tr>
                                                    <td> { x.title } </td>
                                                    <td> { x.days } </td>
                                                    <td> { x.description } </td>
                                                    <td> { moment(x.createdAt).calendar() } </td>
                                                </tr>
                                            )
                                        }
                                    </tbody>
                                </table>
                                :
                                <Empty />
                    }
                </div>
            </div>
            <div className={"paginate"}>
                <div className={"total-items"}>
                    { totalPromote } boost{ totalPromote > 1 ? 's' : '' }
                </div>
                <div className={"page-switch"}>
                    <button
                        onClick={() => {
                            if(page > 1){
                                setPage(page - 1);
                            }
                        }}
                        className={ pageTotal === 1 ? "switch-action prev sad" : "switch-action prev"}>
                        Précédente
                    </button>
                    <div className={"current-page"}>
                        <span>Page</span>
                        <input
                            onChange={(e) => {
                                if(e.currentTarget.value <= pageTotal){
                                    setPage(e.currentTarget.value);
                                }
                            }}
                            type={"number"}
                            value={page}
                        />
                        sur { pageTotal }
                    </div>
                    <button
                        onClick={() => {
                            if(page < pageTotal){
                                setPage(page + 1);
                            }
                        }}
                        className={ pageTotal === page ? "switch-action suiv sad" : "switch-action suiv"}>
                        Suivante
                    </button>
                </div>
            </div>
        </>
    );
}

export default Promote;