import {useState} from 'react';
import CampainService from "@api/entity/CampainService";
import { ArrowBackOutlined, DoneAllOutlined } from '@material-ui/icons';
import loader from "@assets/loader/785.svg";
import { useSelector } from 'react-redux';

const NewCampain = ({close}) => {

    const details = useSelector(state => state.agency.details);

    const [message, setMessage] = useState('');
    const [load, setLoad] = useState(false);
    const [success, setSuccess] = useState(false);
    const [data, setData] = useState({
        goal: '',
        title: '',
        days: 0,
        description: ''
    });

    const campainService = new CampainService();

    const handleSendCampain = () => {
        if(Object.values(data).every(x => x)){
            setMessage('');
            setLoad(true);
            campainService.sendCampain(data, details.id).then(data => {
                if(data.status === 201){
                    setLoad(false);
                    setSuccess(true);
                }else{
                    setMessage('Une erreur s\'est produite, essayez a nouveau');
                }
            }).catch(() => {
                setMessage('Une erreur s\'est produite, essayez a nouveau');
            });
        }else{
            setMessage('Veillez a bien remplir le formulaire');
        }
    }

    return(
        <div className='create-campain'>
            <div className={ success ? "mask active" : "mask"}>
                <div className={"mask-content success"}>
                    <div className='icon'>
                        <DoneAllOutlined />
                    </div>
                    <div className='tit'>
                        Reussie
                    </div>
                    <div className='exp'>
                        Votre requete a bien ete envoyer, nous allons vous contacter pour la suite des operations.
                    </div>
                    <button onClick={() => close(false)} className='btn'>Terminer</button>
                </div>
            </div>
            <div className='second-title'>
                <ArrowBackOutlined onClick={() => close(false)} title={"Retour"} />
                Nouvelle campagne
            </div>
            <p>
                Veillez a donner plus d'information sur votre campagne.
            </p>
            <div className='campain-form'>
                <div className='box'>
                    <label>Saisir le but de votre campagne</label>
                    <textarea value={data.goal} onChange={(e) => setData({...data, goal: e.currentTarget.value})} />
                </div>
                <div className='box'>
                    <label>Titre de la campagne</label>
                    <input type='text' value={data.title} onChange={(e) => setData({...data, title: e.currentTarget.value})} />
                </div>
                <div className='box'>
                    <label>Duree (en jours)</label>
                    <input type='number' min={"1"} value={data.days} onChange={(e) => setData({...data, days: parseInt(e.currentTarget.value)})} placeholder="En jours" />
                </div>
                <div className='box'>
                    <label>Description de votre campagne</label>
                    <textarea value={data.description} onChange={(e) => setData({...data, description: e.currentTarget.value})} />
                </div>
                <div className='message'>
                    { message }
                </div>
                <button onClick={handleSendCampain} className='btn'>
                    {
                        load ? <img src={loader} alt={"..."} /> : ''
                    }
                    Envoyer
                </button>
            </div>
        </div>
    );
}

export default NewCampain;