import "@css/home.css";
import Button from "@components/Button";
import RecentHome from "@components/RecentHome";
import Empty from "@components/Empty";
import { Link, useLocation } from "react-router-dom";
import { absoluePostLink } from "@navigation/queryfactory.js";
import { useSelector } from "react-redux";
import Pagecontent from "@components/Pagecontent";
import { useEffect, useState } from "react";
import UserProvider from "@auth/UserProvider";
import NetworkError from "@components/NetworkError";
import Flatloader from "@components/Flatloader";
import PropertyService from "@api/entity/PropertyService"

function useQuery() {
    return new URLSearchParams(useLocation().search);
}


const Home = () => {

    document.title = "Tableau de bord";

    const [load, setLoad] = useState(true);
    const [stats, setStats] = useState([]);
    const [error, setError] = useState(false);
    const [resents, setResents] = useState([]);

    let { pathname } = useLocation(),
        params = useQuery();

    const token = useSelector(state => state.token);

    useEffect(async () => {
        let provider = new UserProvider(token.token);
        let propertyService = new PropertyService();
        let statsData = await provider.getResume();
        let recentsData = await propertyService.getRecents();
        if(statsData){
            setStats(statsData.data);
        }
        if(recentsData){
            setResents(recentsData['hydra:member'].slice(0, 5));
        }
        setLoad(false);
    }, []);

    if(load){
        return(
            <Pagecontent>
                <Flatloader />
            </Pagecontent>
        )
    }else if(error){
        return(
            <Pagecontent>
                <NetworkError/>
            </Pagecontent>
        )
    }else{
        return(
            <div className="home">
                <div className="resume">
                    <div className="stats">
                        <div className="block publish">
                            <div className="text">
                                <h1>Publier une maison<br /> maintenant</h1>
                                <Link to={{pathname: pathname, search: absoluePostLink(params)}}>
                                    <Button>Poster maintenant</Button>
                                </Link>
                            </div>
                        </div>
                        <div className="block">
                            <div className="block-title">
                                Intérêt
                            </div>
                            <div className="compteur">
                                <div className="current">
                                    { stats.interest }
                                </div>
                            </div>
                        </div>
                        <div className="block">
                            <div className="block-title">
                                Propriétés à vendre
                            </div>
                            <div className="compteur">
                                <div className="current">
                                    {  stats.sale.value }
                                </div>
                            </div>
                        </div>
                        <div className="block">
                            <div className="block-title">
                                Propriétés à louer
                            </div>
                            <div className="compteur">
                                <div className="current">
                                    {stats.rent.value}
                                </div>
                            </div>
                        </div>
                        {/* { stats.map((stat, index) => renderStat(stat, index)) } */}
                    </div>
                    <div className="lister">
                        <div className="block long">
                            <h4>Publier Récemments</h4>
                            {
                                resents.length > 0 ?
                                    resents.map(property => <RecentHome key={property.id} property={property} />)
                                :
                                    <Empty />
                            }
                        </div>
                        <div className="block long transparent">
                            <div className={"other-services red"}>
                                <div className={"title-ser"}>
                                    Services de relocalisation
                                </div>
                                <div className={"desc"}>
                                    Le déménagement peut être une période stressante pour tout le monde...
                                </div>
                                <a target={"_blank"} href={`${process.env.REACT_APP_CLIENT_LINK}/relocation-service`}>Plus d'infos</a>
                            </div>
                            <div className={"other-services blue"}>
                                <div className={"title-ser"}>
                                    Représentations de ventes
                                </div>
                                <div className={"desc"}>
                                    Lorsque vous décidez d'engager les services d’Abidjan Habitat pour vendre...
                                </div>
                                <a target={"_blank"} href={`${process.env.REACT_APP_CLIENT_LINK}/representation-sales`}>Plus d'infos</a>
                            </div>
                            <div className={"other-services green"}>
                                <div className={"title-ser"}>
                                    Commercialisation de Projet
                                </div>
                                <div className={"desc"}>
                                    L'immobilier est l'un des outils les plus puissants de création ...
                                </div>
                                <a target={"_blank"} href={`${process.env.REACT_APP_CLIENT_LINK}/project-marketing`}>Plus d'infos</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;
