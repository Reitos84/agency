import { Search } from "@material-ui/icons";
import Linefaq from "@components/Linefaq";
import { useEffect, useState } from "react";
import FaqsService from "@services/api/entity/FaqsService";
import Flatloader from "@components/Flatloader";
import Nothing from "@components/Nothing";
import "@css/knowledge.css";

let faqService = new FaqsService();

const Knowledge = () => {

    document.title = "Centre d'aide";

    const [questionList, setQuestionList] = useState([]);
    const [search, setSearch] = useState('');
    const [load, setLoad] = useState(false);
    const [pointed, setPointed] = useState(0);

    const handleFilter = (event) => {
        setSearch(event.target.value);
    }

    const handlePointed = (index)=>{
        if(pointed === index){
            setPointed(0);
        }else{
            setPointed(index);
        }
    } 

    const findElements = async () => {
        setLoad(true);
        let data = search.length > 0 ? await faqService.getSeachKeys(search) : await faqService.getOnAgency();
        if(data['hydra:member']){
            setQuestionList(data['hydra:member']);
            setLoad(false);
        }else{
            setLoad(false);
        }
    }

    useEffect(findElements, []);

    return(
        <div className={"faq-content"}>
            <div className="header">
                <div className="small-title">Questions fréquemment posées</div>
                <div className="big-title">Nous sommes là pour votre succès !</div>
                <div className="finder">
                    <input onChange={handleFilter} value={search} placeholder="Rechercher dans les faqs" />
                    <button onClick={findElements}>
                        <Search />
                        <span>Rechercher</span>
                    </button>
                </div>
            </div>
            <div className="faq-container">
                { load ?
                    <Flatloader /> :
                    !load && questionList.length === 0 ?
                        <Nothing /> :
                        questionList.map(x => <Linefaq selected={pointed} setSelected={() => handlePointed(x.id)} key={x.id} data={x} />)
                }
            </div>
        </div>
    )
}

export default Knowledge;