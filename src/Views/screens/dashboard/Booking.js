import styled from "@emotion/styled";
import Pagecontent from "@components/Pagecontent";
import {semibold} from "@constants/fonts";
import {useCallback, useEffect, useState} from "react";
import BookingService from "@api/entity/BookingService";
import {primary} from "@constants/color";
import InlineBooking from "@components/InlineBooking";
import Flatloader from "@components/Flatloader";
import Empty from "@components/Empty";
import InfiniteScroll from "react-infinite-scroll-component";

const Booking = () => {

    let service = new BookingService();

    const [inCorse, setInCorse] = useState([]);
    const [accepted, setAccepted] = useState([]);
    const [activatedPan, setActivatedPan] = useState('incorse');
    const [load, setLoad] = useState(true);
    const [currentPage, setCurrentPage] = useState(1);

    const getInCorse = useCallback((page) => {
        service.getInCorse(page).then((data) => {
            setInCorse([...inCorse, ...data['hydra:member']]);
            setLoad(false);
        });
    });

    const getAccepted = useCallback((page) => {
        service.getAccepted(page).then((data) => {
            setAccepted([...accepted, ...data['hydra:member']]);
            setLoad(false);
        });
    });

    const fetchData = (page) => {
        if(activatedPan === "incorse"){
            setAccepted([]);
            getInCorse(page);
        }else{
            setInCorse([]);
            getAccepted(page);
        }
    }

    useEffect(() => {
        if(currentPage === 1){
            setLoad(true);
        }
        fetchData(currentPage);
    }, [activatedPan, currentPage]);



    const handleAccept = (index) => {
        service.accept(index);
        setInCorse(inCorse.filter(x => x.id !== index));
    }

    const handleRefuse = (index) => {
        service.refuse(index);
        setInCorse(inCorse.filter(x => x.id !== index));
    }

    return(
        <Content>
            <Pagecontent>
                <div className="title">
                    Reservations
                </div>
                <div className={"all-pan"}>
                    <div onClick={() => { setCurrentPage(1); setActivatedPan('incorse');}} className={activatedPan === "incorse" ? "pan active" : "pan"}>En cours</div>
                    <div onClick={() => { setCurrentPage(1); setActivatedPan('accept');}} className={activatedPan === "accept" ? "pan active" : "pan"}>Accepter</div>
                </div>
                { load ?  <Flatloader /> : inCorse.length > 0 || accepted.length > 0 ?
                        <InfiniteScroll
                            dataLength={activatedPan === "incorse" ? inCorse.length : accepted.length }
                            next={() => { setCurrentPage(currentPage + 1); }}
                            hasMore={true}
                            loader={""}
                        >
                        <div className={"booking-content"}>
                            <table>
                                <thead>
                                <tr>
                                    <td>Date</td>
                                    <td>Propriété</td>
                                    <td>Client</td>
                                    <td>Durée</td>
                                    <td>Montant Estimé</td>
                                    <td/>
                                </tr>
                                </thead>
                                <tbody>
                                    {activatedPan === "incorse" ?
                                        inCorse.map(book => <InlineBooking refuse={() => handleRefuse(book.id)} accept={() => handleAccept(book.id)} key={book.id} data={book} />) :
                                        accepted.map(book => <InlineBooking refuse={() => handleRefuse(book.id)} accept={() => handleAccept(book.id)} key={book.id} data={book} />)
                                    }
                                </tbody>
                            </table>
                        </div>
                        </InfiniteScroll>
                    : <Empty />
                }
            </Pagecontent>
        </Content>
    );
}

const Content = styled.div({
    padding: "20px",
    "& .title": {
        fontSize: "22px",
        fontFamily: semibold,
        marginBottom: "30px"
    },
    "& .all-pan": {
        display: "flex",
        marginTop: "30px",
        borderBottom: '1px solid #e5e5e5',
        "& .pan": {
            width: '100px',
            padding: "0 10px 10px",
            color: "#323232",
            borderBottom: '3px solid transparent',
            cursor: "pointer",
            transition: '0.3s'
        },
        "& .pan.active": {
            borderColor: primary,
            color: "#000"
        },
    },
    "& .booking-content": {
        marginTop: "40px",
        paddingBottom: "70px",
        "& table": {
            borderCollapse: "collapse",
            width: "100%",
            "& thead": {
                background: "#f5f5f5",
                "& td": {
                    height: "50px",
                    borderBottom: "1px solid #e5e5e5",
                    padding: '0 5px'
                }
            },
            "& tbody": {
                "& tr": {
                    height: "70px",
                    borderBottom: "1px solid #e5e5e5",
                    color: "#323232",
                    "& a": {
                        color: "#000",
                        textDecoration: "none",
                    },
                    "& img": {
                        width: "50px",
                        height: "50px",
                        objectFit: "cover",
                        borderRadius: "3px"
                    },
                    "& .property-title": {
                        position: "relative",
                        marginLeft: "10px",
                        verticalAlign: "top",
                        top: "10px"
                    },
                    "& .user-avatar": {
                        display: "inline-block",
                        height: "35px",
                        width: "35px",
                        borderRadius: "50%",
                        textAlign: "center",
                        lineHeight: "35px",
                        color: "#fff",
                        fontSize: "20px",
                        marginRight: "10px"
                    },
                    "& .date": {
                        color: "#525252"
                    },
                    "& svg": {
                        margin: "0 10px",
                        position: "relative",
                        top: "6px"
                    },
                    "& .opt": {
                        display: "inline-block",
                        height: "30px",
                        width: "30px",
                        borderRadius: "50%",
                        cursor: "pointer",
                        boxShadow: "0 0 2px rgba(0, 0, 0, 0.2)",
                        color: "#323232"
                    },
                    "& td": {
                        position: "relative",
                        "& .options-box": {
                            position: "absolute",
                            width: "200px",
                            height: "80px",
                            background: "#fff",
                            right: "30px",
                            top: "85%",
                            boxShadow: "0 0 3px rgba(0, 0, 0, 0.2)",
                            padding: "5px",
                            borderRadius: "5px",
                            zIndex: "100",
                            "& .option": {
                                display: "block",
                                cursor: "pointer",
                                color: "#727272",
                                transition: "0.3s",
                                ":hover" : {
                                    color: "#222"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
});

export default Booking;