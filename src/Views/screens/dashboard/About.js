import '@css/about.css';
import Pagecontent from "@components/Pagecontent";
import { useSelector } from "react-redux";
import basepath from "@constants/basepath";
import * as moment from "moment";
import 'moment/locale/fr';
import { 
    VerifiedUserRounded, 
    LanguageOutlined, 
    PhoneOutlined,
    AlternateEmail,
    BusinessRounded
} from "@material-ui/icons";

const About = () => {

    const details = useSelector(state => state.agency.details);

    document.title = "Profil";

    console.log(details);

    return(
        <Pagecontent>
            <div className="comp-header">
                <div className="company">
                    <div className="logo">
                        {
                            details.avatarUrl ?
                                <img alt={details.avatarUrl} src={basepath + details.avatarUrl} />
                            :
                                <BusinessRounded style={{fontSize: '40px', marginRight: '20px'}} />
                        }
                    </div>
                    <div className="sub-details">
                        <div className="name">
                            {details.name}
                            {
                                details.isCertified ?
                                    <VerifiedUserRounded className='greened'/>
                                :  
                                    <VerifiedUserRounded />
                            }
                            
                        </div>
                        <div className="comment">
                            { details.comment }
                        </div>
                        <div className="complete">
                            {
                                details.website ?
                                    <div className="key-contact">
                                        <a href={details.website} target="_blank">
                                            <LanguageOutlined /> {details.website}
                                        </a>
                                    </div>
                                :
                                    ''
                            }
                            
                            <div className="key-contact">
                                <PhoneOutlined /> {details.mobilephone}
                            </div>
                            <div className="key-contact highlited">
                                Inscris { moment(details.createdAt).fromNow() }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="details">
                <h2>Details</h2>
                <div className="line">
                    <span>Email:</span> {details.email}
                </div>
                <div className="line">
                    <span>Contact Mobile :</span> {details.mobilephone}
                </div>
                <div className="line">
                    <span>Contact Bureau :</span> {details.officephone ? details.officephone : <i>NA</i>}
                </div>
                <div className="line">
                    <span>Address :</span> {details.localisation}
                </div>
                <div className="key-contact">
                    <div className="title">{ details.name.split(' ')[0] }</div>
                    <div className="address">
                        <AlternateEmail /> {details.email}
                    </div>
                    <div className="address">
                        <PhoneOutlined /> {details.officephone ? details.officephone : <i>NA</i>}
                    </div>
                    <div className="address">
                        <LanguageOutlined /> {details.website ? details.officephone : <i>NA</i>}
                    </div>
                </div>
            </div>
        </Pagecontent>
    );
}

export default About;
