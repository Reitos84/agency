import Pagecontent from "@components/Pagecontent";
import Editprofile from "@screens/Settings/Editprofile";
import Faq from "@components/Faq";
import { Link, useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import FaqsService from "@api/entity/FaqsService";
import NetworkError from "@components/NetworkError";
import Preferences from "@screens/Settings/Preferences";
import Policy from "@screens/Settings/Policy";
import Flatloader from "@components/Flatloader";
import '@css/settings.css';

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const Settings = () => {

    document.title = "Configuration";

    const matchpath = useLocation().pathname;

    let querylist = useQuery();
    let dist = querylist.get('dist') ? querylist.get('dist') : 'general';

    if(dist != "general" && dist != "preferences" && dist != "policy"){
        dist = "general";
    }

    const [Faqs, setFaqs] = useState(0);
    const [activeFaq, setActiveFaq] = useState(null);
    const [FaqError, setFaqError] = useState(false);

    useEffect(() => {
        let faqService = new FaqsService();
        let response = faqService.getAgencyList();
        response.then((data) => {
            if(data['hydra:member']){
                setFaqs(data['hydra:member']);
                handleShowFaq(data['hydra:member'][0].id)
            }
        }).catch(err => {
            setFaqError(true);
            setFaqs([]);
        });
        return;
    }, []);

    const handleShowFaq = (index) => {
        if(activeFaq === index){
            setActiveFaq(0);
            return true;
        }
        setActiveFaq(index);
    }

    const generatepath = (value) => {
        let query = new URLSearchParams(querylist);
        if(query.has('dist')){
            query.set('dist', value);
        }else{
            query.append('dist', value);
        }
        return {
            pathname: matchpath,
            search: query.toString()
        };
    }

    return(
        <Pagecontent>
            <div className={"configuration"}>
                <div className="settings-side">
                    <div className="title">
                        Configuration
                    </div>
                    <div className="tabs">
                        <Link to={generatepath('general')} className={dist === "general" ? "tab-item active" : "tab-item"}>
                            Informations
                        </Link>
                        <Link to={generatepath('preferences')} className={dist === "preferences" ? "tab-item active" : "tab-item"}>
                            Préférences
                        </Link>
                        <Link to={generatepath('policy')} className={dist === "policy" ? "tab-item active" : "tab-item"}>
                            Légales
                        </Link>
                    </div>
                    <div className="tabs-content">
                        {dist === "general" ? <Editprofile /> : ""}
                        {dist === "preferences" ? <Preferences /> : ""}
                        {dist === "policy" ? <Policy /> : ""}
                    </div>
                </div>
                <div className="how-to">
                    <div className="title">
                        Faqs
                    </div>
                    <div className={"bloc-faq"}>
                        { FaqError ? <NetworkError /> : null }
                        { Faqs === 0 ? <Flatloader /> : Faqs.map(x => <Faq active={activeFaq} set={handleShowFaq} key={x.id} data={x} />)}
                    </div>
                </div>
            </div>
        </Pagecontent>
    );
}

export default Settings;
