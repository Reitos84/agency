import {useLocation} from "react-router-dom";
import PropertyModal from "../PropertyModal";

const PropertyInfo = ({history}) => {

    const params = new URLSearchParams(useLocation().search);
    const pathname = useLocation().pathname;

    let property = params.get('property');

    return(
        <PropertyModal close={() => {
            params.delete('property');
            history.push(pathname + '?' + params.toString());
        }} index={property} />
    )
}

export default PropertyInfo;