import {useEffect, useState} from "react";
import "@css/interest.css";
import Flatloader from "@components/Flatloader";
import InterestService from "@api/entity/InterestService";
import InlineInterest from "@components/InlineInterest";
import Empty from "@components/Empty";
import InterestDetails from "../../components/InterestDetails";
import { useSelector, useDispatch } from "react-redux";
import { removeInterest } from "../../../Core/reducers/bellReducer";

const Interest = () => {

    document.title = "Interêts";
    
    const asInterest = useSelector(state => state.bell.interest);
    const dispatch = useDispatch();

    const interestService = new InterestService();

    const [activatedPan, setActivatedPan] = useState('in-corse');
    const [page, setPage] = useState(1);
    const [listing, setListing] = useState([]);
    const [load, setLoad] = useState(true);
    const [indexedForDetails, setIndexedForDetails] = useState(null);
    const [totalInterest, setTotalInterest] = useState(0);
    const [pageTotal, setPageToTal] = useState(1);
    const [tinyLoad, setTinyLoad] = useState(false);

    useEffect(async () => {
        setLoad(true);
        let result = await interestService.getAll(page, activatedPan === 'in-corse' ? 0 : 1);
        setListing(result['hydra:member']);
        setTotalInterest(result['hydra:totalItems']);
        setPageToTal(Math.ceil(result['hydra:totalItems'] / 30));
        setLoad(false);
        if(asInterest){
            dispatch(removeInterest());
        }
    }, [activatedPan, page]);

    const handleDone = async (index) => {
        setTinyLoad(true);
        let result = await interestService.update(index, {done: true});
        if(result.done){
            setListing(listing.filter(elem => elem.id != index));
            setIndexedForDetails(null);
            setTinyLoad(false);
        }else{
            setTinyLoad(false);
        }
    } 

    return(
        <>
            <div className={"properties-content"}>
                {
                    indexedForDetails ?
                        <InterestDetails done={handleDone} load={tinyLoad} data={indexedForDetails} close={() => setIndexedForDetails(null)} />
                    :
                    ""
                }
                <div className="title">
                    Intérêts
                </div>
                <div className="tab-pan">
                    <span onClick={() => setActivatedPan('in-corse')} className={ activatedPan === 'in-corse' ? "active" : ''}>
                        En attente
                    </span>
                    <span onClick={() => setActivatedPan('done') } className={ activatedPan === 'done' ? "active" : ''}>
                        Terminés
                    </span>
                </div>
                <div className={"interest-content"}>
                    {
                        load ?
                            <Flatloader /> 
                        :
                        
                            listing.length > 0 ?
                            <table>
                                <thead>
                                <tr>
                                    <td>Propriété</td>
                                    <td>Nom du client</td>
                                    <td>Contact</td>
                                    <td>Email</td>
                                    <td>Pays</td>
                                    <td>Date</td>
                                    <td>Actions</td>
                                </tr>
                                </thead>
                                <tbody>
                                    { listing.map(interest => <InlineInterest viewDetails={setIndexedForDetails} data={interest} key={interest.id} />) }
                                </tbody>
                            </table>
                            :
                            <Empty />
                    }
                </div>
            </div>
            <div className={"paginate"}>
                    <div className={"total-items"}>
                        { totalInterest } intérêt{ totalInterest > 1 ? 's' : '' }
                    </div>
                    <div className={"page-switch"}>
                        <button
                            onClick={() => {
                                if(page > 1){
                                    setPage(page - 1);
                                }
                            }}
                            className={ pageTotal === 1 ? "switch-action prev sad" : "switch-action prev"}>
                            Précédente
                        </button>
                        <div className={"current-page"}>
                            <span>Page</span>
                            <input
                                onChange={(e) => {
                                    if(e.currentTarget.value <= pageTotal){
                                        setPage(e.currentTarget.value);
                                    }
                                }}
                                type={"number"}
                                value={page}
                            />
                            sur { pageTotal }
                        </div>
                        <button
                            onClick={() => {
                                if(page < pageTotal){
                                    setPage(page + 1);
                                }
                            }}
                            className={ pageTotal === page ? "switch-action suiv sad" : "switch-action suiv"}>
                            Suivante
                        </button>
                    </div>
                </div>
        </>
    )
}

export default Interest;