import { SearchOutlined } from "@material-ui/icons";
import {useEffect, useRef, useState} from "react";
import '@css/properties.css';
import Flatloader from "@components/Flatloader";
import CategoryService from "@api/entity/CategoryService";
import PropertyService from "@api/entity/PropertyService";
import InlineProperty from "@components/InlineProperty";
import TypeService from "@api/entity/TypeService";
import Empty from "@components/Empty";
import {useDispatch, useSelector} from "react-redux";
import {toggleEditionRelaod} from "@reducers/AddPropertyReducer";
import {fillDataToEdition} from "@reducers/AddPropertyReducer";
import {useLocation, Link} from "react-router-dom";
import { absoluePostLink } from "@navigation/queryfactory.js";

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const Forsale = ({categorySlug, titleText}) => {

    document.title = "A vendre";

    let categoryService = new CategoryService();
    let propertyService = new PropertyService();
    let typeService = new TypeService();

    const [load, setLoad] = useState(true);
    const [list, setList] = useState([]);
    const [searchKey, setSearchKey] = useState('');
    const [category, setCategory] = useState(null);
    const [types, setTypes] = useState([]);
    const [ordering, setOrdering] = useState('');
    const [activatedType, setActivatedType] = useState('');
    const [activatedStatus, setActivatedStatus] = useState('');
    const [page, setPage] = useState(1);
    const [indexed, setIndexed] = useState(0);
    const [totalProperties, setTotalProperties] = useState(0);
    const [pageTotal, setPageToTal] = useState(1);


    const canIRelaod = useSelector(state => state.property.editionReload);
    const dispatch = useDispatch();

    useEffect(async () => {
        categoryService.findWithSlug(categorySlug).then((data) => {
            setCategory(data['hydra:member'][0]);
            propertyService.getProperties(data['hydra:member'][0].id, page).then(data => {
                setLoad(false);
                setList(data['hydra:member']);
                setTotalProperties(data['hydra:totalItems']);
                setPageToTal(Math.ceil(data['hydra:totalItems'] / process.env.REACT_APP_PROPERTY_ITEMS_PER_PAGE));
            });
        });
        typeService.getList().then((response) => {
            setTypes(response['hydra:member']);
        });
    }, []);

    const changeIndexed = (index) =>{
        setIndexed(indexed === index ? 0 : index);
    }

    useEffect(() => {
        fetchMoreData();
    }, [page]);

    const revalidateFinder = () => {
        setPage(1);
        fetchMoreData();
    }

    const handleChangeStatus = async (index, data) => {
        let result = await propertyService.updatePorperty(index, data);
        if(result.id){
            setList(list.filter(element => {
                if(element.id === index){
                    element.status = result.status;
                }
                return element;
            }));
        }
    }

    let { pathname } = useLocation(),
        params = useQuery();

    const handleGoToEditProperty = async (index) => {
        let propertyDetails = await propertyService.getWithId(index);
        dispatch(fillDataToEdition(propertyDetails));
        editRef.current.click();
    }

    const fetchMoreData = async() => {
        setLoad(true);
        if(category){
            if(searchKey.length > 0){
                let firstResults = await propertyService.searchPropertiesWithTitle(category.id, searchKey, page, activatedType, ordering, activatedStatus);
                let secondResults = await propertyService.searchPropertyWithAddress(category.id, searchKey, page, activatedType, ordering, activatedStatus);
                let results = [...new Set(firstResults['hydra:member'].concat(secondResults['hydra:member']))];
                setList([...results]);
                setTotalProperties(firstResults['hydra:totalItems'] + secondResults['hydra:totalItems']);
                setPageToTal(Math.ceil((firstResults['hydra:totalItems'] + secondResults['hydra:totalItems']) / process.env.REACT_APP_PROPERTY_ITEMS_PER_PAGE));
                setLoad(false);
            }else{
                propertyService.getProperties(category.id, page, activatedType, ordering, activatedStatus).then(data => {
                    setList(data['hydra:member']);
                    setTotalProperties(data['hydra:totalItems']);
                    setPageToTal(Math.ceil(data['hydra:totalItems'] / process.env.REACT_APP_PROPERTY_ITEMS_PER_PAGE));
                    setLoad(false);
                });
            }
        }
    }

    if(canIRelaod){
        fetchMoreData();
        dispatch(toggleEditionRelaod());
    }

    const handleChangeOrder = (event) => {
        event.target.value.length > 0 ?
            setOrdering(event.target.value)
        :
            setOrdering(null);
    }

    const handleActivatedChangeStatus = (event) => {
        event.target.value.length > 0 ? 
            setActivatedStatus(event.target.value)
        :
            setActivatedStatus(null);
    }

    const editRef = useRef();

    return(
        <>
            <div onClick={(event) => {
                if(event.target.className !== 'more' || typeof event.target.className != 'object'){
                    if(indexed > 0){
                        setIndexed(0);
                    }
                }
            }} className={"properties-content"}>
                <div className="title">
                    {titleText}
                </div>
                <Link className={"trigger-click"} ref={editRef} to={{pathname: pathname, search: absoluePostLink(params)}}>
                    
                </Link>
                <p>
                    Les propriétés supprimées sont conservées pendant 15 jours.
                </p>
                <br />
                <div className={"filters"}>
                    <div className="search">
                        <SearchOutlined />
                        <input value={searchKey} onChange={(e) => setSearchKey(e.target.value)} type="text" placeholder="Entrer avec un titre ou une localisation" />
                    </div>
                    <div className="filtres">
                        <div className={""}>
                            <select onChange={(e) => setActivatedType(e.target.value)}>
                                <option>Type</option>
                                { types.map(type => <option key={type.id} value={type.id}>{type.title}</option> ) }
                            </select>
                        </div>
                        <div className={""}>
                            <select value={activatedStatus} onChange={handleActivatedChangeStatus}>
                                <option value="">Status</option>
                                <option value={"disponible"}>Disponible</option>
                                <option value={"indisponible"}>Indisponible</option>
                                <option value={"attente"}>En attente</option>
                                <option value={"supprimer"}>Supprimer</option>
                            </select>
                        </div>
                        <div className={""}>
                            <select value={ordering} onChange={handleChangeOrder}>
                                <option value={""}>Trier par</option>
                                <option value={"desc"}>Prix Desc</option>
                                <option value={"asc"}>Prix Asc</option>
                            </select>
                        </div>
                    </div>
                    <button onClick={revalidateFinder}>Rechercher</button>
                </div>

                {
                    load ?
                        <Flatloader />
                        :
                        list.length > 0 ?
                            <table>
                                <thead>
                                <tr>
                                    <td>Titre</td>
                                    <td>Type</td>
                                    <td>Localisation</td>
                                    <td>Prix</td>
                                    <td>Vue</td>
                                    <td>Status</td>
                                    <td>Ajouter le</td>
                                    <td>Actions</td>
                                </tr>
                                </thead>
                                <tbody>
                                {list.map(property => <InlineProperty goToEdition={handleGoToEditProperty} indexed={indexed} toggleIndexed={changeIndexed} changeStatus={handleChangeStatus} key={property.id} property={property} />)}
                                </tbody>
                            </table>
                            :
                            <Empty />
                }

            </div>
            <div className={"paginate"}>
                <div className={"total-items"}>
                    { totalProperties } propriété{ totalProperties > 1 ? 's' : '' }
                </div>
                <div className={"page-switch"}>
                    <button
                        onClick={() => {
                            if(page > 1){
                                setPage(page - 1);
                            }
                        }}
                        className={ pageTotal === 1 ? "switch-action prev sad" : "switch-action prev"}>
                        Précédente
                    </button>
                    <div className={"current-page"}>
                        <span>Page</span>
                        <input
                            onChange={(e) => {
                                if(e.currentTarget.value <= pageTotal){
                                    setPage(e.currentTarget.value);
                                }
                            }}
                            type={"number"}
                            value={page}
                        />
                        sur { pageTotal }
                    </div>
                    <button
                        onClick={() => {
                            if(page < pageTotal){
                                setPage(page + 1);
                            }
                        }}
                        className={ pageTotal === page ? "switch-action suiv sad" : "switch-action suiv"}>
                        Suivante
                    </button>
                </div>
            </div>
        </>
    )
}

export default Forsale;