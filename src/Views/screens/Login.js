import '@css/login-page.css';
import {AlternateEmail, Lock, VisibilityOutlined, VisibilityOffOutlined} from "@material-ui/icons";
import { connection } from "@core/reducers/TokenReducer";
import { useDispatch } from 'react-redux';
import loader from "@assets/loader/785.svg";
import { useState } from "react";
import AuthRequest from "@auth/AuthRequest";
import { Link } from "react-router-dom";

const Login = () => {

    document.title = "Accéder à mon compte";

    const [load, setLoad] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [message, setMessage] = useState('');
    const [showPassword, setShowPassword] = useState(false);

    const toggleChangePassword = () => setShowPassword(state => !state);

    const dispatch = useDispatch();

    const handleConnect = () => {
        if(email.length > 0 && password.length > 0){
            setLoad(true);
            const requestObj = new AuthRequest();
            const response = requestObj.login(email, password);
            response.then(res => {
                if(res.token){
                    dispatch(connection(res));
                    localStorage.setItem('token', res.token);
                    localStorage.setItem('revalidate', res.refresh_token);
                }else{
                    setMessage('Votre email ou mot de passe est incorrect')
                    setLoad(false);
                }
            }).catch(error => {
                console.log(error);
            });
        }else{
            setMessage('Entrer un email et un mot de passe');
        }
    }

    const handleChangeEmail = (event) => {
        setEmail(event.currentTarget.value);
    }

    const handleChangePassword = (event) => {
        setPassword(event.currentTarget.value);
    }

    return(
        <div className="login">
            <div className="image-background">
                <div className={"image-container"}>
                    <div className="logo">
                        Abidjan Habitât
                    </div>
                </div>
            </div>
            <div className="login-side">
                <div className="login-content">
                    <div className={"title"}>
                        Accédez à votre compte
                    </div>
                    <div className="login-box">
                        <AlternateEmail style={{color: "#727272"}} />
                        <input value={email} onChange={handleChangeEmail} type="text" name="email" placeholder="Email"/>
                    </div>
                    <div className="login-box">
                        <Lock style={{color: "#727272"}} />
                        <input value={password} onChange={handleChangePassword} type={ showPassword ? "text" : "password"} name="email" placeholder="Mot de passe"/>
                        {
                            showPassword ? 
                                <VisibilityOffOutlined onClick={toggleChangePassword} className='eye'/>
                            :
                                <VisibilityOutlined onClick={toggleChangePassword} className='eye'/>
                        }
                        
                    </div>
                    <div className="message">
                        {message}
                    </div>
                    <Link to="/reset" className="sad right">
                        Mot de passe oublié ?
                    </Link>
                    <button className="btn-connect" onClick={handleConnect}>
                        { load ? <img src={loader} /> : ""}
                        Connexion
                    </button>
                    <div className={"ask-question"}>
                        Vous n'avez pas de compte ? <Link to={'/register'}>Inscrivez-vous</Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login;