import { Close } from '@material-ui/icons';
import { createPortal } from 'react-dom';
import {useDispatch} from "react-redux";
import {revertUpdateState} from "../../Core/reducers/AddPropertyReducer";
import '@css/add-home.css';

const Postmodal = ({close, children}) => {

    const dispatch = useDispatch();

    return createPortal(
        <>
            <div className="post-modal">
                <div className="md-content">
                    <div className="md-head">
                        <Close onClick={() => {
                            close();
                            dispatch(revertUpdateState());
                        } } titleAccess="Fermer" className="close" />
                    </div>
                    <div className="md-body">
                        {children}
                    </div>
                </div>
            </div>
        </>,
        document.getElementById("property_info_root")
    )
}

export default Postmodal;