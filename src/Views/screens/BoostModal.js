import { Close } from '@material-ui/icons';
import { createPortal } from 'react-dom';
import {useLocation} from "react-router-dom";
import AddBoost from "./boost/AddBoost";

const BoostModal = ({history}) => {

    const params = new URLSearchParams(useLocation().search);
    const pathname = useLocation().pathname;

    let b = params.get('boost');

    const handleCloseModal = () => {
        params.delete('boost');
        history.push(pathname + '?' + params.toString());
    }

    return createPortal(
        b ?
            <>
                <div className="post-modal">
                    <div className="md-content">
                        <div className="md-head">
                            <Close onClick={handleCloseModal} titleAccess="Fermer" className="close" />
                        </div>
                        <div className="md-body">
                            <AddBoost />
                        </div>
                    </div>
                </div>
            </> : <></>,
        document.getElementById("create_boost_modal")
    )
}

export default BoostModal;