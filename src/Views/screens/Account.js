import AccountRouter from "@navigation/AccountRouter";
import Sidebar from "@navigation/Sidebar";
import Header from "@components/Header";
import "@css/dashboard.css";
import FullLoader from '@components/FullLoader';
import { useSelector, useDispatch } from 'react-redux';
import { useEffect } from "react";
import UserProvider from "@auth/UserProvider";
import { Redirect } from "react-router";
import { disconnection } from "@reducers/TokenReducer";
import ACCESS_ROLE from "@constants/access_roles";
import { fill, validated } from "@reducers/AgencyReducer";
import TwoFactorView from "@screens/TwoFactorView";
import { newInterest, removeInterest } from "../../Core/reducers/bellReducer";
import InterestService from "../../Services/api/entity/InterestService";

const Account = () => {
    const store = useSelector(state => state);
    const dispatch = useDispatch();
    const interestService = new InterestService();

    const goBack = () => {
        dispatch(disconnection());
        <Redirect to="/login" />
    }

    const asInterest = () => {
        interestService.getAll(1).then(result => {
            if(result['hydra:totalItems'] > 0){
                dispatch(newInterest());
            }else{
                dispatch(removeInterest());
            }
        });
    }

    useEffect(() => {
        asInterest();
        if(store.agency.details === null){
            let provider = new UserProvider();
            provider.Provide(localStorage.getItem('token')).then(response => {
                if(response.success === false){
                    goBack();
                }
                const {roles} = response;
                if(roles.find(x => x === ACCESS_ROLE)){
                    if(response.isSecured !== true){
                        dispatch(validated());
                    }
                    dispatch(fill(response))
                }else{
                    goBack();
                }
            });
        }
    }, [store.agency.infos]);

    if(store.agency.details === null){
        return(
            <div className="dashboard">
                { store.agency.details ? "" : <FullLoader />}
            </div>
        )
    }else if(store.agency.twoFactor === false){
        return(
            <TwoFactorView />
        )
    }else{
        return(
            <div className="dashboard">
                <Sidebar />
                <Header />
                <div className="container">
                    <AccountRouter />
                </div>
            </div>
        )
    }
}

export default Account;