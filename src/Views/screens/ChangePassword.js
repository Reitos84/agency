import {Link} from "react-router-dom";
import '@css/login-page.css';
import {LockOutlined, VisibilityOffOutlined, VisibilityOutlined} from "@material-ui/icons";
import {useEffect, useRef, useState} from "react";
import loader from "@assets/loader/785.svg";
import expired from "@assets/icon/expired.png";
import ResetPasswordService from "@api/entity/ResetPasswordService";
import {useLocation} from "react-router-dom";
import {useQuery} from "@lodash/utility";
import bigLoader from "@assets/loader/786.gif";

const ChangePassword = () => {
    const [message, setMessage] = useState('');
    const [load, setLoad] = useState(false);
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [error, setError] = useState(false);
    const [showPass, setShowPass] = useState(false);
    const [bigLoad, setBigLoad] = useState(true);
    const [successMessage, setSuccessMessage] = useState('');

    const resetPasswordService = new ResetPasswordService();

    const params = useQuery(useLocation().search);

    useEffect(() => {
        resetPasswordService.verifyParams({
            resetkey: params.get('hash'),
            selector: params.get('u')
        }).then(data => {
            if(!data.success){
                setError(true);
            }
            setBigLoad(false);
        })
    }, []);

    const handleChangePassword = (event) => {
        setPassword(event.currentTarget.value);
    }

    const handleChangeConfirmPassword = (event) => {
        setConfirmPassword(event.currentTarget.value);
    }

    const loginRef = useRef();

    const changePassword = () => {
        if(password === confirmPassword){
            setMessage('');
            setLoad(true);
            resetPasswordService.changePassword({
                password: password,
                resetkey: params.get('hash'),
                selector: params.get('u')
            }).then(data => {
                if(data.success){
                    setSuccessMessage('Votre mot de passe a été modifier');
                    setLoad(false);
                    setTimeout(() => {
                        loginRef.current.click();
                    }, 5000);
                }
            })
        }else{
            setMessage('Les mots de passe ne correspondent pas')
        }
    }

    return(
        <div className="login">
            <div className="image-background">
                <div className={"image-container"}>
                    <div className="logo">
                        Abidjan Habitât
                    </div>
                </div>
            </div>
            <div className="login-side">
                <div className="login-content">
                    {
                        bigLoad ?
                            <img className={"load-centered"} src={bigLoader} alt={"verification"} />
                            :

                        error ?
                            <>
                                <div className={"sent"}>
                                    <div className={"thumb"}>
                                        <img src={expired}  alt={"lien expiré"} />
                                    </div>
                                    <div className={"title"}>Votre lien est expiré</div>
                                    <div className={"exp"}>
                                        Le lien le réinitialisation est valable pour une heure, clicquez sur le bouton ci-dessous pour obtenir un autre lien
                                    </div>
                                    <Link to={"/reset"} style={{textDecoration: 'none'}}>
                                        <button onClick={changePassword} className="btn-connect">
                                            Mot de passe oublié
                                        </button>
                                    </Link>
                                    <Link to="/" className="sad">
                                        Se connecter
                                    </Link>
                                </div>
                            </>
                            :
                            <>
                                <div className="title">
                                    Choisir un mot de passe
                                </div>
                                <div className="message">
                                    {message}
                                </div>
                                <div className={"message success"}>
                                    { successMessage }
                                </div>
                                <div className="login-box">
                                    <LockOutlined style={{color: "#727272"}} />
                                    {
                                        showPass ? <VisibilityOffOutlined onClick={() => setShowPass(!showPass)} className={"eye"} /> : <VisibilityOutlined onClick={() => setShowPass(!showPass)} className={"eye"} />
                                    }
                                    <VisibilityOutlined onClick={() => setShowPass(!showPass)} className={"eye"} />
                                    <input value={password} onChange={handleChangePassword} type={ showPass ? "text" : "password"} name="password" placeholder="Saisir votre mot de passe"/>
                                </div>
                                <div className="login-box">
                                    <LockOutlined style={{color: "#727272"}} />
                                    <input value={confirmPassword} onChange={handleChangeConfirmPassword} type={ showPass ? "text" : "password"} name="password" placeholder="Confirmer votre mot de passe"/>
                                </div>
                                <button onClick={changePassword} className="btn-connect">
                                    { load ? <img src={loader} alt={"veillez patienter"}/> : ""}
                                    Sauvegarder
                                </button>
                                <Link ref={loginRef} to="/" className="sad">
                                    Se connecter
                                </Link>
                            </>
                    }

                </div>
            </div>
        </div>
    )
}

export default ChangePassword;