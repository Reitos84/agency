import '@css/two-factor.css';
import shield from '@assets/icon/user-shield.png';
import { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import UserProvider from '@services/auth/UserProvider';
import { validated } from "@reducers/AgencyReducer";
import loader from "@assets/loader/785.svg";

const TwoFactorView = () => {

    const [code, setCode] = useState('');
    const [message, setMessage] = useState('');
    const [load, setLoad] = useState(false);
    const [resendText, setResendText] = useState(false);

    let provider = new UserProvider();
    const user = useSelector(state => state.agency.details);
    const dispatch = useDispatch();

    const requestEmail = useCallback(() => {
        provider.requestEmail(user.email);
    });

    const verifyCode = useCallback(() => {
        setLoad(true);
        provider.verifyCode(code).then(response => {
            if(response.success){
                dispatch(validated());
                setTimeout(() => {
                    setResendText(true);
                }, 15000);
            }else{
                setLoad(false);
                setMessage('Votre code de confirmation est incorrect');
            }
        });
    });

    useEffect(() => {
        requestEmail();
    }, []);

    const handleChangeCode = (e) => {
        setCode(e.target.value);
    }

    return(
        <div className="two-factor-content">
            <div className="two-factor-view">
                <img alt={"secure concept"} src={shield} />
                <div className="title">
                    Anthentifier votre compte
                </div>
                <div>    
                    La protection de vos billets est notre priorité absolue, veuillez confirmer votre compte en saisissant le code d'autorisation envoyé à <strong>{user.email}</strong>
                </div>
                <div className="message">
                    {message}
                </div>
                <div className="box">
                    <input value={code} onChange={handleChangeCode} type="text" name="code" placeholder="Saisir votre code ici" />
                </div>
                <button onClick={verifyCode} className="valid">
                    {load ? <img src={loader} alt={"Veillez patienter..."} /> : <></>}
                    Confirmer
                </button>
                <div className="extra">
                   <span onClick={() => { setResendText(false); requestEmail(); }} className={ resendText ? "resend view" :  "resend"}>Renvoyer le code</span>
                </div>
            </div>
        </div>
    )
}

export default TwoFactorView;