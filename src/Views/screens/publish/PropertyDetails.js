import poolIcon from "@assets/icon/pool.png";
import balconyIcon from "@assets/icon/balcony.png";
import elevatorIcon from "@assets/icon/elevator.png";
import escalatorIcon from "@assets/icon/escalator.png";
import fountainIcon from "@assets/icon/balcony.png";
import garageIcon from "@assets/icon/garage.png";
import gardenIcon from "@assets/icon/garden.png";
import generatorIcon from "@assets/icon/generator.png";
import libraryIcon from "@assets/icon/library.png";
import officeIcon from "@assets/icon/office.png";
import armchairIcon from "@assets/icon/armchair.png";
import blenderIcon from "@assets/icon/blender.png";
import fridgeIcon from "@assets/icon/fridge.png";
import microwaveIcon from "@assets/icon/microwave.png";
import internetIcon from "@assets/icon/internet.png";
import stepperIcon from "@assets/icon/stepper.png";
import homeSafe from "@assets/icon/home_safety.png";
import airconditionnerIcon from "@assets/icon/air-conditionner.png";
import PropertyService from "@services/api/entity/PropertyService";
import PictureService from "@services/api/entity/PictureService";
import {useRef, useState} from "react";
import { useSelector, useDispatch } from "react-redux";
import Whiteloader from "@components/Whiteloader";
import {Link, useLocation} from "react-router-dom";
import {addAmanties} from "@reducers/AddPropertyReducer";

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const PropertyDetails = () => {

    const allData = useSelector(state => state.property.data);

    const currentData = allData.amanties ? allData.amanties : {};
    const [title, setTitle] = useState(currentData ? currentData.title : '');
    const [price, setPrice] = useState(currentData.price ? currentData.price : '');
    const [frequency, setFrequency] = useState(currentData.frequency ? currentData.frequency : 'jour');
    const [description, setDescription] = useState(currentData.description ? currentData.description : '');
    const [bedroom, setBedroom] = useState(currentData.bedroom ? currentData.bedroom : 0);
    const [saloon, setSaloon] = useState(currentData.saloon ? currentData.saloon : 0);
    const [kitchen, setKitchen] = useState(currentData.kitchen ? currentData.kitchen : 0);
    const [shower, setShower] = useState(currentData.shower ? currentData.shower : 0);
    const [pool, setPool] = useState(currentData.pool ? currentData.pool : null);
    const [balcony, setBalcony] = useState(currentData.balcony ? currentData.balcony : null);
    const [gymnasium, setGymnasium] = useState(currentData.gymnasium ? currentData.gymnasium : null);
    const [garden, setGarden] = useState(currentData.garden ? currentData.garden : null);
    const [generator, setGenerator] = useState(currentData.generator ? currentData.generator : null);
    const [fountain, setFountain] = useState(currentData.fountain ? currentData.fountain : null);
    const [garage, setGarage] = useState(currentData.garage ? currentData.garage : null);
    const [office, setOffice] = useState(currentData.garage ? currentData.office : null);
    const [stair, setStair] = useState(currentData.stair ? currentData.stair : null);
    const [elevator, setElevator] = useState(currentData.elevator ? currentData.elevator : null);
    const [library, setLibrary] = useState(currentData.library ? currentData.library : null);
    const [buildAt, setBuildAt] = useState(currentData.buildAt ? currentData.buildAt : "");
    const [internet, setInternet] = useState(currentData.internet ? currentData.internet : null);
    const [microwave, setMicrowave] = useState(currentData.microwave ? currentData.microwave : null);
    const [armchair, setArmchair] = useState(currentData.armchair ? currentData.armchair : null);
    const [refrigerator, setRefrigerator] = useState(currentData.refrigerator ? currentData.refrigerator : null);
    const [blender, setBlender] = useState(currentData.blender ? currentData.blender : null);
    const [airConditionner, setAirConditionner] = useState(currentData.airConditionner ? currentData.airConditionner : null);
    const [space, setSpace] = useState(currentData.space ? currentData.space : null);
    const [piece, setPiece] = useState(currentData.piece ? currentData.piece : null);
    const [officeType, setOfficeType] = useState(currentData.officeType ? currentData.officeType : null);
    const [landStatus, setLandStatus] = useState(currentData.landStatus ? currentData.landStatus : null);
    const [officePosition, setOfficePosition] = useState(currentData.officePosition ? currentData.officePosition : 'down');
    const [floor, setFloor] = useState(currentData.floor ? currentData.floor : null);
    const [apartments, setApartments] = useState(currentData.apartments ? currentData.apartments : null);
    const [secure, setSecure] = useState(currentData.secure ? currentData.secure : null);
    const [load, setLoad] = useState(false);
    const [message, setMessage] = useState('');
    const [log, setLog] = useState('');

    let {property, agency} = useSelector(state => state);
    const {category, type} = property.data;
    const dispatch = useDispatch();

    const handleSaveProperty = () => {
        setMessage('');
        if(
            title.length > 0 &&
            parseInt(price) > 0 &&
            description.length > 0 
        ){
            setLoad(true);
            setLog('Publication en cours...');
            let data = {
                title: title,
                price: parseInt(price),
                description: description,
                bedroom: parseInt(bedroom),
                saloon: parseInt(saloon),
                kitchen: parseInt(kitchen),
                frequency: category.slug === "rent" ? frequency : null,
                shower: parseInt(shower),
                pool: pool,
                balcony: balcony,
                gymnasium: gymnasium,
                garden: garden,
                generator: generator,
                fountain: fountain,
                garage: garage,
                office: office,
                stair: stair,
                elevator: elevator,
                library: library,
                refrigerator: refrigerator,
                internet: internet,
                microwave: microwave,
                officeType: officeType,
                officePosition: officePosition,
                landStatus: landStatus,
                armchair: armchair,
                airConditionner: airConditionner,
                blender: blender,
                floor: floor ? parseInt(floor) : null,
                apartments: apartments ? parseInt(apartments) : null,
                piece: piece ? parseInt(piece) : null,
                space: space,
                buildAt: buildAt !== "" ? parseInt(buildAt) : null
            };
            data = {
                ...property.data, 
                ...data
            };
            data.category = category['@id'];
            data.type = type['@id'];
            data.lat = parseFloat(data.lat);
            data.lng = parseFloat(data.lng);
            data.pictures = null; 
            if(!allData.isEditing){
                data.agency = `/api/users/${agency.details.id}`;
                data.status = "disponible";
                data.deleted = false;
            }
            const propertyService = new PropertyService();
            const pictureService = new PictureService();
            if(allData.isEditing){
                propertyService.updatePorperty(allData.id, data).then(response => {
                    if(response.id){
                        setLog('Mise à jour des photos...');
                        property.data.pictures.forEach(picture => {
                            pictureService.update(picture.id, {property : response['@id']});
                        });
                        setLog('');
                    }
                    setTimeout(() => {
                        redirectRef.current.click();
                    }, 3000);
                })
            }else{
                propertyService.post(data).then((response) => {
                    if(response.status === 201){
                        property.data.pictures.forEach(picture => {
                            pictureService.update(picture.id, {property : response.data['@id']});
                        });
                        setLog('');
                        setTimeout(() => {
                            redirectRef.current.click();
                        }, 3000);
                    }else{
                        setLog('');
                        setLoad(false);
                        setMessage('Publication non éffectué, veillez bien remplir le formulaire');
                    }
                });
            }
        }else{
            setMessage('Veillez bien remplir le formulaire')
        }
    }

    const handleReturn = () => {
        let data = {
            title: title,
            price: parseInt(price),
            description: description,
            bedroom: parseInt(bedroom),
            saloon: parseInt(saloon),
            kitchen: parseInt(kitchen),
            frequency: category.slug === "rent" ? frequency : null,
            shower: parseInt(shower),
            pool: pool,
            balcony: balcony,
            gymnasium: gymnasium,
            garden: garden,
            generator: generator,
            fountain: fountain,
            garage: garage,
            office: office,
            stair: stair,
            elevator: elevator,
            library: library,
            refrigerator: refrigerator,
            internet: internet,
            microwave: microwave,
            armchair: armchair,
            blender: blender,
            landStatus: landStatus,
            airConditionner: airConditionner,
            officeType: officeType,
            piece: piece,
            officePosition: officePosition,
            space: space,
            secure: secure,
            floor: floor ? parseInt(floor) : null,
            apartments: apartments ? parseInt(apartments) : null,
            buildAt: buildAt !== "" ? parseInt(buildAt) : null
        };
        dispatch(addAmanties(data));
    }

    let params = useQuery();
    params.set('publish', 'done');
    let params2 = useQuery();
    params2.set('publish', 'gallery');
    let pathname = useLocation().pathname;

    const redirectRef = useRef(null);

    return(
        <div className={"property-detail"}>
            <div className={"property-detail-info"}>
                <div className={"title"}>
                    Détails de votre propriété
                </div>
                <p>
                    Les champs avec (*) sont obligatoires.
                </p>
                <div className={"forms"}>
                    <div className={"split"}>
                        <div className={"box"}>
                            <label>Titre *</label>
                            <input value={title} onChange={event => setTitle(event.target.value)} type={"text"} />
                        </div>
                        <div className={"box"}>
                            <label>Montant *</label>
                            <input value={ price } onChange={event => setPrice(event.target.value)} type={"number"}/>
                            <strong>{ process.env.REACT_APP_CURENCY }</strong>
                        </div>
                        <div style={{display: category.slug === "rent" ? "block" : "none"}} className={"box"}>
                            <label>Frequence de paiement</label>
                            <select value={frequency} onChange={event => setFrequency(event.target.value)}>
                                <option value={"jour"}>Journalier</option>
                                <option value={"semaine"}>Hebdomadaire</option>
                                <option value={"mois"}>Mensuel</option>
                                <option value={"an"}>Annuel</option>
                            </select>
                        </div>
                        <div className={"box"}>
                            <label>Superficie *</label>
                            <input value={space} onChange={event => setSpace(event.target.value)} type={"number"}/>
                            <strong>m<sup>2</sup></strong>
                        </div>
                        {
                            allData.type.slug === 'office' || allData.type.slug === 'commercial' ?
                                <>
                                    <div className={"box"}>
                                        <label>Nombre de pieces</label>
                                        <input type={"number"} value={piece} onChange={event => setPiece(event.target.value)} />
                                    </div>
                                </>
                                :
                                <></>

                        }
                        {
                            allData.type.slug === 'office' ?
                                <>
                                    <div className={"box"}>
                                        <label>Type de bureau *</label>
                                        <select value={officeType} onChange={event => setOfficeType(event.target.value)}>
                                            <option value={"privé"}>Privé</option>
                                            <option value={"partagé"}>Partagé</option>
                                        </select>
                                    </div>
                                    <div className={"box"}>
                                        <label>A l'étage</label>
                                        <select value={officePosition} onChange={(e) => setOfficePosition(e.currentTarget.value)}>
                                            <option value={"up"}>A l'etage</option>
                                            <option value={"down"}>Au rez de chaussée</option>
                                        </select>
                                    </div>
                                </>
                            :
                                <></>
                        }
                        {
                            allData.type.slug === 'land' ?
                                <div className={"box"}>
                                    <label>Status du terrain</label>
                                    <select value={landStatus} onChange={event => setLandStatus(event.target.value)}>
                                        <option value={"Attestation villageoise"}>Attestation villageoise</option>
                                        <option value={"ACD"}>ACD</option>
                                        <option value={"Titre foncié"}>Titre foncié</option>
                                        <option value={"Viabilisé"}>Viabilisé</option>
                                        <option value={"Décapé"}>Décapé</option>
                                    </select>
                                </div>
                                :
                                ""
                        }
                        {
                            allData.type.slug === 'building' ?
                                <>
                                    <div className={"box"}>
                                        <label>Nombre d'étage</label>
                                        <input onChange={e => setFloor(e.currentTarget.value)} type={"number"} value={floor} />
                                    </div>
                                    <div className={"box"}>
                                        <label>Nombre d'appartement</label>
                                        <input onChange={e => setApartments(e.currentTarget.value)} type={"number"} value={apartments} />
                                    </div>
                                </>
                                :
                                <></>

                        }
                        {
                            allData.type.slug === 'appartment' || allData.type.slug === 'house' || allData.type.slug === 'villa' ?
                            <>
                                <div className={"box"}>
                                    <label>Chambre *</label>
                                    <input value={bedroom} onChange={event => setBedroom(event.target.value)} type={"number"} />
                                </div>
                                <div className={"box"}>
                                    <label>Salon *</label>
                                    <input value={saloon} onChange={event => setSaloon(event.target.value)} type={"number"} />
                                </div>
                                <div className={"box"}>
                                    <label>Cuisine *</label>
                                    <input value={kitchen} onChange={event => setKitchen(event.target.value)} type={"number"} />
                                </div>
                                <div className={"box"}>
                                    <label>Douches *</label>
                                    <input value={shower} onChange={event => setShower(event.target.value)} type={"number"} />
                                </div>
                            </>
                            :
                            ""
                        }
                        {
                            allData.type.slug !== 'land' ?
                            <div className={"box"}>
                                <label>Année de construction</label>
                                <input value={buildAt} onChange={event => setBuildAt(event.target.value)} type={"number"} />
                            </div>
                            :
                            ""
                        }
                    </div>
                    {
                        allData.type.slug !== 'land' ?
                            <div className={"box"}>
                                <div>Que contient votre propriété en plus</div>
                                <div className={"amanties"}>
                                    {
                                        allData.type.slug === 'house' || allData.type.slug === 'villa' || allData.type.slug === 'appartment' || allData.type.slug === 'building' ?
                                            <>
                                                <span onClick={() => setPool(!pool)} className={pool ? "active" : ""}>
                                                    <img alt={"Icone de piscine"} src={poolIcon} />
                                                    Piscine
                                                </span>
                                                <span onClick={() => setBalcony(!balcony)} className={balcony ? "active" : ""}>
                                                    <img alt={"Icone de balcon"} src={balconyIcon} />
                                                    Balcon
                                                </span>
                                                <span className={gymnasium ? "active" : ""} onClick={() => setGymnasium(!gymnasium)}>
                                                    <img alt={"Icone de gymnase"} src={stepperIcon} />
                                                    Gymnase
                                                </span>
                                                <span className={garden ? "active" : ""} onClick={() => setGarden(!garden)}>
                                                    <img alt={"Icone de jardin"} src={gardenIcon} />
                                                    Jardin
                                                </span>
                                                <span className={generator ? "active" : ""} onClick={() => setGenerator(!generator)}>
                                                    <img alt={"Icone de groupe électronique"} src={generatorIcon} />
                                                    Groupe électrogène
                                                </span>
                                                <span className={fountain ? "active" : ""} onClick={() => setFountain(!fountain)}>
                                                    <img alt={"Icone de fontaine"} src={fountainIcon} />
                                                    Fontaine
                                                </span>
                                                <span className={garage ? "active" : ""} onClick={() => setGarage(!garage)}>
                                                    <img alt={"Icone de Garage"} src={garageIcon} />
                                                    Parking
                                                </span>
                                                <span className={office ? "active" : ""} onClick={() => setOffice(!office)}>
                                                    <img alt={"Icone de bureau"} src={officeIcon} />
                                                    Bureau
                                                </span>
                                                <span className={stair ? "active" : ""} onClick={() => setStair(!stair)}>
                                                    <img alt={"Icone d'escalier"} src={escalatorIcon} />
                                                    Escalier
                                                </span>
                                                <span className={elevator ? "active" : ""} onClick={() => setElevator(!elevator)}>
                                                    <img alt={"Icone d'ascenceur"} src={elevatorIcon} />
                                                    Ascenceur
                                                </span>
                                                <span className={library ? "active" : ""} onClick={() => setLibrary(!library)}>
                                                    <img alt={"Icone de bibliothèque"} src={libraryIcon} />
                                                    Bibliothèque
                                                </span>
                                            </>
                                        :
                                            ""
                                    }
                                    {
                                        allData.type.slug === 'building' ?
                                            <>
                                                <span className={secure ? "active" : ""} onClick={() => setSecure(!secure)}>
                                                    <img alt={"Icone d'internet"} src={homeSafe}/>
                                                    Sécurisé
                                                </span>
                                                <span className={internet ? "active" : ""} onClick={() => setInternet(!internet)}>
                                                    <img alt={"Icone d'internet"} src={internetIcon}/>
                                                    Accès internet
                                                </span>
                                            </>
                                            :
                                            <></>
                                    }
                                    {
                                        allData.type.slug === 'office' || allData.type.slug === 'commercial' ?
                                            <>
                                                <span className={generator ? "active" : ""} onClick={() => setGenerator(!generator)}>
                                                    <img alt={"Icone de groupe électronique"} src={generatorIcon} />
                                                    Groupe électrogène
                                                </span>
                                                <span className={stair ? "active" : ""} onClick={() => setStair(!stair)}>
                                                    <img alt={"Icone d'escalier"} src={escalatorIcon} />
                                                    Escalier
                                                </span>
                                                <span className={airConditionner ? "active" : ""} onClick={() => setAirConditionner(!airConditionner)}>
                                                    <img alt={"Icone de climatiseur"} src={airconditionnerIcon} />
                                                    Climatisation
                                                </span>
                                                <span className={elevator ? "active" : ""} onClick={() => setElevator(!elevator)}>
                                                    <img alt={"Icone d'ascenceur"} src={elevatorIcon} />
                                                    Ascenceur
                                                </span>
                                            </>
                                        :
                                            <></>
                                    }
                                </div>
                            </div>
                        :
                            ""
                    }
                    {
                        category.slug === "rent" && (allData.type.slug === 'appartment' || allData.type.slug === 'studio' || allData.type.slug === 'villa' || allData.type.slug === 'house') ?
                            <div className={"box"}>
                                <div>Equipements</div>
                                <div className={"amanties"}>
                                    <span className={internet ? "active" : ""} onClick={() => setInternet(!internet)}>
                                        <img alt={"Icone d'internet"} src={internetIcon}/>
                                        Accès internet
                                    </span>
                                    <span className={armchair ? "active" : ""} onClick={() => setArmchair(!armchair)}>
                                        <img alt={"Icone d'internet"} src={armchairIcon}/>
                                        Meubles
                                    </span>
                                    <span className={blender ? "active" : ""} onClick={() => setBlender(!blender)}>
                                        <img alt={"Icone d'internet"} src={blenderIcon}/>
                                        Mixeur
                                    </span>
                                    <span className={refrigerator ? "active" : ""}
                                                    onClick={() => setRefrigerator(!refrigerator)}>
                                        <img alt={"Icone d'internet"} src={fridgeIcon}/>
                                        Réfrigérateur
                                    </span>
                                    <span className={microwave ? "active" : ""} onClick={() => setMicrowave(!microwave)}>
                                        <img alt={"Icone d'internet"} src={microwaveIcon}/>
                                        Micro-onde
                                    </span>
                                </div>
                            </div>
                            :
                            ""
                    }
                    <div className={"box"}>
                        <label>Description * { description ? `${description.length}/100` : '(moins de 100 caractères)'}</label>
                        <div>
                            Veillez à saisir tous les détails concernant votre propriété
                        </div>
                        <br />
                        <textarea value={description} onChange={event => setDescription(event.target.value)}/>
                    </div>
                    <div className={"footer"}>
                        <div style={{color: "red"}}>
                            {message}
                        </div>
                        <div className={"log"}>
                            {log}
                        </div>
                        <div className={"btn-group"}>
                            <Link className={"distancer"} to={{pathname: pathname, search: params2.toString()}}>
                                <button onClick={handleReturn} className={"return"}>Arrière</button>
                            </Link>
                            <button onClick={handleSaveProperty}>
                                { load ? <Whiteloader /> : "" }
                                {
                                    allData.isEditing ? 'Enregistrer' : 'Publier'
                                }
                            </button>
                            <Link ref={redirectRef} to={{pathname: pathname, search: params.toString()}}>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <div className={"image-size"}>

            </div>
        </div>
    )
}

export default PropertyDetails;