import styled from "@emotion/styled";
import {DoneOutlined} from "@material-ui/icons";
import {Link, useLocation} from "react-router-dom";
import {primary} from "@constants/color";
import {anotherbold} from "@constants/fonts";
import {useDispatch} from "react-redux";
import {emptyReducers} from "@reducers/AddPropertyReducer";
import {useEffect} from "react";
import {toggleEditionRelaod} from "@reducers/AddPropertyReducer";
import succes from "@assets/icon/success.png";

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const Published = () => {

    const dispatch = useDispatch();

    let params = useQuery();
    params.delete('publish');
    let pathname = useLocation().pathname;

    useEffect(() => {
        dispatch(emptyReducers());
        if(pathname === '/for-sale' || pathname === '/for-rent'){
            dispatch(toggleEditionRelaod());
        }
    }, []);

    return(
        <div className={"completed"}>
            <div className={"icon"}>
                <img src={succes} alt={"reussie"} />
            </div>
            <div className={"title"}>Publication effectuée</div>
            <Link to={{pathname: pathname, search: params.toString()}}>
                Terminer
            </Link>
        </div>
    )
}

export default Published;