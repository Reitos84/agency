import styled from "@emotion/styled";
import { semibold } from "@constants/fonts";
import GoogleMapReact from "google-map-react";
import { useEffect, useState } from "react";
import { HelpOutlined, CloseOutlined, ChevronRight, MenuOutlined } from "@material-ui/icons";
import what_1 from "@assets/icon/what3words-1.svg";
import what_2 from "@assets/icon/what3words-2.svg";
import what_3 from "@assets/icon/what3words-3.svg";
import what_4 from "@assets/icon/what3words-4.svg";
import whatLogo from "@assets/icon/what3word.png";
import pined from "@assets/icon/pined.png";
import {useDispatch, useSelector} from "react-redux";
import { addAddress } from "@reducers/AddPropertyReducer";
import { Link, useLocation } from "react-router-dom";
import { GOOGLE_MAPS } from "@constants/google_maps_key";
import { WHATWORD_API_KEY } from "@constants/what3word_key";

const whatApi = require('@what3words/api');
whatApi.setOptions({ key: process.env.REACT_APP_WHAT3WORD_API_KEY });

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const PropertyAdress = () => {

    const currentData = useSelector(state => state.property.data);

    const [coords, setCoords] = useState({});
    const [selectedCoords, setSelectedCoords] = useState(
        currentData.lat ? {
            lat: currentData.lat,
            lng: currentData.lng
        } : null
    );

    const [what3wordAbout, setWhat3wordAbout] = useState(false);
    const [currentSlide, setCurrentSlide] = useState(0);
    const [words, setWords] = useState(currentData.what3words);
    const [city, setCity] = useState(currentData.city ? currentData.city : '');
    const [country, setCountry] = useState('Côte D\'Ivoire');
    const [address, setAddress] = useState(currentData.address ? currentData.address : '');
    const [area, setArea] = useState(currentData.area ? currentData.area : '');
    const [message, setMessage] = useState(null);
    const [toggleInsertSide, setToggleInsertSide] = useState(false);

    useEffect(() => {
        if(!selectedCoords) {
            navigator.geolocation.getCurrentPosition((pos) => {
                setCoords({
                    lat: pos.coords.latitude,
                    lng: pos.coords.longitude
                });
            });
        }
    }, []);

    useEffect(() => {
        if(selectedCoords){
            whatApi.convertTo3wa(selectedCoords).then(data => {
                setWords(data.words)
            });
            fetch(`https://api.geoapify.com/v1/geocode/reverse?lat=${selectedCoords.lat}&lon=${selectedCoords.lng}&apiKey=a82b8b97032641b5ae89582c74255fda`).then(data => {
                data.json().then(response => {
                    let props = {...response.features[0].properties};
                    let currentArea = '';
                    if(props.suburb){
                        currentArea += ' ' + props.suburb ;
                    }
                    setCity(props.state ? props.state : props.country);
                    setAddress(props.city ? props.city : props.state);
                    setArea(props.formatted.trim());
                    if(!toggleInsertSide){
                        handleToggleInserSide();
                    }
                })
            });
        }
    }, [selectedCoords])

    let defCoords = {
        lat: 5.316667,
        lng: -4.033333
    };

    if(selectedCoords){
        defCoords.lat = selectedCoords.lat;
        defCoords.lng = selectedCoords.lng;
    }else if(coords){
        defCoords.lat = coords.lat; 
        defCoords.lng = coords.lng;       
    }

    const defaultProps = {
        center: defCoords,
        zoom: 15,
        mapTypeId: "roadmap",
        styles: [{ stylers: [{ 'saturation': -100 }, { 'gamma': 0.8 }, { 'lightness': 4 }, { 'visibility': 'on' }] }]
    };

    const _onClick = ({x, y, lat, lng, event}) => {
        setSelectedCoords({
            lat: lat,
            lng: lng
        });
    };

    const handleToggleWhat3words = () => {
        setWhat3wordAbout(!what3wordAbout);
    }

    const handleNextSlide = (nombre) => {
        if(nombre === 0){
            setWhat3wordAbout(false);
        }
        setCurrentSlide(nombre);
    }

    let params = useQuery();
    params.set('publish', 'gallery');
    let params2 = useQuery();
    params2.set('publish', 'type');
    let pathname = useLocation().pathname;

    const dispatch = useDispatch();

    const handleSelectedAddress = () => {
        setMessage(null);
        if(country.length <= 0){
            setMessage('Veillez bien remplir ce formulaire')
        }else if(city.length <= 0){
            setMessage('Veillez bien remplir ce formulaire')
        }else if(address.length <= 0){
            setMessage('Veillez bien remplir ce formulaire')
        }else if(area.length <= 0){
            setMessage('Veillez bien remplir ce formulaire')
        }else{
            dispatch(addAddress({
                country: country,
                city: city,
                address: address,
                area: area,
                what3words: words,
                lat: selectedCoords.lat,
                lng: selectedCoords.lng
            }));
        }
    }

    const handleToggleInserSide = () => {
        setToggleInsertSide(!toggleInsertSide);
    }

    return(
        <div className={"address-content"}>
            <div className={ what3wordAbout ? "what3words active" : 'what3words'}>
                <div className="what-content">
                    <CloseOutlined onClick={handleToggleWhat3words} className="close" />
                    <div style={{marginLeft: `${currentSlide}`}} className="slide-content">
                        <div className="one-slide">
                            <img alt={"what3word vector"} src={what_1} />
                            <div className="text-body">
                                Les adresses classiques n'indiquent pas toujours des endroits précis
                                <div onClick={()=>handleNextSlide('-100%')}>
                                    <ChevronRight />
                                </div>
                            </div>
                        </div>
                        <div className="one-slide">
                            <img alt={"what3word vector"} src={what_2} />
                            <div className="text-body">
                                what3words a donné à chaque carré de 3 m x 3 m dans le monde une adresse unique de 3 mots
                                <div onClick={()=>handleNextSlide('-200%')}>
                                    <ChevronRight />
                                </div>
                            </div>
                        </div>
                        <div className="one-slide">
                            <img alt={"what3word vector"} src={what_3} />
                            <div className="text-body">
                                Les mots sont attribués aléatoirement à chaque carré et ne changeront plus jamais
                                <div onClick={()=>handleNextSlide('-300%')}>
                                    <ChevronRight />
                                </div>
                            </div>
                        </div>
                        <div className="one-slide">
                            <img alt={"what3word vector"} src={what_4} />
                            <div className="text-body">
                                Il est désormais facile de trouver et de partager n'importe quel lieu avec simplement 3 mots
                                <div onClick={()=>handleNextSlide(0)}>
                                    <ChevronRight />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={toggleInsertSide ? "insert-location active" : "insert-location"}>
                <CloseOutlined onClick={handleToggleInserSide} className="st-insert-close" />
                <div className="tit">
                    Saisir l'emplacement de la propriété
                </div>
                <p>
                    Veillez par préférence utiliser la carte pour définir l'addresse de la propriété. De plus what3words est utilisable qu'avec la carte. Pour en savoir plus sur what3words, cliquer sur l'icone d'info en dessous
                </p>
                <div className="box">
                    <input type="text" disabled placeholder="Pays" value={country} />
                </div>
                <div className="box">
                    <input onChange={(e) => setCity(e.target.value)} value={city} type="text" placeholder="Ville" />
                </div>
                <div className="box">
                    <input onChange={(e) => setAddress(e.target.value)} value={address} type="text" placeholder="Commune et/ou Quartier" />
                </div>
                <div className="box">
                    <textarea onChange={(e) => setArea(e.target.value)} value={area} placeholder="Localisation" />
                </div>
                <div className="message"> {message} </div>
                <div className={"btn-group"}>
                    <Link to={{pathname:`${pathname}`, search: params2.toString()}}>
                        <button className={"return"}>Arrière</button>
                    </Link>
                    {
                        country && city && address && area && words && selectedCoords.lat && selectedCoords.lng ?
                            <Link to={{pathname:`${pathname}`, search: params.toString()}}>
                                <button onClick={handleSelectedAddress}>Suivant</button>
                            </Link>
                        :
                            <button className={'blur'} onClick={() => {}}>Suivant</button>
                    }
                    
                </div>
            </div>
            <div className="choose-map">
                <div className="map-title">
                    <MenuOutlined onClick={handleToggleInserSide} className={"st-map-bar"} />
                    Déplacer ou zoomer la carte pour préciser l'emplacement de votre proprété 
                </div>
                <div className="autosuggest">
                    <img alt={"what3word logo"} src={whatLogo} />
                    <input type="text" value={words} name="what3words" placeholder="what3words" />
                    <HelpOutlined onClick={handleToggleWhat3words} title="A propos de What3words" />
                </div>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_MAPS_API_KEY }}
                    center={defaultProps.center}
                    defaultZoom={defaultProps.zoom}
                    onClick={_onClick}
                >
                    { selectedCoords ? <div className={"pined"} alt={"Property Location"} lat={selectedCoords.lat} lng={selectedCoords.lng}><span></span></div> : '' }
                </GoogleMapReact>
            </div>
        </div>
    )
}

export default PropertyAdress;