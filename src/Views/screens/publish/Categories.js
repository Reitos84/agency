import styled from "@emotion/styled";
import Simpletitle from "@components/Simpletitle";
import { useEffect, useState } from "react";
import CategoryService from "@api/entity/CategoryService";
import basepath from "@constants/basepath";
import Flatloader from "@components/Flatloader";
import {useDispatch, useSelector} from "react-redux";
import { chooseCategory } from "@reducers/AddPropertyReducer";
import {useLocation, Link} from "react-router-dom";
import {DoneOutlined} from "@material-ui/icons";

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const Categories = () => {

    const currentData = useSelector(state => state.property.data.category);

    let [categories, setCategories] = useState([]);
    let [selectedCategory, setSelectedCategory] = useState(currentData);
    const dispatch = useDispatch();


    useEffect(() => {
        let catService = new CategoryService();
        catService.getAll().then(data => {
            setCategories(data['hydra:member']);
        });
    }, []);

    let params = useQuery();
    params.set('publish', 'type');
    let pathname = useLocation().pathname;

    const handleChooseCategory = (index) => {
        setSelectedCategory(index);
    }


    return(
        <div className="choose-category">
            <Simpletitle>
                Votre propriété est en vente ou en location ?
            </Simpletitle>
            <div className={"categories"}>
                { categories.length > 0 ?  categories.map(category => 
                <div className={'category'} key={category.id} onClick={() => handleChooseCategory(category) }>
                    {   selectedCategory && selectedCategory['@id'] === category['@id'] ?
                            <DoneOutlined />
                        :
                            <></>
                    }
                    <img src={basepath + category.icon} alt={category.title} />
                    <span>{category.title}</span>
                </div>) : 
                <Flatloader /> }
            </div>
            <div className={"bottom"}>
                {
                    selectedCategory ?
                        <Link to={{pathname:`${pathname}`, search: params.toString()}}>
                            <button onClick={() => dispatch(chooseCategory(selectedCategory))}>Suivant</button>
                        </Link>
                    :
                        <button className={'blur'} onClick={() => {}}>Suivant</button>
                }
            </div>
        </div>
    )
}

export default Categories;