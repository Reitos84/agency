import styled from "@emotion/styled";
import Simpletitle from "@components/Simpletitle";
import { useState, useEffect } from "react";
import TypeService from "@services/api/entity/TypeService";
import basepath from "@constants/basepath";
import Flatloader from "@components/Flatloader";
import {useDispatch, useSelector} from "react-redux";
import { chooseType } from "@reducers/AddPropertyReducer";
import {Link, useLocation} from "react-router-dom";
import {DoneOutlined} from "@material-ui/icons";

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const PropertyType = () => {

    const currentData = useSelector(state => state.property.data.type);
    const category = useSelector(state => state.property.data.category);

    const [selectedType, setSelectedType] = useState(currentData);
    const [types, setTypes] = useState(false);

    const dispatch = useDispatch();

    let params = useQuery();
    params.set('publish', 'location');
    let params2 = useQuery();
    params2.set('publish', 'true');
    let pathname = useLocation().pathname;

    useEffect(() => {
        let service = new TypeService();
        service.getList().then(res => {
            setTypes(res['hydra:member']);
        });
    }, []);

    const handleChooseType = (index) => {
        setSelectedType(index);
    }

    let acceptCategory = category.slug;

    return(
        <div>
            <Simpletitle>
                Comment vous définissez votre propriété ?
            </Simpletitle>
            <div className={"categories sub"}>
                { types.length > 0 ? types.map(type => {
                        if(type.category === null || type.category === acceptCategory){
                            return <div className={"type"} onClick={() => handleChooseType(type)} key={type.id}>
                            {    selectedType && selectedType['@id'] === type['@id'] ?
                                <DoneOutlined />
                                :
                                <></>
                            }
                            <img src={basepath + type.icon} alt={type.title} />
                            <span>{type.title}</span>
                        </div>
                        }
                    })
                    :
                        <Flatloader /> 
                }
            </div>
            <div className={"btn-group"}>
                <Link to={{pathname:`${pathname}`, search: params2.toString()}}>
                    <button className={"return"}>Arrière</button>
                </Link>
                {
                    selectedType ?
                        <Link to={{pathname:`${pathname}`, search: params.toString()}}>
                            <button onClick={() => dispatch(chooseType(selectedType))}>Suivant</button>
                        </Link>
                    :
                        <button className={'blur'} onClick={() => {}}>Suivant</button>
                }
            </div>
        </div>
    )
}

export default PropertyType;