import {useEffect, useRef, useState} from "react";
import {addPictures} from "@core/reducers/AddPropertyReducer";
import {useDispatch, useSelector} from "react-redux";
import {toast} from "material-react-toastify";
import {ImagePreview} from "@components/Preview";
import PictureService from "@api/entity/PictureService";
import Flatloader from "../../components/Flatloader";
import {Link, useLocation} from "react-router-dom";

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const Gallery = () => {
    const dispatch = useDispatch();
    let currentData = useSelector(state => state.property.data);
    let savedPictures = null;
    if (currentData.pictures){
        savedPictures = currentData.pictures;
        savedPictures = savedPictures.slice(0);
        savedPictures = savedPictures.sort(function (a, b) {
            return b.isBackground - a.isBackground;
        });
    }

    const [picturesObjects, setPicturesObjects] = useState(savedPictures ? savedPictures : []);
    const [backIndex, setBackIndex] = useState(null);
    const [load, setLoad] = useState(false);

    const pictureServive = new PictureService();

    useEffect(() => {
        if(currentData.pictures) {
            currentData.pictures.forEach(elems => {
                if (elems.isBackground) {
                    setBackIndex(elems.id);
                }
            });
        }
    }, []);

    const uploadProcess = async (files) => {
        return await pictureServive.sendPictures(files);
    }

    const deletePicture = (index) => {
        pictureServive.remove(index).then(res => {
            if(res.status === 204){
                setPicturesObjects(picturesObjects.filter(x => index !== x.id));
            }
        });
    }

    const setBackground = (index) => {
        picturesObjects.forEach(x => {
           if(x.isBackground === true){
               pictureServive.removeBackground(x.id);
           }
        });
        pictureServive.setIsBackground(index).then(res => {
           if(res.status === 200){
               let elems = [...picturesObjects];
               elems = elems.map(x => {
                   return {
                       ...x,
                       isBackground: x.id === index
                   };
               });
               var listing = elems.slice(0);
               listing = listing.sort(function(a, b) {
                   return b.isBackground - a.isBackground;
               });
               setBackIndex(index);
               setPicturesObjects(listing);
           }
        });
    }
    let options = {
        type: 'file',
        style: {
            display: "none"
        }
    };
    options.accept = "image/*, .mp4";
    options.multiple = true;
    options.onChange = async (event) => {
        if(event.target.files.length > 0){
            setLoad(true);
            uploadProcess(event.target.files).then(list => {
                setPicturesObjects([...picturesObjects, ...list]);
                setLoad(false);
            }).catch(() => {
                setLoad(false);
            })
        }else{
            setLoad(false);
        }
    }

    const handleChoosePicture = () => {
        insertInput.current.click();
    }

    const insertInput = useRef(null);

    const handleSavePictures = (e) => {
        if(picturesObjects.length >= 5){
            dispatch(addPictures(picturesObjects));
        }else{
            e.preventDefault();
        }
    }

    let params = useQuery();
    params.set('publish', 'details');
    let params2 = useQuery();
    params2.set('publish', 'location');
    let pathname = useLocation().pathname;

    return(
        <div className={"picture-upload"}>
            <div className={"page-loading"}>
                <span> </span>
            </div>
            <div className="title">Ajouter les photos de votre propriété </div>
            <p style={{textAlign: "center"}}>
                Les photos permettent de mieux vendre votre propriéte, ajoutez des photos claires et soignées.
            </p>
            <div className="gallery">
                <div className="pictures">
                    <div className="top">
                        Fichiers Envoyées (minimum=5)
                    </div>
                    <div className="pictures-list">
                        { picturesObjects.map(picture => <ImagePreview backIndex={backIndex} setIsBack={() => setBackground(picture.id)} delections={() => deletePicture(picture.id)} key={picture.id} file={picture} />) }
                        { load ? <Flatloader /> : '' }
                    </div>
                </div>
                <div className="drag">
                    <div style={{textAlign: "center"}} className={"drag-content"}>
                        <input {...options} ref={insertInput} />
                        <p>Cliquez sur le bouton parcourir pour séléctionner vos photos</p>
                        <button onClick={handleChoosePicture}>Parcourir</button>
                    </div>
                </div>
            </div>
            <div className={"btn-group"}>
                <Link to={{pathname:`${pathname}`, search: params2.toString()}}>
                    <button className={"return"}>Arrière</button>
                </Link>
                {
                    picturesObjects.length >= 5 ?
                        <Link to={{pathname:`${pathname}`, search: params.toString()}}>
                            <button onClick={handleSavePictures}>Suivant</button>
                        </Link>
                    :
                        <button className="blur" onClick={() => {}}>Suivant</button>
                }
            </div>
        </div>
    )
}

export default Gallery;