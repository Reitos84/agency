import styled from "@emotion/styled";
import Button from "@components/Button";
import { semibold } from "@constants/fonts";
import { PhotoCamera } from "@material-ui/icons";
import { useSelector, useDispatch } from "react-redux";
import { useEffect, useRef, useState } from "react";
import UserProvider from "@auth/UserProvider";
import { fill } from "@reducers/AgencyReducer";
import Whiteloader from "@components/Whiteloader";
import { toast } from "material-react-toastify";

const Editprofile = () => {
    const agency = useSelector(state => state.agency.details);
    const [infos, setInfos] = useState(agency);
    const [load, setLoad] = useState(false);
    const [uploaded, setUploaded] = useState(null);

    useEffect(() => {
        if(uploaded){
            let data = new FormData();
            data.append('avatar', uploaded);
            const response = provider.PostAvatar(data);
            response.then(user => {
                dispatch(fill(user));
                toast.success('Votre image a été envoyer');
            }).catch(() => {
                toast.error('Une erreur s\'est produite, essayez à nouveau')
            });
        }
    }, [uploaded]);
    
    const provider = new UserProvider();
    const dispatch = useDispatch();

    const handleChangeName = (event) => {
        setInfos({...infos, name: event.target.value});
    }

    const handleChangeMobile = (event) => {
        setInfos({...infos, mobilephone: event.target.value});
    }

    const handleChangeOffice = (event) => {
        setInfos({...infos, officephone: event.target.value});
    }

    const handleChangeWebsite = (event) => {
        setInfos({...infos, website: event.target.value});
    }

    const handleChangeComment = (event) => {
        setInfos({...infos, comment: event.target.value});
    }

    const handleChangeLocalisation = (event) => {
        setInfos({...infos, localisation: event.target.value});
    }

    const handleChangeAvatar = (event) => {
        const file = event.target.files[0];
        if(file.size > 1500000){
            toast.error('La taille de votre logo doit être inférieur à 1.5mb');
        }else{
            setUploaded(file);
        }
    }

    const saveEdit = () => {
        setLoad(true);
        if(!infos.name || !infos.mobilephone || !infos.localisation ){
            toast.error('Veillez bien renseigner les champs ci-dessus');
            setLoad(false);
            return false;
        }
        if(infos.comment && infos.comment.length < 60){
            toast.error('Veillez bien renseigner les champs ci-dessus');
            setLoad(false);
            return false;
        }
        const query = provider.EditInfos(infos);
        query.then(res => {
            dispatch(fill(res));
            setLoad(false);
            toast.success('Votre profil a été modifier');
        }).catch(() => {
            setLoad(false);
            toast.error('Veillez bien renseigner les champs ci-dessus');
        });
    }

    const avatarInput = useRef(null);

    const choosePic = () => {
        avatarInput.current.click();
    }

    return(
        <Boxes>
            <div className="split">
                <div className="box">
                    <label>Nom de l'agence</label>
                    <input onChange={handleChangeName} type="text" value={infos.name === null ? "" : infos.name} />
                </div>
                <div className="box">
                    <label>Contact Mobile</label>
                    <input onChange={handleChangeMobile} type="text" value={infos.mobilephone === null ? "" : infos.mobilephone} />
                </div>
                <div className="box">
                    <label>Contact Bureau</label>
                    <input onChange={handleChangeOffice} type="text" value={infos.officephone === null ? "" : infos.officephone} />
                </div>
                <div className="box">
                    <label>Site web</label>
                    <input onChange={handleChangeWebsite} type="text" value={infos.website === null ? "" : infos.website} />
                </div>
                <div className="box">
                    <label>Description ({ infos.comment && infos.comment.length > 0 ? infos.comment.length + '/' : '' }60 caractères)</label>
                    <textarea onChange={handleChangeComment} value={infos.comment === null ? "" : infos.comment}/>
                </div>
                <Button onClick={saveEdit}>
                    { load ? <Whiteloader /> : "" }
                    Enregistrer
                </Button>
            </div>
            <div className="picture">
                <div className="picture-title">Logo de l'agence</div>
                <p>Taille recommandée 100x100 et inférieur à 1.5mb</p>
                <div onClick={choosePic} className="choose">
                    { uploaded ? <img src={ URL.createObjectURL(uploaded) } alt={"Choose element"} /> : <><PhotoCamera />Ajouter une image</>}
                </div>
                <input onChange={handleChangeAvatar} style={{display: "none"}} accept="image/*" type="file" ref={avatarInput} />
                <div className="box">
                    <label>Localisation</label>
                    <input onChange={handleChangeLocalisation} value={infos.localisation === null ? "" : infos.localisation} type="text" />
                </div>
            </div>
        </Boxes>
    );
}

const Boxes = styled.div({
    display: "flex",
    "@media (max-width: 700px)": {
        flexDirection: "column"
    },
    "& .split": {
        width: "50%",
        padding: "0 20px 0 0",
        "@media (max-width: 700px)": {
            width: "100%",
            order: 2,
            padding: "0"
        },
        "& .message": {
            height: "30px",
            lineHeight: "30px"
        }
    },
    "& .message":{
        marginBottom: "20px",
        color: "red"
    },
    "& .picture": {
        width: "50%",
        borderLeft: "2px solid #e5e5e5",
        padding: "0 20px",
        "@media (max-width: 700px)": {
            borderLeft: "none",
            padding: "0",
            order: 1,
            width: "100%"
        },
        "& .picture-title": {
            fontFamily: semibold
        },
        "& .choose": {
            border: "5px dashed #e5e5e5",
            borderRadius: "15px",
            margin: "30px 0 30px",
            background: "#f5f5f5",
            textAlign: "center",
            width: "100px",
            height: "100px",
            fontFamily: semibold,
            color: "#525252",
            cursor: "pointer",
            fontSize: "14px",
            "& img": {
                width: "90px",
                height: "90px",
                objectFit: "cover",
                borderRadius: "10px"
            },
            "& svg": {
                display: "block",
                margin: "auto",
                color: "#727272",
                position: "relative",
                fontSize: "30px",
                marginTop: "12px"
            }
        }
    },
    "& .box": {
        marginBottom: "20px",
        "& label": {
            display: "block",
            fontFamily: semibold,
            marginBottom: "5px"
        },
        "& input": {
            width: "100%",
            height: "40px",
            background: "#f5f5f5",
            border: "none",
            borderRadius: "3px",
            padding: "0 10px"
        },
        "& textarea": {
            width: "100%",
            background: "#f5f5f5",
            border: "none",
            borderRadius: "3px",
            padding: "3px 10px",
            height: "150px",
            resize: "none"
        },
        "& button": {
            background: "#fff",
            color: "#323232",
            marginTop: "20px",
            border: "2px solid #aaa",
            width: "auto",
            padding: "0 10px 0 40px",
            position: "relative",
            "& svg": {
                position: "absolute",
                left: "10px",
                top: "5px"
            }
        }
    }
}) 

export default Editprofile;