import styled from "@emotion/styled";
import { semibold } from "@constants/fonts";
import Switch from '@mui/material/Switch';
import Button from "@components/Button";
import Whiteloader from "@components/Whiteloader";
import { toast } from "material-react-toastify";
import { VisibilityOffOutlined, VisibilityOutlined } from "@material-ui/icons";
import { useState } from "react";
import UserProvider from "@services/auth/UserProvider";
import Token from "@services/auth/Token";
import { useDispatch, useSelector } from "react-redux";
import { alpha, styled as stile } from '@mui/material/styles';
import { changeTwoFactor } from "@reducers/AgencyReducer";

const Preferences = () => {
    const user = useSelector(state => state.agency.details);
    const dispatch = useDispatch();

    const [showPassword, setShowPassword] = useState(false); 
    const [changePassLoad, setChangePassLoad] = useState(false);
    const [firstPass, setFirstPass] = useState('');
    const [secondPass, setSecondPass] = useState('');
    
    let provide = new UserProvider(Token);

    const togglePassword = () => {
        setShowPassword(!showPassword);
    }

    const handleChangeFirst = (event) => {
        setFirstPass(event.target.value);
    }

    const handleChangeSecond = (event) => {
        setSecondPass(event.target.value);
    }

    const handleChangePassword = () => {
        if(firstPass === secondPass){
            setChangePassLoad(true);
            provide.changePassword(secondPass).then(data => {
                if(data.success === true){
                    toast.success("Mot de passe modifié avec success");
                    setChangePassLoad(false);
                }
            }).catch(() => {
                toast.warning('Impossible d\'effectuer cette opération. Esayez à nouveau');
                setChangePassLoad(false);
            })
        }else{
            toast.error('Les deux mot de passe doivent correspondre');
        }
    }

    const label = { inputProps: { 'aria-label': 'Switch demo' } };
    const GreenSwitch = stile(Switch)(({ theme }) => ({
        '& .MuiSwitch-switchBase.Mui-checked': {
          color: '#43AA8B',
          '&:hover': {
            backgroundColor: alpha('#43aa8b87', theme.palette.action.hoverOpacity),
          },
        },
        '& .MuiSwitch-switchBase.Mui-checked + .MuiSwitch-track': {
          backgroundColor: '#43aa8b87',
        },
    }));

    const handleChangeSecured = () => {
        provide.changeSecurity(user.id, !user.isSecured).then(data => {
            dispatch(changeTwoFactor(data.isSecured));
        });
    }

    return(
        <Content>
            <div className="split">
                <div className="block">
                    <div className="setting-box">
                        <div className="head-title">
                            Mot de passe
                        </div>
                        <div className="setting-content">
                            <p>
                                Cette action est irreversible, vous devez maintenant utiliser le nouveau mot de passe pour vous connecter.
                            </p>
                            <div className="passbox">
                                { showPassword ? <VisibilityOffOutlined onClick={togglePassword} /> : <VisibilityOutlined onClick={togglePassword} /> }
                                
                                <input onChange={handleChangeFirst} value={firstPass} className="fill-pass" type={showPassword ? "text" : "password"} placeholder="Nouveau mot de passe" />
                                <input onChange={handleChangeSecond} value={secondPass} className="fill-pass" type={showPassword ? "text" : "password"} placeholder="Répéter le mot de passe" />
                            </div>
                            <Button onClick={handleChangePassword}>
                                { changePassLoad ? <Whiteloader /> : "" }
                                Enregistrer
                            </Button>
                        </div>
                    </div>
                    <div className="setting-box">
                        <div className="head-title">
                            Double authentification
                            <GreenSwitch onClick={handleChangeSecured} {...label} defaultChecked={user.isSecured} />
                        </div>
                        <div className="setting-content">
                            Une fois cette option activer, nous allons vous envoyer un code de validation par email à chaque fois que vous essayez de vous connecter à votre compte.
                        </div>
                    </div>
                </div>
            </div>
        </Content>
    )
}

const Content = styled.div({
    "& .setting-box": {
        marginBottom: "20px",
        border: "1px solid #ccc",
        borderRadius: "5px",
        "& .head-title": {
            fontFamily: semibold,
            height: "40px",
            lineHeight: "40px",
            borderBottom: "1px solid #ccc",
            display: "flex",
            justifyContent: "space-between",
            padding: "0 10px"
        },
        "& .passbox": {
            width: "60%",
            marginTop: "10px",
            position: "relative",
            "& svg": {
                color: "#525252",
                position: "absolute",
                right: "-40px",
                top: "4px",
                cursor: "pointer",
                fontSize: "27px"
            }
        },
        "& .setting-content": {
            padding: "9px",
        },
        "& label": {
            display: "block",
            fontFamily: semibold,
            marginBottom: "5px"
        },
        "& .fill-pass": {
            marginBottom: "10px"
        },
        "& input": {
            display: "block",
            width: "100%",
            height: "35px",
            background: "#f5f5f5",
            border: "none",
            borderRadius: "3px",
            padding: "0 10px"
        },
        "& textarea": {
            width: "100%",
            height: "40px",
            background: "#f5f5f5",
            border: "none",
            borderRadius: "3px",
            padding: "3px 10px",
            height: "120px"
        },
        "& button": {
            marginTop: "20px",
            width: "auto",
            position: "relative",
            padding: "0 30px"
        }
    }
});

export default Preferences;