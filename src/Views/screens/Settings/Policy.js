import styled from "@emotion/styled";
import { OpenInNewOutlined } from "@material-ui/icons";

const Policy = () => {

    const clientLink = process.env.REACT_APP_CLIENT_LINK;

    return(
        <div>
            <div className="stage-step">
                <div className="st-title">Termes et conditions</div>
                <a target={'_blank'} href={`${clientLink}/users-conditions`}><OpenInNewOutlined /></a>
            </div>
            <div className="stage-step">
                <div className="st-title">Politiques de confidentialités</div>
                <a target={'_blank'} href={`${clientLink}/policy`}><OpenInNewOutlined /></a>
            </div>
            <div className="stage-step">
                <div className="st-title">Mentions Légales</div>
                <a target={'_blank'} href={`${clientLink}/legal-notice`}><OpenInNewOutlined /></a>
            </div>
            <div className="stage-step">
                <div className="st-title">Lois sur la vente et la location</div>
                <a target={'_blank'} href={process.env.REACT_APP_LEGAL_FILE}><OpenInNewOutlined /></a>
            </div>
        </div>
    )
}

export default Policy;