import {createPortal} from "react-dom";
import {Close} from "@material-ui/icons";
import "@css/add-campain.css";
import {useEffect, useState} from "react";
import CampainType from "@components/CampainType";
import CampainDetails from "@components/CampainDetails";
import CampainAction from "../components/CampainAction";
import {useLocation} from "react-router-dom";
import { useSelector } from "react-redux";
import CampainService from "@api/entity/CampainService";
import CampainValidation from "../components/CampainValidation";

const campainService = new CampainService();

const CampainModal = ({history}) => {
    const [step, setStep] = useState(1);

    const params = new URLSearchParams(useLocation().search);
    const pathname = useLocation().pathname;

    const currentCampain = useSelector(state => state.agency.newCampain)

    useEffect(() => {
        if(currentCampain){
            if(currentCampain.action){
                setStep(4)
            }else if(currentCampain.title){
                setStep(3)
            }else if(currentCampain.type){
                setStep(2)
            }
        }
    }, []);

    let c = params.get('c');

    const handleCloseModal = () => {
        params.delete('c');
        history.push(pathname + '?' + params.toString());
    }

    return createPortal(
        c ?
        <div className="post-modal">
            <div className="md-content">
                <div className="md-head">
                    <Close onClick={handleCloseModal} titleAccess="Fermer" className="close" />
                </div>
                <div className="md-body">
                    <div className={"add-campain"}>
                        <div className={"progress-line"}>
                            <div onClick={() => setStep(1)} className={step > 1 ? "active" : ""} />
                            <div onClick={() => setStep(2)} className={step > 2 ? "active" : ""} />
                            <div onClick={() => setStep(3)} className={step > 3 ? "active" : ""} />
                            <div className={step > 4 ? "active" : ""} />
                        </div>
                        {
                            step === 1 ? <CampainType service={campainService} nextStep={setStep} /> : ""
                        }
                        {
                            step === 2 ? <CampainDetails nextStep={setStep} /> : ""
                        }
                        {
                            step === 3 ? <CampainAction service={campainService} nextStep={setStep} /> : ""
                        }
                        {
                            step === 4 ? <CampainValidation service={campainService} nextStep={handleCloseModal} /> : ""
                        }
                    </div>
                </div>
            </div>
        </div> : <></>,
        document.getElementById('campain_create_root')
    )
}

export default CampainModal;