import {Link} from "react-router-dom";
import '@css/login-page.css';
import {AlternateEmail} from "@material-ui/icons";
import { useState } from "react";
import loader from "@assets/loader/785.svg";
import paperplane from "@assets/icon/paper_plane.png";
import ResetPasswordService from "@api/entity/ResetPasswordService";

const ResetPassword = () => {
    const [message, setMessage] = useState('');
    const [load, setLoad] = useState(false);
    const [email, setEmail] = useState('');
    const [success, setSuccess] = useState(false);

    const resetPasswordService = new ResetPasswordService();

    const handleChangeEmail = (event) => {
        setEmail(event.currentTarget.value);
    }

    const requestResetMail = () => {
        if(email.length > 0){
            setLoad(true);
            resetPasswordService.requestEmail(email).then(data => {
                if(data.success){
                    setSuccess(true);
                }else{
                    setMessage('Votre email est incorrect');
                }
            })
        }else{
            setMessage('Veillez saisir votre email');
        }
    }

    return(
        <div className="login">
            <div className="image-background">
                <div className={"image-container"}>
                    <div className="logo">
                        Abidjan Habitât
                    </div>
                </div>
            </div>
            <div className="login-side">
                <div className="login-content">
                    {
                        success ?
                        <>
                            <div className={"sent"}>
                                <div className={"thumb"}>
                                    <img src={paperplane}  alt={"email envoyé"} />
                                </div>
                                <div className={"title"}>L'email a été envoyé</div>
                                <div className={"exp"}>
                                    Veuillez vérifier votre boîte de réception et cliquez sur le lien reçu pour réinitialiser votre mot de passe
                                </div>
                                <div className={"ask-question"}>
                                    Vous n'avez pas reçu le lien ? <span onClick={requestResetMail} className={"click"}>Renvoyé</span>
                                </div>
                                <Link to="/" className="sad">
                                    Se connecter
                                </Link>
                            </div>
                        </>
                        :
                        <>
                            <div className="title">
                                Réinitialiser votre mot de passe
                            </div>
                            <div className={"exp"}>
                                Entrez votre e-mail enregistré ci-dessous pour recevoir les instructions de réinitialisation du mot de passe
                            </div>
                            <div className="message">
                                {message}
                            </div>
                            <div className="login-box">
                                <AlternateEmail style={{color: "#727272"}} />
                                <input value={email} onChange={handleChangeEmail} type="text" name="email" placeholder="Email"/>
                            </div>
                            <button onClick={requestResetMail} className="btn-connect">
                                { load ? <img src={loader} alt={"veillez patienter"}/> : ""}
                                Envoyer un email
                            </button>
                            <Link to="/" className="sad">
                                Se connecter
                            </Link>
                        </>
                    }

                </div>
            </div>
        </div>
    )
}

export default ResetPassword;