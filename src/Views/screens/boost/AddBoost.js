import '@css/add_banner.css';
import { useState, useEffect } from "react";
import Flatloader from "@components/Flatloader";
import { useParams } from "react-router-dom";
import PropertyService from "@api/entity/PropertyService";
import BoostDetails from "@components/BoostDetails";
import BoostPayment from "@components/BoostPayment";

const propertyService = new PropertyService();

const AddBoost = () => {

    document.title = "Booster une propriété";
    
    let propertyId = useParams().propertyId;
    const [load, setLoad] = useState(true);
    const [selectedProperty, setSelectedProperty] = useState({});
    const [picture, setPicture] = useState(null);
    const [boostData, setBoostData] = useState({});
    const [current, setCurrent] = useState(1);

    useEffect(async () => {
        let property = await propertyService.getWithId(propertyId);
        property.pictures.forEach(pic => {
            if(pic.isBackground){
                setPicture(pic);
            }
        });
        if(!picture){
            setPicture(property.pictures[0]);
        }
        setSelectedProperty(property);
        setLoad(false);
    }, [propertyId]);

    const attachBoostData = (data) => {
        setBoostData(data);
        setCurrent(2);
    }

    //Ajouter un bouton retour

    return(
        <div className={"add-boost-container"}>
            <div className={"part gray"}>
                <div className={"choosed-property"}>
                    {
                        load ?
                        <Flatloader />
                        :
                        <div className={"choosed-content"}>
                            <div className={"choosed-title"}>
                                <div className={"choosed-label"}>
                                    { selectedProperty.title.substr(0, 30) }
                                    {
                                        selectedProperty.title.length > 30 ? '...' : ''
                                    }
                                    </div>
                                <div className={"choosed-cat"}>{ selectedProperty.category.title }</div>
                            </div>
                            <img src={ `${process.env.REACT_APP_API_BASE_PATH}${picture.url}`} />
                            <p>
                                { selectedProperty.description }
                            </p>
                        </div>
                    }
                </div>
            </div>
            <div className={"part"}>
                <div className={"bullet"}>
                    <span className="bull">
                        <i></i>
                    </span>
                    <span className="bull"></span>
                    <span className="bull"></span>
                </div>
                <div className={"form-part"}>
                    {
                        current === 1 ?
                        <BoostDetails data={boostData} setData={attachBoostData} /> : <BoostPayment uid={selectedProperty['@id']} boostData={boostData} />
                    }
                </div>
            </div>
        </div>
    )
}

export default AddBoost;