import {
    Close,
    TrendingUpOutlined
} from '@material-ui/icons';
import { createPortal } from 'react-dom';
import { useEffect, useState } from "react";
import PropertyService from "@api/entity/PropertyService";
import PropertyComponent from "./PropertyComponent";
import Flatloader from "../components/Flatloader";
import { Link, useLocation } from "react-router-dom"

const PropertyModal = ({close, index}) => {
    const [property, setProperty] = useState({});
    const [load, setLoad] = useState(true);

    let propertyService = new PropertyService();

    useEffect(async () => {
        if(index){
            setLoad(true);
            const data = await propertyService.getWithId(index);
            setProperty(data);
            setLoad(false);
        }
    }, [index]);

    const params = new URLSearchParams(useLocation().search);
    params.delete('property');

    return createPortal(
        <>
            <div className={index ? "slider-modal active" : "slider-modal"}>
                <div className={"property-content"}>
                    <div className={"houses"}>
                        <div className="close-side">
                            {/*
                                <Link to={{pathname: `/boost/add/${index}`, search: params.toString()}}>
                                    <TrendingUpOutlined />
                                    Booster cette propriété
                                </Link>
                            */}
                            <span onClick={close} className={"houses-close"}>
                                <Close />
                            </span>
                        </div>
                        { load ? <Flatloader /> : <PropertyComponent data={property} /> }
                    </div>
                </div>
            </div>
        </>,
        document.getElementById("modal_root")
    )
}

export default PropertyModal;