import '../css/register.css';
import { Link } from "react-router-dom";
import { DoneOutlined } from "@material-ui/icons";
import {useCallback, useState} from "react";
import {profileColors} from "../constants/color";
import UserProvider from "../../Services/auth/UserProvider";
import loader from "@assets/loader/785.svg";

const userProvider = new UserProvider();

const Register = () => {

    document.title = "Réjoindre le réseau";

    const [email, setEmail] = useState('');
    const [name, setName] = useState('');
    const [mobilephone, setMobilePhone] = useState('');
    const [officephone, setOfficephone] = useState('');
    const [localisation, setLocalisation] = useState('');
    const [website, setWebsite] = useState('');
    const [message, setMessage] = useState('');
    const [emailExist, setEmailExist] = useState(false);
    const [load, setLoad] = useState(false);
    const [success, setSuccess] = useState(false);

    let emailPattern = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    const findEmail = useCallback(() => {
        userProvider.emailIsExist(email).then((data) => {
            if(data['hydra:totalItems'] > 0){
                setMessage('Cet email est déjà associer à un compte');
                setEmailExist(true);
            }else{
                setEmailExist(false);
                setMessage('');
            }
        });
    })

    const handleChangeName = (e) => {
        setName(e.currentTarget.value)
    }

    const handleChangeEmail = (e) => {
        setEmail(e.currentTarget.value)
    }

    const handleChangePhone = (e) => {
        setMobilePhone(e.currentTarget.value)
    }

    const handleChangeDesktopPhone = (e) => {
        setOfficephone(e.currentTarget.value)
    }

    const handleChangePlace = (e) => {
        setLocalisation(e.currentTarget.value)
    }

    const handleChangeWebsite = (e) => {
        setWebsite(e.currentTarget.value)
    }

    const colorNum = Math.round(Math.random() * profileColors.length);

    const handleSendRequest = useCallback(() => {
        if(name.length > 0 && mobilephone.length > 0 && localisation.length > 0 && email.length){
            if(emailPattern.test(email) && !emailExist){
                setLoad(true);
                userProvider.sendSignInRequest({
                    name: name,
                    email: email,
                    username: email,
                    password: 'guest_book_require_sign',
                    mobilephone: mobilephone,
                    officephone: officephone.length > 0 ? officephone : null,
                    website: website.length > 0 ? website : null,
                    localisation: localisation.length > 0 ? localisation : null,
                    color: profileColors[colorNum]
                }).then(data => {
                    if(data.status == 201){
                        setSuccess(true);
                    }else{
                        setMessage('Une erreur s\'est produite, vérifiez vos informations');
                    }
                })
            }else{
                setMessage('Veillez entrer un email valide');
            }
        }else{
            setMessage('Veillez à bien remplir le formulaire');
        }
    })

    return(
        <div className={"register"}>
            <div className={"left-side"}>
                <div className={"left-container"}>
                    <div className={"logo"}>Abidjan Habitat</div>
                </div>
            </div>
            {
                success ? 
                <div className="form-side">
                    <div className='reg-success-side'>
                        <div className="reg-success-icon">
                            <DoneOutlined />
                        </div>
                        <strong>Réussie</strong>
                        <p>
                            Votre demande a bien été effectuée, nous allons vous contacter pour finaliser votre inscription.
                        </p>
                    </div>
                </div>
                :
                <div className={"form-side"}>
                    <div className={"title"}>
                        Formulaire d'inscription
                    </div>
                    {
                        message.length <= 0 ? <div>
                            Les champs avec un astérix sont obligatoires.
                        </div> : ''
                    }
                    <div style={{color: 'red'}} className={"message"}>{ message }</div>
                    <div className={"form"}>
                        <div className={"two-block"}>
                            <div className="register-box">
                                <label>Nom de l'agence <span>*</span></label>
                                <input onChange={handleChangeName} value={name} type="text" name="name"/>
                            </div>
                            <div className="register-box">
                                <label>Email <span>*</span></label>
                                <input onBlur={findEmail} value={email} onChange={handleChangeEmail} type="text" name="email" />
                            </div>
                        </div>
                        <div className={"two-block"}>
                            <div className="register-box">
                                <label>Contact Mobile <span>*</span></label>
                                <input onChange={handleChangePhone} value={mobilephone} type="text" name="mobile" placeholder='+225xxxxxxxxxx'/>
                            </div>
                            <div className="register-box">
                                <label>Contact Bureau </label>
                                <input onChange={handleChangeDesktopPhone} value={officephone} type="text" name="desktop"/>
                            </div>
                        </div>
                        <div className={"two-block"}>
                            <div className="register-box">
                                <label>Emplacement <span>*</span></label>
                                <input onChange={handleChangePlace} value={localisation} type="text" name="place" />
                            </div>
                            <div className="register-box">
                                <label>Site web </label>
                                <input onChange={handleChangeWebsite} value={website} type="text" name="website" />
                            </div>
                        </div>
                        <button onClick={handleSendRequest}>
                            { load ? <img src={loader} alt="veillez patienter" /> : "" }
                            Envoyer
                        </button>
                        <div className={"ask-question"}>
                            Vous avez déjà un compte ? <Link to={"/login"}>Connectez-vous</Link>
                        </div>
                    </div>
                </div>
            }
       </div>
    )
}

export default Register;