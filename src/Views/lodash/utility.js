export function useQuery(url) {
    return new URLSearchParams(url);
}