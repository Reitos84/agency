import './App.css';
import Navigation from "@navigation/Navigation";
import Redirect from '@navigation/Redirect';
import Account from '@screens/Account';
import { useSelector } from 'react-redux';
import { ToastContainer } from 'material-react-toastify';
import 'material-react-toastify/dist/ReactToastify.css';
import "@css/tips.css";

function App() {
  const auth = useSelector(state => state.token);

  return (
    <div className="App">
      <ToastContainer 
      position="bottom-center"
      autoClose={6000}
      pauseOnFocusLoss
      hideProgressBar={false}
      />
      <Navigation>
          {auth.token ? <Account /> : <Redirect /> }
      </Navigation>
    </div>
  );
}

export default App;
