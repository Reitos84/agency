const path = require('path');

module.exports = {
    webpack: {
        alias: {
            "@" : path.resolve(__dirname, 'src/'),
            "@core" : path.resolve(__dirname, 'src/Core'),
            "@actions" : path.resolve(__dirname, 'src/Core/actions'),
            "@middleware" : path.resolve(__dirname, 'src/Core/middleware'),
            "@reducers" : path.resolve(__dirname, 'src/Core/reducers'),
            "@selectors" : path.resolve(__dirname, 'src/Core/selectors'),
            "@stores" : path.resolve(__dirname, 'src/Core/stores'),
            "@services" : path.resolve(__dirname, 'src/Services'),
            "@api" : path.resolve(__dirname, 'src/Services/api'),
            "@auth" : path.resolve(__dirname, 'src/Services/auth'),
            "@assets" : path.resolve(__dirname, 'src/Views/assets'),
            "@components" : path.resolve(__dirname, 'src/Views/components'),
            "@css" : path.resolve(__dirname, 'src/Views/css'),
            "@fonts" : path.resolve(__dirname, 'src/Views/fonts'),
            "@navigation" : path.resolve(__dirname, 'src/Views/navigation'),
            "@screens" : path.resolve(__dirname, 'src/Views/screens'),
            "@constants": path.resolve(__dirname, "src/Views/constants"),
            "@views": path.resolve(__dirname, "src/Views"),
            "@lodash": path.resolve(__dirname, "src/Views/lodash")
        }
    }
}